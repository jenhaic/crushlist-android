package crushlist.advanced.activity;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.ExpandableListView.OnChildClickListener;
import android.widget.ImageView;
import android.widget.TextView;

import crushlist.advanced.R;
import crushlist.advanced.util.General;
import crushlist.advanced.activity.CCExpListAdapter;

public class CCActivity extends Activity {

	private CCExpListAdapter expListAdapter;
    private ExpandableListView expListView;
    private List<String> listDataHeader;
    private HashMap<String, List<String>> listDataChild;
    
    private TextView ccemailTextView;
    private TextView ccothersTextView;
    private ImageView ccemailImageView;
    private View ccSeparatorView;

    private String activity;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cc);
        
        ccSeparatorView = (View) findViewById(R.id.ccSeparator);
        
        // get the listview
        expListView = (ExpandableListView) findViewById(R.id.cclist);
        // preparing list data
        prepareListData();
        expListAdapter = new CCExpListAdapter(this, listDataHeader, listDataChild);
        // setting list adapter
        expListView.setAdapter(expListAdapter);
        // Listview on child click listener
        expListView.setOnChildClickListener(new OnChildClickListener() {
            @Override
            public boolean onChildClick(ExpandableListView parent, View v, int groupPosition, int childPosition, long id) {
            	String selected = expListAdapter.getChild(groupPosition, childPosition).toString().replaceAll("\\D","");
            	
            	if(activity != null && activity.equals("login"))
            		LoginActivity.cc = selected;
            	else if(activity != null && activity.equals("selected"))
            		SelectedActivity.cc = selected;
            	else if(activity != null && activity.equals("SMS"))
            		SMSActivity.cc = selected;
            	
            	General.goBack(CCActivity.this);
            	
                return false;
            }
        });
        
        ccemailTextView = (TextView) findViewById(R.id.ccemail);
        ccemailTextView.setOnClickListener(new CCEmailTextClickListener());
        ccothersTextView = (TextView) findViewById(R.id.ccothers);
        ccothersTextView.setOnClickListener(new CCOthersTextClickListener());
        ccemailImageView = (ImageView) findViewById(R.id.ccemailImage);
        ccemailImageView.setOnClickListener(new CCEmailTextClickListener());
    }
    
    public void onResume() {	
    	super.onResume();
    	
    	Intent intent = getIntent();
        activity = intent.getStringExtra("activity");  
        
        if(activity != null && activity.equals("selected"))
        {
        	ccemailTextView.setVisibility(View.VISIBLE);
        	ccSeparatorView.setVisibility(View.VISIBLE);
        	ccemailImageView.setVisibility(View.VISIBLE);
        }
        else
        {
        	ccemailTextView.setVisibility(View.GONE);
        	ccSeparatorView.setVisibility(View.GONE);
        	ccemailImageView.setVisibility(View.GONE);
        }
        
    }

    //Preparing the list data
    private void prepareListData() {
        listDataHeader = new ArrayList<String>();
        listDataChild = new HashMap<String, List<String>>();
 
        // Adding child data
        listDataHeader.add(getString(R.string.asia));
        listDataHeader.add(getString(R.string.america));
        listDataHeader.add(getString(R.string.europe));
        listDataHeader.add(getString(R.string.africa));
 
        // Adding child data
        List<String> asia = new ArrayList<String>();
        asia.add(getString(R.string.china));
        asia.add(getString(R.string.taiwan));
        asia.add(getString(R.string.hongkong));
        asia.add(getString(R.string.japan));
        asia.add(getString(R.string.macau));
        asia.add(getString(R.string.korea));
        asia.add(getString(R.string.singapore));
        asia.add(getString(R.string.malaysia));
        asia.add(getString(R.string.philippines));
        asia.add(getString(R.string.thailand));
        asia.add(getString(R.string.indonesia));
        asia.add(getString(R.string.india));
        asia.add(getString(R.string.turkey));
        asia.add(getString(R.string.uae));
        asia.add(getString(R.string.saudiarabia));
        asia.add(getString(R.string.israel));
        asia.add(getString(R.string.iran));
        asia.add(getString(R.string.iraq));
        asia.add(getString(R.string.jordan));
        asia.add(getString(R.string.myanmar));
        asia.add(getString(R.string.mongolia));
        asia.add(getString(R.string.vietnam));
 
        List<String> america = new ArrayList<String>();
        america.add(getString(R.string.us));
        america.add(getString(R.string.mexico));
        america.add(getString(R.string.cuba));
        america.add(getString(R.string.brazil));
        america.add(getString(R.string.argentina));
        america.add(getString(R.string.chile));
        america.add(getString(R.string.colombia));
        america.add(getString(R.string.paraguay));
        america.add(getString(R.string.uruguay));
        america.add(getString(R.string.venezuela));
        america.add(getString(R.string.peru));
 
        List<String> europe = new ArrayList<String>();
        europe.add(getString(R.string.uk));
        europe.add(getString(R.string.germany));
        europe.add(getString(R.string.france));
        europe.add(getString(R.string.ireland));
        europe.add(getString(R.string.russia));
        europe.add(getString(R.string.ukraine));
        europe.add(getString(R.string.italy));
        europe.add(getString(R.string.netherlands));
        europe.add(getString(R.string.belgium));
        europe.add(getString(R.string.luxembourg));
        europe.add(getString(R.string.spain));
        europe.add(getString(R.string.portugal));
        europe.add(getString(R.string.switzerland));
        europe.add(getString(R.string.greece));
        europe.add(getString(R.string.norway));
        europe.add(getString(R.string.sweden));
        europe.add(getString(R.string.finland));
        europe.add(getString(R.string.denmark));
        europe.add(getString(R.string.hungary));
        europe.add(getString(R.string.austria));
        europe.add(getString(R.string.poland));
        europe.add(getString(R.string.czech));
        
        List<String> africa = new ArrayList<String>();
        africa.add(getString(R.string.australia));
        africa.add(getString(R.string.newzealand));
        africa.add(getString(R.string.egypt));
        africa.add(getString(R.string.southafrica));
        africa.add(getString(R.string.morocco));
        africa.add(getString(R.string.niger));
        africa.add(getString(R.string.nigeria));
        africa.add(getString(R.string.algeria));
        
        // Header, Child data
        listDataChild.put(listDataHeader.get(0), asia); 
        listDataChild.put(listDataHeader.get(1), america);
        listDataChild.put(listDataHeader.get(2), europe);
        listDataChild.put(listDataHeader.get(3), africa);
    }
       
    class CCEmailTextClickListener implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
        	String tag = (String)v.getTag();
        	//Log.e("tag", tag);

        	SelectedActivity.cc = tag;
        	
        	General.goBack(CCActivity.this);
        }
    }
    
    class CCOthersTextClickListener implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
        	AlertDialog.Builder alert = new AlertDialog.Builder(CCActivity.this);

        	alert.setTitle(getString(R.string.inputCC));
        	//alert.setMessage("Message");

        	// Set an EditText view to get user input 
        	final EditText inputEdit = new EditText(CCActivity.this);
        	inputEdit.setInputType(InputType.TYPE_CLASS_PHONE);
        	alert.setView(inputEdit);

        	alert.setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
        	public void onClick(DialogInterface dialog, int whichButton) {
        	  dialog.dismiss();
        	  }
        	});

        	alert.setNegativeButton(getString(R.string.confirm), new DialogInterface.OnClickListener() {
			public void onClick(DialogInterface dialog, int whichButton) {
        		  
        		  if(inputEdit != null && inputEdit.getText().toString().trim().length() > 0)
        		  {
        			  String otherCC = null;
        			  
        		      if(inputEdit.getText().toString().trim().replaceAll("\\D", "").length() >= 3)
        		    	  otherCC = inputEdit.getText().toString().trim().replaceAll("\\D", "").substring(0, 3);
        		      else
        		    	  otherCC = inputEdit.getText().toString().trim().replaceAll("\\D", "");
        			  
	        		  if(activity != null && activity.equals("login"))
	        			  LoginActivity.cc = otherCC;
	              	  else if(activity != null && activity.equals("selected"))
	              		  SelectedActivity.cc = otherCC;
	              	  else if(activity != null && activity.equals("SMS"))
	              		  SMSActivity.cc = otherCC;
	        		  
	        		  dialog.dismiss();
	        		  General.goBack(CCActivity.this);
        		  }
        		  else if(inputEdit != null && inputEdit.getText().toString().trim().length() == 0)
        			  dialog.dismiss();
        			 
        	  }
        	});

        	alert.show(); 	
        }
    }

    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}

