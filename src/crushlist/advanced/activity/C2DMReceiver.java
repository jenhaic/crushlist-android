package crushlist.advanced.activity;

import crushlist.advanced.R;
import crushlist.advanced.util.General;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;
//import crushlist.advanced.activity.MsgContentActivity;

public class C2DMReceiver extends BroadcastReceiver implements Runnable{
	
	private String registration;
	
	public static SharedPreferences settings;
	
    @Override
    public void onReceive(Context context, Intent intent) {
    	settings = context.getSharedPreferences("crushlist", 0);
    	
        if (intent.getAction().equals("com.google.android.c2dm.intent.REGISTRATION")) {
            try 
            {
                handleRegistration(context, intent);
            } catch (Exception e) 
            {
                e.printStackTrace();
            }
        } else if (intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {
            //handleMessage(context, intent);
        }
     }

    private void handleRegistration(Context context, Intent intent) throws Exception {
        registration = intent.getStringExtra("registration_id");
        if (intent.getStringExtra("error") != null) {
            Log.e("c2dm", "registration failed");
            String error = intent.getStringExtra("error");
            if(error == "SERVICE_NOT_AVAILABLE"){
                Log.d("c2dm", "SERVICE_NOT_AVAILABLE");
            }else if(error == "ACCOUNT_MISSING"){
                Log.d("c2dm", "ACCOUNT_MISSING");
            }else if(error == "AUTHENTICATION_FAILED"){
                Log.d("c2dm", "AUTHENTICATION_FAILED");
            }else if(error == "TOO_MANY_REGISTRATIONS"){
                Log.d("c2dm", "TOO_MANY_REGISTRATIONS");
            }else if(error == "INVALID_SENDER"){
                Log.d("c2dm", "INVALID_SENDER");
            }else if(error == "PHONE_REGISTRATION_ERROR"){
                Log.d("c2dm", "PHONE_REGISTRATION_ERROR");
            }
        } else if (intent.getStringExtra("unregistered") != null) {
            Log.e("c2dm", "unregistered");
        } else if (registration != null) {
            Log.e("c2dm", registration);
   
            Thread thread = new Thread(this);
		    thread.start();        	
        }
    }
    
    @Override
	public void run() {
    	String[] parameter = new String[3];
        String[] data = new String[3];
        parameter[0] = "registrationId"; data[0] = registration;
        parameter[1] = "country"; data[1] = LoginActivity.cc;
        parameter[2] = "cellPhoneNumber"; data[2] = LoginActivity.number;

        General.handleHttp("http://www.crushlist.cc/push/pushIdAndroid.php", parameter, data, LoginActivity.localContext);   
	}

    /*
    private void handleMessage(Context context, Intent intent)
    { 	 
    	 String title = intent.getStringExtra("title");
    	 String message = intent.getStringExtra("message");
    	 String time = intent.getStringExtra("time");
  	     String tag = intent.getStringExtra("tag");
  	     
  	     MsgIndexNewActivity.tag = tag;
  	     MsgIndexOldActivity.tag = tag;
    	 
    	 if(title != null && !title.equals("+1"))
    	 {
    		 ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);    	     
    		 String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
    		 String className = am.getRunningTasks(1).get(0).topActivity.getClassName();
    		 
    		 if(className.equals("crushlist.advanced.activity.MsgContentActivity") && MsgContentActivity.talkId != null && MsgContentActivity.talkId.equals(tag))
    		 {
    			 Intent intentForMessage = new Intent();
	        	 intentForMessage.setAction("crushlist_message_new");
	        	 intentForMessage.putExtra("message", message);
	        	 intentForMessage.putExtra("time", time);
	        	 context.sendBroadcast(intentForMessage);
    		 }
    		 else if(packageName.equals("crushlist.advanced"))
    		 {
    			 Toast textToast;
	    	     if(className.equals("crushlist.advanced.activity.MsgContentActivity"))  
		    	     textToast = Toast.makeText(context, context.getString(R.string.toastForMsg1), Toast.LENGTH_LONG);
	    		 else
	    	         textToast = Toast.makeText(context, context.getString(R.string.toastForMsg2), Toast.LENGTH_LONG);
		    	 textToast.show();
    		 }
    		 else
    		 {
    			 if(settings.getBoolean("acceptPush", true)) 
    			     notify(context, intent, title, message);
    		 }
    	 }
    	 else
    	 {
    		 if(settings.getBoolean("acceptPush", true))
    		     notify(context, intent, title, message);
    	 }
    }
    */
    
    public void notify(Context context, Intent intent, String title, String msg)
    {
    	NotificationManager manager = (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        Notification notification = new Notification(R.drawable.icon57_57, title, System.currentTimeMillis());         
        notification.flags = Notification.FLAG_AUTO_CANCEL;     
        notification.defaults|= Notification.DEFAULT_VIBRATE;
        notification.defaults|= Notification.DEFAULT_SOUND;
        notification.flags |= Notification.FLAG_SHOW_LIGHTS;
        
        intent = new Intent(context, LoginActivity.class);
        PendingIntent contentIntent = PendingIntent.getActivity(context, 0, intent, PendingIntent.FLAG_UPDATE_CURRENT);
        notification.setLatestEventInfo(context, title, msg, contentIntent);
        
        if(title != null && !title.equals("+1"))
          manager.notify(0, notification);
        else
        {
          ActivityManager am = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);    	     
   		  String packageName = am.getRunningTasks(1).get(0).topActivity.getPackageName();
          
   		  if(packageName.equals("crushlist.advanced"))
   		  {
   		    Toast textToast = Toast.makeText(context, context.getString(R.string.toastFor1), Toast.LENGTH_LONG);
   		    textToast.show();
   		  }
   		  else
            manager.notify(0, notification);
        }
    }
}
