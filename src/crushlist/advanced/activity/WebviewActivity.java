package crushlist.advanced.activity;

import crushlist.advanced.R;
import crushlist.advanced.util.General;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.widget.ProgressBar;
import android.widget.TextView;

public class WebviewActivity extends Activity{
	
	private TextView    webTitleTextView;
	private ProgressBar progressBar;
	private WebView     webView;
	
	private String      url = null;
	private String      title = null;
	
    @SuppressLint("SetJavaScriptEnabled")
	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web);
        
        webView     = (WebView)findViewById(R.id.webView);
        webTitleTextView = (TextView)findViewById(R.id.webTitle);
        progressBar = (ProgressBar)findViewById(R.id.webProgressBar);
        
        Intent intent = getIntent();
        url = intent.getStringExtra("url");
        title = intent.getStringExtra("title");
        
        webTitleTextView.setText(title);
        
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollbarFadingEnabled(false);
        if(haveNetworkConnection())      
          webView.loadUrl(url);     
        webView.setWebChromeClient(new WebChromeClient() 
        {
            @Override
			public void onProgressChanged(WebView view, int progress)  
            {
                if(progress == 100)
                	progressBar.setVisibility(View.INVISIBLE);
            }
        });   
    }
    
    private boolean haveNetworkConnection() {
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
