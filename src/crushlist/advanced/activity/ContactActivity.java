package crushlist.advanced.activity;

import java.util.ArrayList;
import java.util.HashMap;

import android.app.Activity;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.ContactsContract;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.SimpleAdapter;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

public class ContactActivity extends Activity implements Runnable {
    
	private ListView listView;
    private EditText contactSearch;
    private SimpleAdapter adapter;
    private Cursor cursor;
    
    private ArrayList<HashMap<String, Object>> list;
    
    private String activity;
    private String contacts;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.contact);
        
        contactSearch = (EditText) findViewById(R.id.contactSearch);
        contactSearch.addTextChangedListener(filterTextWatcher);
        listView = (ListView) findViewById(R.id.list);

        list = new ArrayList<HashMap<String, Object>>();
        
        General.showProgressDialog(this);
    	Thread thread = new Thread(this);
	    thread.start();
    }
    
    public void onResume() {	
    	super.onResume();
    	
    	Intent intent = getIntent();
        activity = intent.getStringExtra("activity"); 
        contacts = intent.getStringExtra("contacts"); 
    }
    
    @Override
	public void run()
	{
    	String[] PROJECTION = null;
        String SELECTION = null;
    	
    	if(contacts.equals("phone"))
    	{
    		PROJECTION = new String[] {ContactsContract.Contacts._ID, ContactsContract.Contacts.DISPLAY_NAME};
            SELECTION = ContactsContract.Contacts.HAS_PHONE_NUMBER + ">='1'";
            cursor = getContentResolver().query(ContactsContract.Contacts.CONTENT_URI, PROJECTION, SELECTION, null, null);
            
            while(cursor != null && cursor.moveToNext())
            {
            	String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            	String id = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts._ID)); //cursor.getString(cursor.getColumnIndex(BaseColumns._ID)); the same
         	
            	HashMap<String, Object> user = new HashMap<String, Object>();
                user.put("name", name);
                user.put("id", id);
                list.add(user);
            }
    	}
    	else
    	{
    		PROJECTION = new String[] {ContactsContract.CommonDataKinds.Email.CONTACT_ID, ContactsContract.Contacts.DISPLAY_NAME};
    		cursor = getContentResolver().query(ContactsContract.CommonDataKinds.Email.CONTENT_URI, PROJECTION, SELECTION, null, null);
    		
    		while(cursor != null && cursor.moveToNext())
            {
            	String name = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
            	String id = cursor.getString(cursor.getColumnIndex(ContactsContract.CommonDataKinds.Email.CONTACT_ID)); 
         	
            	HashMap<String, Object> user = new HashMap<String, Object>();
                user.put("name", name);
                user.put("id", id);
                list.add(user);
            }
    	}

        handler.sendEmptyMessage(0);
	}
    
    private Handler handler = new Handler() {
        @Override
		public void handleMessage(Message msg) {
        	
        	adapter = new SimpleAdapter(ContactActivity.this, list, R.layout.user, new String[]{"name"}, new int[]{ R.id.name });

            listView.setAdapter(adapter);
            listView.setOnItemClickListener(new SelectContactListener());
            listView.setTextFilterEnabled(true);
            
        	General.dismissProgressDialog();
        }
    };

    public SimpleAdapter getAdapter() {
        return this.adapter;
    }
    
    private TextWatcher filterTextWatcher = new TextWatcher() {

        public void afterTextChanged(Editable s) {}
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

        public void onTextChanged(CharSequence s, int start, int before, int count) 
        {
            adapter.getFilter().filter(s);
        }

    }; 
    
    class SelectContactListener implements OnItemClickListener 
    {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) 
        {
        	@SuppressWarnings("unchecked")
			HashMap<String, Object> selected = (HashMap<String, Object>) ContactActivity.this.getAdapter().getItem(position);
            String ID   = (String) selected.get("id");
            String name = (String) selected.get("name");
            String phoneOrEmail = null;
            
            if(contacts.equals("phone"))
            {
	            Cursor cursrorPhone = managedQuery(ContactsContract.CommonDataKinds.Phone.CONTENT_URI, null, ContactsContract.CommonDataKinds.Phone.CONTACT_ID + "=" + ID, null, null);
	            if (cursrorPhone != null && cursrorPhone.getCount() > 0) 
	            {
	            	cursrorPhone.moveToFirst();
	            	phoneOrEmail = cursrorPhone.getString(cursrorPhone.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
	            	SelectedActivity.cc = LoginActivity.cc;
	            }
            }
            else
            {
            	Cursor cursrorEmail = managedQuery(ContactsContract.CommonDataKinds.Email.CONTENT_URI, null, ContactsContract.CommonDataKinds.Email.CONTACT_ID + "=" + ID, null, null);
                if (cursrorEmail != null && cursrorEmail.getCount() > 0) 
                {
                	cursrorEmail.moveToFirst();
                	phoneOrEmail = cursrorEmail.getString(cursrorEmail.getColumnIndex(ContactsContract.CommonDataKinds.Email.DATA));
                	SelectedActivity.cc = "email";
                }
            }
 	
        	if(activity != null && activity.equals("selected"))
        	{        		
        		SelectedActivity.number = phoneOrEmail;
        		SelectedActivity.name = name;
        	}
        	
        	General.goBack(ContactActivity.this);
        }
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}


