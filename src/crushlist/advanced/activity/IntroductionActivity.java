package crushlist.advanced.activity;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;

import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

public class IntroductionActivity extends Activity{ 
	
	private TextView introNoteView;
    private Button introUnderStandButton;
    
    private String activity;
    private SharedPreferences settings = LoginActivity.settings;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.introduction);
        
        introNoteView = (TextView) findViewById(R.id.introNote);
        introUnderStandButton = (Button) findViewById(R.id.introUnderstand); 

        introNoteView.setText(getString(R.string.introContent));
        introUnderStandButton.setOnClickListener(new ButtonListenerUnderstand()); 
    }
    
    public void onResume() {	
    	super.onResume();
    	
    	Intent intent = getIntent();
        activity = intent.getStringExtra("activity"); 
    }
    

    class ButtonListenerUnderstand implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
        	if(activity.equals("login"))
        	{
	            settings.edit().putBoolean("firstLoading", false).commit();
	
	    		Intent intent = new Intent();
	            intent.setClass(IntroductionActivity.this, LoginActivity.class);
	            startActivity(intent);
	
	            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
	            finish();
        	}
        	else
        		General.goBack(IntroductionActivity.this);
        }
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
