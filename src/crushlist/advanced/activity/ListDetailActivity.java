package crushlist.advanced.activity;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

public class ListDetailActivity extends Activity implements Runnable{ 
	
	private ProgressBar progressBar;
	private Button howManyView;
	//private Button anonymousRealCardView;
    private Button anonymousSMSView;
    private Button anonymousEmailView;
    private TextView nameAndNumberView;
	
	private String howMany;
    private String result;
    private String listDetailCrush;
    private JSONObject jsonResponse;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listdetail);
        
        progressBar = (ProgressBar) findViewById(R.id.progressBar);    
        howManyView = (Button) findViewById(R.id.listDetailHowMany);
        nameAndNumberView = (TextView) findViewById(R.id.nameAndNumber);
        
        //anonymousRealCardView = (Button) findViewById(R.id.anonymousMsg); 
        //anonymousRealCardView.setOnClickListener(new ButtonListenerMsg());
        
        anonymousSMSView = (Button) findViewById(R.id.anonymousSMS);
        anonymousSMSView.setOnClickListener(new ButtonListenerSMS());
        
        anonymousEmailView = (Button) findViewById(R.id.anonymousEmail);
        anonymousEmailView.setOnClickListener(new ButtonListenerEmail());    
     
        Bundle extras = getIntent().getExtras();
    	
		nameAndNumberView.setText(getString(R.string.howManyForHim) + " " + extras.getString("nameAndNumber"));
		listDetailCrush = extras.getString("listCrush");
        
        progressBar.setVisibility(View.VISIBLE);
        Thread thread = new Thread(ListDetailActivity.this);
        thread.start();
    }
    
    /*
    class ButtonListenerMsg implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent();
            intent.setClass(ListDetailActivity.this, MsgSelectedActivity.class);
            ListDetailActivity.this.startActivity(intent);
        }
    }
    */
    
    class ButtonListenerSMS implements OnClickListener
    {
        @Override    
        public void onClick(View v)
        {
            Intent intent = new Intent();
            if(!listDetailCrush.contains("@"))
            {
            	int ccLength = LoginActivity.cc.length();
        		String listDetailCrushWOCC = listDetailCrush.substring(ccLength);
                intent.putExtra("listDetailCell",listDetailCrushWOCC);
                intent.putExtra("listDetailEmail","");
            }
            intent.setClass(ListDetailActivity.this, SMSActivity.class);
            
            ListDetailActivity.this.startActivity(intent);
            
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    class ButtonListenerEmail implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent();
            if(listDetailCrush.contains("@"))
            {
                intent.putExtra("listDetailEmail",listDetailCrush);
                intent.putExtra("listDetailCell","");
            }
            intent.setClass(ListDetailActivity.this, EmailActivity.class);
            
            ListDetailActivity.this.startActivity(intent);
            
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    @Override
	public void run(){
        try
        {
        	String[] parameter = new String[1];
            String[] data = new String[1];
            parameter[0] = "cellOrEmail"; data[0] = listDetailCrush;
            result = General.handleHttp("http://www.crushlist.cc/match/checkHowManyFinal.php", parameter, data, LoginActivity.localContext); 
            
        	jsonResponse = new JSONObject(result);   
			howMany = jsonResponse.getString("howMany");
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        handler.sendEmptyMessage(0);
    }
        
    private Handler handler = new Handler(){
        @Override
		public void handleMessage(Message msg){
        	progressBar.setVisibility(View.INVISIBLE);  

            howManyView.setText(howMany);
        }
    };
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
