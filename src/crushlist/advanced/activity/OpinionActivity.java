package crushlist.advanced.activity;

import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import crushlist.advanced.R;
import crushlist.advanced.util.General;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

public class OpinionActivity extends Activity {

	private ProgressBar progressBar;
	private EditText opinionContentEditText;
    private Button opinionSendButton;

    private String opinionResult = null;
	private HttpContext localContext = LoginActivity.localContext;
	private SharedPreferences settings = LoginActivity.settings;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.opinion);
        
        progressBar = (ProgressBar) findViewById(R.id.opinionProgressBar);
        opinionContentEditText = (EditText) findViewById(R.id.opinionContent);
        opinionSendButton = (Button) findViewById(R.id.opinionSend);

        opinionSendButton.setOnClickListener(new ButtonListenerOpinionSend());
        
    }
    
    class ButtonListenerOpinionSend implements OnClickListener, Runnable
    {
        @Override
        public void onClick(View v)
        {
        	progressBar.setVisibility(View.VISIBLE);
        	
            Thread thread = new Thread(this);
		    thread.start();
        }
        
        @Override
        public void run() {
            try {

                String url = "http://www.crushlist.cc/opinionFromAndroid.php";
                String uri = url + "?" + "content=" + URLEncoder.encode(opinionContentEditText.getText().toString().trim()) 
                		               + "&email="  + URLEncoder.encode(settings.getString("email", null));
                HttpGet httpGet = new HttpGet(uri);

                HttpClient httpClient = new DefaultHttpClient();
                HttpResponse httpResponse = httpClient.execute(httpGet, localContext);
                opinionResult = EntityUtils.toString(httpResponse.getEntity()).substring(8, 15);

            } catch (Exception e) {
                e.printStackTrace();
            }

            handler.sendEmptyMessage(0);
        }

        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {
            	progressBar.setVisibility(View.INVISIBLE);

                Builder AlertDialog = new AlertDialog.Builder(OpinionActivity.this);
                if (opinionResult != null && opinionResult.equals("success")) {
                    AlertDialog.setTitle(getString(R.string.sendSuccess));
                    DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    };
                    AlertDialog.setNeutralButton("OK", OkClick);
                } else {
                    AlertDialog.setMessage(getString(R.string.opinionFail));
                    DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                        	dialog.dismiss();
                        }
                    };
                    AlertDialog.setNeutralButton("OK", OkClick);
                }
                
                AlertDialog.show();
            }
        };
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
