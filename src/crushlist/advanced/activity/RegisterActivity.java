package crushlist.advanced.activity;

import org.json.JSONObject;

import crushlist.advanced.R;
import crushlist.advanced.util.General;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.SystemClock;
import android.telephony.SmsManager;
import android.text.Html;
//import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;

public class RegisterActivity extends Activity implements Runnable{

	private EditText registerpassword1EditView;
    private EditText registerpassword2EditView;
    private Button updatePasswordButton;
    private ProgressDialog progressDialog;
    
    private String httpResult;
    private String SMSNumber;
    private JSONObject jsonResponse;
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
        
        registerpassword1EditView = (EditText) findViewById(R.id.registerpassword1);
        registerpassword2EditView = (EditText) findViewById(R.id.registerpassword2);
        updatePasswordButton = (Button) findViewById(R.id.registerupdate);

        updatePasswordButton.setOnClickListener(new ButtonListenerUpdatePassword());
        
        progressDialog = new ProgressDialog(this);
    	progressDialog.setMessage(Html.fromHtml(getString(R.string.progressMessage)));
    	progressDialog.setCancelable(false);
    	progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
    	    @Override
    	    public void onClick(DialogInterface dialog, int which) {
    	        dialog.dismiss();
    	    }
    	});	    	
    	progressDialog.show();
        
        Thread thread = new Thread(this);
        thread.start();
    }
    
    @Override
    public void run() {

        try 
        {
        	httpResult = General.handleHttp("http://www.crushlist.cc/getSMSNumber.php", null, null, null); 
            
            jsonResponse = new JSONObject(httpResult);
        	SMSNumber = jsonResponse.getString("number");

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        handler.sendEmptyMessage(0);
    }
    
    private Handler handler = new Handler() {
        @Override
		public void handleMessage(Message msg) {
            progressDialog.dismiss();
        }
    };
    
    class ButtonListenerUpdatePassword implements OnClickListener, Runnable
    {
    	@Override
        public void onClick(View v)
        {
        	InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE); 
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                       InputMethodManager.HIDE_NOT_ALWAYS);
            
            String password1 = null;
            String password2 = null;
            
            password1 = General.getEditText(registerpassword1EditView);
            password2 = General.getEditText(registerpassword2EditView);
        	
            if(password1.length() < 1 || password2.length() < 1)
            	General.showAlert(getString(R.string.sendFail), RegisterActivity.this);
            else if(!password1.equals(password2))
            	General.showAlert(getString(R.string.updatePasswordMsg2), RegisterActivity.this);
            else if(!General.haveNetworkConnection(RegisterActivity.this))
                General.showAlert(getString(R.string.noNetwork), RegisterActivity.this);
            else
            {
            	SmsManager sm = SmsManager.getDefault(); 
            	
            	//Log.e("SMSNumber:", SMSNumber);
            	if(SMSNumber == null) 
            		SMSNumber = "+886978913386"; 
            	
            	sm.sendTextMessage(SMSNumber, null, password2, null, null); 
            	
            	General.showProgressDialog(RegisterActivity.this);
            	Thread thread = new Thread(this);
    		    thread.start();
            }
        }
        

		@Override
		public void run()
		{
			try
			{
				SystemClock.sleep(5000);
			} catch (Exception e)
			{
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			handler.sendEmptyMessage(0);
		}
		
		private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {
    
                General.dismissProgressDialog();
                
                Builder AlertDialog = new AlertDialog.Builder(RegisterActivity.this);
            	
            	DialogInterface.OnClickListener OKClick = new DialogInterface.OnClickListener() {
                    @Override
        			public void onClick(DialogInterface dialog, int which) {
                    	
                        //Intent intent = new Intent();
                        //intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        //intent.setClass(ListActivity.this, LoginActivity.class);
                        //startActivity(intent);  
                    	dialog.dismiss();
                        General.goBack(RegisterActivity.this);
                    }
                };
                
                String success = getString(R.string.sendSuccess);
                String YES = getString(R.string.YES);
                
                AlertDialog.setMessage(success).setPositiveButton(YES, OKClick).show();
                
            }
        };
  
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
