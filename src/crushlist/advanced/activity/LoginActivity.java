package crushlist.advanced.activity;

import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.HttpResponse;
import org.apache.http.client.CookieStore;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.protocol.ClientContext;
import org.apache.http.impl.client.BasicCookieStore;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.BasicHttpContext;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.telephony.TelephonyManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;   

import crushlist.advanced.R;
import crushlist.advanced.util.C2DMRegister;
import crushlist.advanced.util.General;

import android.text.Html;

public class LoginActivity extends Activity{

    private EditText numberView;
    private EditText passwordView;
    private Button loginView;
    private Button ccButton;
    private Button rememberButton;
    private ProgressDialog progressDialog;
    private Button smsButton;
    private Button emailButton;
   
	public static String cc;
    public static String number;
    private String password;

    public static String locale;
    public static String langMobile;
    public static CookieStore cookieStore = new BasicCookieStore();
    public static HttpContext localContext = new BasicHttpContext();   
    public static SharedPreferences settings;
    public static int normalGoBack; 
    
    private String loginResult;
    private JSONObject jsonResponse;
    
    /** Called when the activity is first created. */

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        
        ccButton = (Button) findViewById(R.id.ccButton);
        numberView = (EditText) findViewById(R.id.number);
        passwordView = (EditText) findViewById(R.id.password);
        loginView = (Button) findViewById(R.id.Login);
        rememberButton = (Button) findViewById(R.id.Remember);
        smsButton = (Button) findViewById(R.id.smsButton);
        emailButton = (Button) findViewById(R.id.emailButton);
        
        ccButton.setOnClickListener(new ButtonListenerCC());
        smsButton.setOnClickListener(new ButtonListenerSMS());
        emailButton.setOnClickListener(new ButtonListenerEmail());
        loginView.setOnClickListener(new ButtonListenerLogin());
        rememberButton.setOnClickListener(new ButtonListenerRemember());
        passwordView.requestFocus();
       
        locale = Locale.getDefault().toString();
        if (locale.equals("zh_TW") || locale.equals("zh_HK"))
            langMobile = "zh_TW.utf8";   
        else if (locale.equals("zh_CN"))
            langMobile = "zh_CN.utf8";     
        else
            langMobile = "en_US.utf8";
        
        settings = getSharedPreferences("crushlist", 0);     
      
        if(!General.haveNetworkConnection(this))
        {
            General.showAlert(getString(R.string.noNetwork), this);
        }
        
        normalGoBack = 1;
        
        if(settings.getBoolean("firstLoading", true))
        {        	
            Intent intent = new Intent();
            intent.setClass(LoginActivity.this, IntroductionActivity.class);
            intent.putExtra("activity", "login");
            startActivity(intent);
            
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
            finish();
        }
        
        /*
         * Get Key Hash for FB
        PackageInfo info;
        try{
            info = getPackageManager().getPackageInfo("crushlist.advanced",PackageManager.GET_SIGNATURES);
         for(Signature signature : info.signatures)
         {      MessageDigest md;
                md =MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
         String KeyResult =new String(Base64.encode(md.digest(),0));//String something = new String(Base64.encodeBytes(md.digest()));
         Log.e("hash key", KeyResult);
         //Toast.makeText(this,"My FB Key is \n"+ KeyResult , Toast.LENGTH_LONG ).show();
         }
        }catch(NameNotFoundException e){Log.e("name not found", e.toString());
        }catch(NoSuchAlgorithmException e){Log.e("no such an algorithm", e.toString());
        }catch(Exception e){Log.e("exception", e.toString());}
        */
    }

    @Override
    public void onResume() {
        super.onResume();
        
        if(settings.getBoolean("waitingCode1", false) && LoginActivity.normalGoBack == 1)
        {
        	Intent intent = new Intent();    
        	intent.putExtra("fromWhichEmail","email");
	    	intent.setClass(LoginActivity.this, RegisterEmailActivity.class);
	    	startActivity(intent);
	    	
	    	overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
	    	finish();
        }
        else if(settings.getBoolean("waitingCode2", false) && LoginActivity.normalGoBack == 1)
        {
        	Intent intent = new Intent();    
        	intent.putExtra("fromWhichEmail","email2");
	    	intent.setClass(LoginActivity.this, RegisterEmailActivity.class);
	    	startActivity(intent);
	    	
	    	overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
	    	finish();
        }
        else
        {
	        if(settings.getBoolean("registered", false))
	        {
	        	ccButton.setText(settings.getString("cc", ""));
	        	numberView.setText(settings.getString("number", ""));
	        	ccButton.setEnabled(false);
	        	numberView.setEnabled(false);   	
	        }
	        else
	        {    
	        	settings.edit().putBoolean("acceptPush", true).commit();
	        	getPhoneNumber();   
	            
	        	ccButton.setEnabled(true);
	        	numberView.setEnabled(true);
	        }
	        
	        if(cc != null)
	        	ccButton.setText(cc);
	        
	        if(!settings.getBoolean("notRememberPassword", false))
	    	{
	    		passwordView.setText(settings.getString("password", ""));
	    		rememberButton.setBackgroundResource(R.drawable.check32_32);
	    	}
	    	else
	    	{
	    		passwordView.setText("");
	    		rememberButton.setBackgroundResource(R.drawable.uncheck32_32);
	    	}
        }
    }
    
    
    class ButtonListenerRemember implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
        	if(settings.getBoolean("notRememberPassword", false))
        	{
        		settings.edit().putBoolean("notRememberPassword", false).commit();
        		rememberButton.setBackgroundResource(R.drawable.check32_32);
        	}
        	else
        	{
        		settings.edit().putBoolean("notRememberPassword", true).commit();
        		rememberButton.setBackgroundResource(R.drawable.uncheck32_32);
        	}
        }
    }
    
    class ButtonListenerCC implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent();
            intent.setClass(LoginActivity.this, CCActivity.class);
            intent.putExtra("activity","login");
            LoginActivity.this.startActivity(intent);
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    class ButtonListenerSMS implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {  	     	
        	Intent intent = new Intent();
            intent.setClass(LoginActivity.this, RegisterActivity.class);
            LoginActivity.this.startActivity(intent);
  
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    class ButtonListenerEmail implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
        	Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
        	emailIntent.setType("plain/text");
        	emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{"crushlist.service@gmail.com"});
        	LoginActivity.this.startActivity(emailIntent);

            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    class ButtonListenerLogin implements OnClickListener, Runnable
    {
        @Override
        public void onClick(View v)
        {
        	InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE); 
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
        	   	
        	cc = ccButton.getText().toString().trim().replaceAll("\\D", ""); 
            number = General.getEditText(numberView).replaceAll("\\D", "");
            number = number.replaceFirst("^0*", "");
            password = General.getEditText(passwordView);            
            
            if(password.length() <= 0)
                General.showAlert(getString(R.string.sendFail), LoginActivity.this);
            else if(number.length() < 5)
                General.showAlert(getString(R.string.sendFail), LoginActivity.this);
            else if(cc.length() <= 0)
                General.showAlert(getString(R.string.sendFail), LoginActivity.this);
            else
            {
	        	progressDialog = new ProgressDialog(LoginActivity.this);
		    	progressDialog.setMessage(Html.fromHtml(getString(R.string.progressMessage)));
		    	progressDialog.setCancelable(false);
		    	progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
		    	    @Override
		    	    public void onClick(DialogInterface dialog, int which) {
		    	        dialog.dismiss();
		    	    }
		    	});	    	
		    	progressDialog.show();
		
		        Thread thread = new Thread(this);
			    thread.start();
            }
        }
        
        @Override
        public void run() {
            try {

                String url = "http://www.crushlist.cc/loginAndroidFinal.php";
                String uri = url + "?" + "country=" + cc + "&cellPhoneNumber=" + URLEncoder.encode(number) + "&password=" + URLEncoder.encode(password);
                
                HttpGet httpGet = new HttpGet(uri);
                
                DefaultHttpClient httpClient = new DefaultHttpClient();
                HttpResponse httpResponse = null;
                
                httpResponse = httpClient.execute(httpGet);
                
                cookieStore = httpClient.getCookieStore();
                localContext.setAttribute(ClientContext.COOKIE_STORE, cookieStore);

                loginResult = EntityUtils.toString(httpResponse.getEntity());
                jsonResponse = new JSONObject(loginResult);
                     
                //Log.e("======= json data",loginResult);
                
            } catch (Exception e) {
                e.printStackTrace();
            }

            handler.sendEmptyMessage(0);
        }

        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {        
                try
				{
					if (jsonResponse.getString("result") != null && jsonResponse.getString("result").equals("success")) {

						settings.edit().putBoolean("registered", true).commit();
						settings.edit().putString("cc", cc).commit();
						settings.edit().putString("number", number).commit();
						settings.edit().putString("password", password).commit();
	
						//numberView.setText(number);
						
					    C2DMRegister.register(LoginActivity.this);
					    
					    if(jsonResponse.getString("email") != null && jsonResponse.getString("email").contains("@"))
					    {
					    	settings.edit().putBoolean("isLogin", true).commit();
					    	settings.edit().putString("email", jsonResponse.getString("email")).commit();
					    	
					    	Intent intent = new Intent();
					    	
					    	intent.putExtra("howMany",jsonResponse.getString("howMany"));
							intent.putExtra("email2",jsonResponse.getString("email2"));
							intent.putExtra("email",jsonResponse.getString("email"));
							
							intent.putExtra("style1",jsonResponse.getString("style1"));
							intent.putExtra("style2",jsonResponse.getString("style2"));
							intent.putExtra("style3",jsonResponse.getString("style3"));
							intent.putExtra("style4",jsonResponse.getString("style4"));
							intent.putExtra("style5",jsonResponse.getString("style5"));
							intent.putExtra("style6",jsonResponse.getString("style6"));
							intent.putExtra("style7",jsonResponse.getString("style7"));
							
							intent.putExtra("crush1",jsonResponse.getString("crush1"));
							intent.putExtra("crush2",jsonResponse.getString("crush2"));
							intent.putExtra("crush3",jsonResponse.getString("crush3"));
							intent.putExtra("crush4",jsonResponse.getString("crush4"));
							intent.putExtra("crush5",jsonResponse.getString("crush5"));
							intent.putExtra("crush6",jsonResponse.getString("crush6"));
							intent.putExtra("crush7",jsonResponse.getString("crush7"));

							intent.putExtra("crush1Name",jsonResponse.getString("crush1Name") + "\n" + jsonResponse.getString("crush1"));
							intent.putExtra("crush2Name",jsonResponse.getString("crush2Name") + "\n" + jsonResponse.getString("crush2"));
							intent.putExtra("crush3Name",jsonResponse.getString("crush3Name") + "\n" + jsonResponse.getString("crush3"));
							intent.putExtra("crush4Name",jsonResponse.getString("crush4Name") + "\n" + jsonResponse.getString("crush4"));
							intent.putExtra("crush5Name",jsonResponse.getString("crush5Name") + "\n" + jsonResponse.getString("crush5"));
							intent.putExtra("crush6Name",jsonResponse.getString("crush6Name") + "\n" + jsonResponse.getString("crush6"));
							intent.putExtra("crush7Name",jsonResponse.getString("crush7Name") + "\n" + jsonResponse.getString("crush7"));
							
							if(jsonResponse.getInt("crush1Result")==1) intent.putExtra("status1","No"); else intent.putExtra("status1","Yes");
							if(jsonResponse.getInt("crush2Result")==1) intent.putExtra("status2","No"); else intent.putExtra("status2","Yes");
							if(jsonResponse.getInt("crush3Result")==1) intent.putExtra("status3","No"); else intent.putExtra("status3","Yes");
							if(jsonResponse.getInt("crush4Result")==1) intent.putExtra("status4","No"); else intent.putExtra("status4","Yes");
							if(jsonResponse.getInt("crush5Result")==1) intent.putExtra("status5","No"); else intent.putExtra("status5","Yes");
							if(jsonResponse.getInt("crush6Result")==1) intent.putExtra("status6","No"); else intent.putExtra("status6","Yes");
							if(jsonResponse.getInt("crush7Result")==1) intent.putExtra("status7","No"); else intent.putExtra("status7","Yes");
						
					        intent.setClass(LoginActivity.this, ListActivity.class);
					        LoginActivity.this.startActivity(intent);
					        
					        overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
					    }
					    else
					    {
					    	Intent intent = new Intent(); 
					    	intent.putExtra("fromWhichEmail","login");
					    	intent.setClass(LoginActivity.this, RegisterEmailActivity.class);
					    	startActivity(intent);
					    	
					    	overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
					    }                 
					} 
					else 
						General.showAlert(getString(R.string.loginFail), LoginActivity.this);
				} catch (JSONException e)
				{
					e.printStackTrace();
				}   
                
                progressDialog.dismiss();
            }
        };
    }
    
    public void getPhoneNumber() {
    	
        String mPhoneNumber = null;

        try {
            TelephonyManager tm = (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
            if(tm != null)
              mPhoneNumber = tm.getLine1Number().replaceAll("\\D", "");
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        if (mPhoneNumber != null)
        {
        	String first3Digit = null; String first3DigitRemove = null;
        	String first2Digit = null; String first2DigitRemove = null;
        	String first1Digit = null; String first1DigitRemove = null;
        	
        	if(mPhoneNumber.length() > 3) 
        	{
        		first3Digit = mPhoneNumber.substring(0, 3);
        		first3DigitRemove = mPhoneNumber.substring(3);
        	}
        	if(mPhoneNumber.length() > 2)
        	{
        		first2Digit = mPhoneNumber.substring(0, 2);
        		first2DigitRemove = mPhoneNumber.substring(2);
        	}
        	if(mPhoneNumber.length() > 1)
        	{
        		first1Digit = mPhoneNumber.substring(0, 1);
        		first1DigitRemove = mPhoneNumber.substring(1);
        	}
     
      	    List<String> cc3Array = new ArrayList<String>();
      	    List<String> cc2Array = new ArrayList<String>();
      	    List<String> cc1Array = new ArrayList<String>();
      	    
      	    cc2Array.add("86");
      	    cc3Array.add("886");
      	    cc3Array.add("852");
      	    cc2Array.add("81");
      	    cc3Array.add("853");
      	    cc2Array.add("82");
      	    cc2Array.add("65");
      	    cc2Array.add("60");
      	    cc2Array.add("63");
      	    cc2Array.add("66");
      	    cc2Array.add("62");
      	    cc2Array.add("91");
      	    cc2Array.add("90");
      	    cc3Array.add("971");
      	    cc3Array.add("966");
      	    cc3Array.add("972");
      	    cc2Array.add("98");
      	    cc3Array.add("964");
      	    cc3Array.add("962");
      	    cc2Array.add("95");
      	    cc3Array.add("976");
      	    cc2Array.add("84");
      	    
      	    cc1Array.add("1");
      	    cc2Array.add("52");
      	    cc2Array.add("53");
      	    cc2Array.add("55");
      	    cc2Array.add("54");
      	    cc2Array.add("56");
      	    cc2Array.add("57");
      	    cc3Array.add("595");
      	    cc3Array.add("598");
      	    cc2Array.add("58");
      	    cc2Array.add("51");

      	    cc2Array.add("44");
      	    cc2Array.add("49");
      	    cc2Array.add("33");
      	    cc3Array.add("353");
	      	cc1Array.add("7");
	      	cc3Array.add("380");
	      	cc2Array.add("39");
	      	cc2Array.add("31");
	      	cc2Array.add("32");
	      	cc3Array.add("352");
	      	cc2Array.add("34");
	      	cc3Array.add("351");
	      	cc2Array.add("41");
	      	cc2Array.add("30");
	      	cc2Array.add("47");
	      	cc2Array.add("46");
	      	cc3Array.add("358");
	      	cc2Array.add("45");
	      	cc2Array.add("36");
	      	cc2Array.add("43");
	      	cc2Array.add("48");
	      	cc3Array.add("420");
	        
	        cc2Array.add("61");
	        cc2Array.add("64");
	        cc2Array.add("20");
	        cc2Array.add("27");
	        cc3Array.add("212");
	        cc3Array.add("227");
	        cc3Array.add("234");
	        cc3Array.add("213");
        	
        	if(first3Digit != null)
        	{   		
        		for (String s : cc3Array) {
        		    if(s.equals(first3Digit)){
        		    	ccButton.setText(first3Digit);
        		    	break;
        		    }
        		}
        		
        		if(first3DigitRemove != null && first3DigitRemove.length() <= 11 && first3DigitRemove.length() >= 5)
        		{
        		    numberView.setText(first3DigitRemove);
        		}
        	}
            else if (first1Digit != null)
            {
            	for (String s : cc1Array) {
        		    if(s.equals(first1Digit)){
        		    	ccButton.setText(first1Digit);
        		    	break;
        		    }
        		}
        		
        		if(first1DigitRemove != null && first1DigitRemove.length() <= 11 && first1DigitRemove.length() >= 5)
        		{
            	    numberView.setText(first1DigitRemove);
        		}
            }  	
        	else if (first2Digit != null)
        	{
        		for (String s : cc2Array) {
        		    if(s.equals(first2Digit)){
        		    	ccButton.setText(first2Digit);
        		    	break;
        		    }
        		}
        		
        		if(first2DigitRemove != null && first2DigitRemove.length() <= 11 && first2DigitRemove.length() >= 5)
        		{
        		    numberView.setText(first2DigitRemove);
        		}
        	}
        }
    }
}
