package crushlist.advanced.activity;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
//import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import crushlist.advanced.R;
import crushlist.advanced.util.General;

public class SetActivity extends Activity {
    
    private Button setNotificationView;
    private Button opinionButton;     
    private Button changePwdView;
    private Button buyView;
    private TextView mobileTextView;
    private Button emailButton;
    private Button email2Button;
    private Button introButton;
    
	private SharedPreferences settings = LoginActivity.settings;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.set);

        buyView = (Button) findViewById(R.id.buy);
        opinionButton = (Button) findViewById(R.id.settingOpinion);     
        mobileTextView = (TextView) findViewById(R.id.settingMobile);
        emailButton = (Button) findViewById(R.id.settingEmail);
        email2Button = (Button) findViewById(R.id.settingEmail2);
        changePwdView = (Button) findViewById(R.id.updatePassword);
        setNotificationView = (Button) findViewById(R.id.setNotification);
        introButton = (Button) findViewById(R.id.settingIntro);
        
        mobileTextView.setText(settings.getString("cc", "")+settings.getString("number", ""));
        
        emailButton.setText(ListActivity.emailFromServer);
        email2Button.setText(ListActivity.email2FromServer);
        
        /*
        if(settings.getString("email2", "").contains("@"))
        	email2Button.setText(settings.getString("email2", ""));
        else
            email2Button.setText("");     
        */
        
        buyView.setOnClickListener(new ButtonListenerBuy());
        changePwdView.setOnClickListener(new ButtonListenerUpdatePassword());
        emailButton.setOnClickListener(new ButtonListenerUpdateEmail1()); 
        email2Button.setOnClickListener(new ButtonListenerUpdateEmail2()); 
        opinionButton.setOnClickListener(new ButtonListenerOpinion());
        introButton.setOnClickListener(new ButtonListenerIntro());
               
        if(LoginActivity.settings.getBoolean("acceptPush", true))
          setNotificationView.setText(getResources().getString(R.string.notification)+": "+getString(R.string.on));
        else
          setNotificationView.setText(getResources().getString(R.string.notification)+": "+getString(R.string.off));
        setNotificationView.setOnClickListener(new ButtonListenerNotification());
 
    }
    
    class ButtonListenerNotification implements OnClickListener{
        @Override
        public void onClick(View v) {
        	Builder AlertDialog = new AlertDialog.Builder(SetActivity.this);
        	DialogInterface.OnClickListener offClick = new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int which) {
                	LoginActivity.settings.edit().putBoolean("acceptPush", false).commit(); //KeyValueStorage.setNotification(SetActivity.this, "Off");
                	setNotificationView.setText(getResources().getString(R.string.notification)+": "+getString(R.string.off));
                    dialog.dismiss();
                }
            };
            DialogInterface.OnClickListener onClick = new DialogInterface.OnClickListener() {
                @Override
				public void onClick(DialogInterface dialog, int which) {
                	LoginActivity.settings.edit().putBoolean("acceptPush", true).commit(); //KeyValueStorage.setNotification(SetActivity.this, "On");
                	setNotificationView.setText(getResources().getString(R.string.notification)+": "+getString(R.string.on));
                    dialog.dismiss();
                }
            };
            AlertDialog.setTitle(getString(R.string.notification));
        	AlertDialog.setNegativeButton(getString(R.string.off), offClick);
        	AlertDialog.setPositiveButton(getString(R.string.on), onClick);
        	AlertDialog.show();
        	
        }
    }
    
    class ButtonListenerIntro implements OnClickListener{
        @Override
        public void onClick(View v) {
 	
            Intent intent = new Intent();
            intent.putExtra("activity","set");
            intent.setClass(SetActivity.this, IntroductionActivity.class);
            startActivity(intent);      

            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
 
    class ButtonListenerUpdateEmail1 implements OnClickListener{
        @Override
        public void onClick(View v) {
 	
            Intent intent = new Intent();
            intent.putExtra("fromWhichEmail","email");
            intent.setClass(SetActivity.this, RegisterEmailActivity.class);
            startActivity(intent);      

            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    class ButtonListenerUpdateEmail2 implements OnClickListener{
        @Override
        public void onClick(View v) {
 	
            Intent intent = new Intent();
            intent.putExtra("fromWhichEmail","email2");
            intent.setClass(SetActivity.this, RegisterEmailActivity.class);
            startActivity(intent);      
            
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    class ButtonListenerBuy implements OnClickListener{
        @Override
        public void onClick(View v) {
 	
            Intent intent = new Intent();
            intent.setClass(SetActivity.this, BuyActivity.class);
            startActivity(intent);      
            
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    class ButtonListenerOpinion implements OnClickListener{
        @Override
        public void onClick(View v) {
 	
            Intent intent = new Intent();
            intent.setClass(SetActivity.this, OpinionActivity.class);
            startActivity(intent);    
            
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    class ButtonListenerUpdatePassword implements OnClickListener{
        @Override
        public void onClick(View v) {
 	
        	Intent intent = new Intent();
            intent.setClass(SetActivity.this, UpdatePasswordActivity.class);
            startActivity(intent);    
            
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
    
    /*
    public void onBackPressed ()
    {
    	Builder AlertDialog = new AlertDialog.Builder(this);
    	
    	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setClass(SetActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        };
        
        DialogInterface.OnClickListener CancelClick = new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };
        
        String Logout = getString(R.string.label_logout);
        String YES = getString(R.string.YES);
        String NO = getString(R.string.NO);
        
        AlertDialog.setMessage(Logout).setPositiveButton(NO, CancelClick).setNegativeButton(YES, OkClick).show();
    }
    */
}
