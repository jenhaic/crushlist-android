package crushlist.advanced.activity;

import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import crushlist.advanced.R;
import crushlist.advanced.util.General;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;

public class UpdatePasswordActivity extends Activity {

	private ProgressBar progressBar;
	private EditText password1View;
    private EditText password2View;
    private EditText password3View;
    private Button updatePasswordButton;
    

    private String passwordResult = null;
	private HttpContext localContext = LoginActivity.localContext;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.updatepassword);
        
        progressBar = (ProgressBar) findViewById(R.id.updatePasswordProgressBar);
        password1View = (EditText) findViewById(R.id.password1);
        password2View = (EditText) findViewById(R.id.password2);
        password3View = (EditText) findViewById(R.id.password3);
        updatePasswordButton = (Button) findViewById(R.id.updatePassword);

        updatePasswordButton.setOnClickListener(new ButtonListenerUpdatePassword());
        
    }
    
    class ButtonListenerUpdatePassword implements OnClickListener, Runnable
    {
        @Override
        public void onClick(View v)
        {
        	progressBar.setVisibility(View.VISIBLE);
        	
            Thread thread = new Thread(this);
		    thread.start();
        }
        
        @Override
        public void run() {
            try {
            	String password1 = password1View.getText().toString().trim();
                String password2 = password2View.getText().toString().trim();
                String password3 = password3View.getText().toString().trim();

                String url = "http://www.crushlist.cc/updatePassword.php";
                String uri = url + "?" + "password=" + URLEncoder.encode(password1) + "&password2=" + URLEncoder.encode(password2) + "&password3=" + URLEncoder.encode(password3);
                HttpGet httpGet = new HttpGet(uri);

                HttpClient httpClient = new DefaultHttpClient();
                HttpResponse httpResponse = httpClient.execute(httpGet, localContext);
                passwordResult = EntityUtils.toString(httpResponse.getEntity()).substring(2, 9);

            } catch (Exception e) {
                e.printStackTrace();
            }

            handler.sendEmptyMessage(0);
        }

        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {
            	progressBar.setVisibility(View.INVISIBLE);

                Builder AlertDialog = new AlertDialog.Builder(UpdatePasswordActivity.this);
                if (passwordResult != null && passwordResult.equals("success")) {
                    AlertDialog.setTitle(getString(R.string.updatePasswordSuccess));
                    DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                            finish();
                        }
                    };
                    AlertDialog.setNeutralButton("OK", OkClick);
                } else {
                    AlertDialog.setTitle(getString(R.string.updatePasswordFail));
                    AlertDialog.setMessage(getString(R.string.updatePasswordMsg));
                    DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                        	dialog.dismiss();
                        }
                    };
                    AlertDialog.setNeutralButton("OK", OkClick);
                }
                
                AlertDialog.show();
            }
        };
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
