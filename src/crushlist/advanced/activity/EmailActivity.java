package crushlist.advanced.activity;

import crushlist.advanced.R;
import crushlist.advanced.util.General;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;

public class EmailActivity extends Activity{

	//private ProgressBar progressBar;
    private EditText emailView;
	private EditText contentView;
	private Button sendView;
	private ProgressDialog progressDialog;
	
    private String emailResult = null;  
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.email);
        
        //progressBar = (ProgressBar) findViewById(R.id.emailProgressBar);
        emailView = (EditText) findViewById(R.id.email);
        contentView = (EditText) findViewById(R.id.userContent);
        sendView = (Button) findViewById(R.id.send);
        
        Bundle extras = getIntent().getExtras();
        if(extras != null && extras.getString("listDetailEmail") != null)
            emailView.setText(extras.getString("listDetailEmail"));
        else
            emailView.setText("");

        sendView.setOnClickListener(new ButtonListenerEmail()); 
    }
    
    class ButtonListenerEmail implements OnClickListener, Runnable {

        @Override
    	public void onClick(View v) {
        	
        	progressDialog = new ProgressDialog(EmailActivity.this);
        	progressDialog.setMessage(Html.fromHtml(getString(R.string.progressMessage)));
        	progressDialog.setCancelable(false);
        	progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
        	    @Override
        	    public void onClick(DialogInterface dialog, int which) {
        	        dialog.dismiss();
        	    }
        	});
        	
        	progressDialog.show();
        	
            //progressBar.setVisibility(View.VISIBLE);
            new Thread(this).start();
        }

        @Override
        public void run() {
            try {
            	
            	if(emailView.getText().toString().trim().length() <= 0 || contentView.getText().toString().trim().length() <= 0)
            	{
            		handler.sendEmptyMessage(0);
            	}
            	else
            	{
                    String[] parameter = new String[3];
                    String[] data = new String[3];
                    parameter[0] = "content"; data[0] = contentView.getText().toString().trim();
                    parameter[1] = "email"; data[1] = emailView.getText().toString().trim();
                    parameter[2] = "subject"; data[2] = getString(R.string.emailSubject);
                    emailResult = General.handleHttp("http://www.crushlist.cc/contentFreeFinal.php", parameter, data, LoginActivity.localContext); 
                	handler.sendEmptyMessage(0);
            	}
            } catch (Exception e) {
                e.printStackTrace();
            }
            
        }
        
        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {
            	progressDialog.dismiss();
                //progressBar.setVisibility(View.INVISIBLE);

                Builder AlertDialog = new AlertDialog.Builder(EmailActivity.this);
                if (emailResult != null && emailResult.contains("success"))
                    AlertDialog.setMessage(getString(R.string.sendSuccess));
                else
                    AlertDialog.setMessage(getString(R.string.sendFail));
                DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                    @Override
    				public void onClick(DialogInterface dialog, int which) {
                    }
                };
                AlertDialog.setNeutralButton("OK", OkClick);
                AlertDialog.show();
            }
        };       
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
    
}
