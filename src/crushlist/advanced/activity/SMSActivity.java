package crushlist.advanced.activity;

import org.json.JSONObject;

import crushlist.advanced.R;
import crushlist.advanced.util.General;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
//import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class SMSActivity extends Activity implements Runnable{
	
	private ProgressBar progressBar2;
    private Button ccView;
    private EditText numberView;
    private EditText smsContentView;
    private Button smsSendView;
    private TextView goBuyPointView;
    private ProgressDialog progressDialog;
    
    public static String cc = null;
    private String number = null;
	
    private String smsResult = null;
    private String point;
    
    private JSONObject jsonResponse;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.sms);
          	
    	progressBar2 = (ProgressBar) findViewById(R.id.smsProgressBar);;
        ccView = (Button) findViewById(R.id.ccButton);
        numberView = (EditText) findViewById(R.id.cell);
        smsContentView = (EditText) findViewById(R.id.smsContent);
        smsSendView = (Button) findViewById(R.id.smsSend);
        goBuyPointView = (TextView) findViewById(R.id.goBuyPoint);

        ccView.setOnClickListener(new ButtonListenerCC());
        smsSendView.setOnClickListener(new ButtonListenerSMS()); 
        
        ccView.setText(LoginActivity.cc);
        Bundle extras = getIntent().getExtras();
        if(extras != null && extras.getString("listDetailCell") != null)
        	numberView.setText(extras.getString("listDetailCell"));
        else
        	numberView.setText("");
        
        Thread thread = new Thread(this);
        thread.start();
        progressBar2.setVisibility(View.VISIBLE);
    }
    
    public void onResume() {
        super.onResume();
            
        if(cc != null) 
            ccView.setText(cc);  
            
        if(number != null) 
            numberView.setText(number);
    }
    
    /*
    public void onPause() {
        super.onDestroy();
                
        if(numberView.getText().toString() != null) number = numberView.getText().toString();
        if(ccView.getText().toString() != null) cc = ccView.getText().toString();
    }
    */
    
    @Override
    public void run() {

        try 
        {
        	smsResult = General.handleHttp("http://www.crushlist.cc/buy/getPointFinal.php", null, null, LoginActivity.localContext); 
            
            jsonResponse = new JSONObject(smsResult);
        	point = jsonResponse.getString("point");

        } catch (Exception e) {
            e.printStackTrace();
        }
        
        handler.sendEmptyMessage(0);
    }
    
    private Handler handler = new Handler() {
        @Override
		public void handleMessage(Message msg) {
            progressBar2.setVisibility(View.INVISIBLE);
            
            goBuyPointView.setText(getString(R.string.yourPoint)+" "+point);
        }
    };
    
    class ButtonListenerCC implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent();
            intent.setClass(SMSActivity.this, CCActivity.class);
            intent.putExtra("activity","SMS");
            startActivity(intent);
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }  
    
    class ButtonListenerSMS implements OnClickListener, Runnable {

        @Override
    	public void onClick(View v) {
        	
        	Builder AlertDialog = new AlertDialog.Builder(SMSActivity.this);
        	if(numberView.getText().toString().trim().length() <= 0 || smsContentView.getText().toString().trim().length() <= 0 || ccView.getText().toString().trim().length() <= 0 || Integer.parseInt(point) < 5)
        	{
        		AlertDialog.setMessage(getString(R.string.smsFail));
                
                DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                    @Override
    				public void onClick(DialogInterface dialog, int which) {
                    }
                };
                AlertDialog.setNeutralButton("OK", OkClick);
                AlertDialog.show();
        	}
        	else if(smsContentView.getText().toString().trim().length() > 70)
        	{
        		AlertDialog.setMessage(getString(R.string.smsFail70));
                
                DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                    @Override
    				public void onClick(DialogInterface dialog, int which) {
                    }
                };
                AlertDialog.setNeutralButton("OK", OkClick);
                AlertDialog.show();
        	}
        	else
        	{
        		progressDialog = new ProgressDialog(SMSActivity.this);
            	progressDialog.setMessage(Html.fromHtml(getString(R.string.progressMessage)));
            	progressDialog.setCancelable(false);
            	progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            	    @Override
            	    public void onClick(DialogInterface dialog, int which) {
            	        dialog.dismiss();
            	    }
            	});        	
            	progressDialog.show();
            	
                new Thread(this).start();
        	}
        }

        @Override
        public void run() {
            try 
            {
                String content = smsContentView.getText().toString().trim();     
            
                if(content.length() > 70)
                    content = content.substring(0, 70); 

                String[] parameter = new String[3];
                String[] data = new String[3];
                parameter[0] = "content"; data[0] = content;
                parameter[1] = "country"; data[1] = ccView.getText().toString().trim();
                parameter[2] = "crush"; data[2] = General.getEditText(numberView);

                smsResult = General.handleHttp("http://www.crushlist.cc/buy/sendSMSFinal.php", parameter, data, LoginActivity.localContext); 
                handler.sendEmptyMessage(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
           
        }
        
        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {
            	progressDialog.dismiss();

                Builder AlertDialog = new AlertDialog.Builder(SMSActivity.this);
                if (smsResult != null && smsResult.contains("success"))
                {
                    AlertDialog.setMessage(getString(R.string.sendSuccess));
                    
                    DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
        				public void onClick(DialogInterface dialog, int which) {
                        }
                    };
                    AlertDialog.setNeutralButton("OK", OkClick);
                    AlertDialog.show();
                    
                    Thread thread = new Thread(SMSActivity.this);
                    thread.start();
                    progressBar2.setVisibility(View.VISIBLE);
                }
                else
                {
                    AlertDialog.setMessage(getString(R.string.smsFail));
                    
                    DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
        				public void onClick(DialogInterface dialog, int which) {
                        }
                    };
                    AlertDialog.setNeutralButton("OK", OkClick);
                    AlertDialog.show();
                }
            }
        };       
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
