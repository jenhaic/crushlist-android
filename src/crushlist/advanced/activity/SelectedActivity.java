package crushlist.advanced.activity;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.app.ProgressDialog;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.*;
import android.content.*;

public class SelectedActivity extends Activity {

	private ProgressBar progressBar;
    private EditText cellEdit;
    private EditText nameEdit;
    private Button addButton;
    private Button phoneContactsButton;
    private Button emailContactsButton;
    private Button ccButton;
    private ProgressDialog progressDialog;
       
    private String selectResult = null;
    
    public static String cc;
    public static String number;
    public static String name;
    
    private SharedPreferences settings = LoginActivity.settings;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.selected);

        progressBar = (ProgressBar) findViewById(R.id.selectedProgressBar);
        phoneContactsButton = (Button) findViewById(R.id.phoneContacts);
        emailContactsButton = (Button) findViewById(R.id.emailContacts);
        ccButton = (Button) this.findViewById(R.id.ccButton);
        cellEdit = (EditText) findViewById(R.id.cell);
        nameEdit = (EditText) findViewById(R.id.firstName);    
        addButton = (Button) findViewById(R.id.choose);                
        
        ccButton.setText(LoginActivity.cc);
        
        phoneContactsButton.setOnClickListener(new ButtonListenerPhoneContacts());
        emailContactsButton.setOnClickListener(new ButtonListenerEmailContacts());
        ccButton.setOnClickListener(new ButtonListenerCC());
        addButton.setOnClickListener(new ButtonListenerChoose());
        
        //Log.e("add number",ListActivity.addNumber+"");
    }
    
    public void onResume() {
        super.onResume();    
        
        if(number != null) cellEdit.setText(number);
        if(name != null) nameEdit.setText(name);
        if(cc != null) ccButton.setText(cc); 
        
        progressBar.setVisibility(View.INVISIBLE);
    }
    
    public void onPause() {
        super.onDestroy();
                
        if(cellEdit.getText().toString() != null) number = cellEdit.getText().toString();
        if(nameEdit.getText().toString() != null) name = nameEdit.getText().toString();
        if(ccButton.getText().toString() != null) cc = ccButton.getText().toString();  
    }
    
    public void onDestroy() {
        super.onDestroy();
                
        number = null;
        name = null;
    }
    
    class ButtonListenerCC implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent();
            intent.setClass(SelectedActivity.this, CCActivity.class);
            intent.putExtra("activity","selected");
            startActivity(intent);
            
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }

    class ButtonListenerChoose implements OnClickListener, Runnable {
        @Override
        public void onClick(View v) {
        	
        	if(cellEdit.getText().toString().trim().length() <= 0)
        	{
        		Builder AlertDialog = new AlertDialog.Builder(SelectedActivity.this);
                AlertDialog.setMessage(R.string.selectedCrushWarning);
                DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                    @Override
					public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
                AlertDialog.setNeutralButton("OK", OkClick);
                AlertDialog.show();
        	}
        	else if(cellEdit.getText().toString().trim().contains("@") && !ccButton.getText().toString().trim().equals("email"))
        	{
        		Builder AlertDialog = new AlertDialog.Builder(SelectedActivity.this);
                AlertDialog.setMessage(R.string.selectedEmailWarning);
                DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                    @Override
					public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
                AlertDialog.setNeutralButton("OK", OkClick);
                AlertDialog.show();
        	}
        	else if(!cellEdit.getText().toString().trim().contains("@") && ccButton.getText().toString().trim().equals("email"))
        	{
        		Builder AlertDialog = new AlertDialog.Builder(SelectedActivity.this);
                AlertDialog.setMessage(R.string.selectedCCWarning);
                DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                    @Override
					public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                };
                AlertDialog.setNeutralButton("OK", OkClick);
                AlertDialog.show();
        	}
        	else
        	{
        		progressDialog = new ProgressDialog(SelectedActivity.this);
            	progressDialog.setMessage(Html.fromHtml(getString(R.string.progressMessage)));
            	progressDialog.setCancelable(false);
            	progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
            	    @Override
            	    public void onClick(DialogInterface dialog, int which) {
            	        dialog.dismiss();
            	    }
            	});     	
            	progressDialog.show();
            	
                new Thread(this).start();
        	}
        }

        @Override
		public void run() {
            try 
            {
                String[] parameter = new String[6];
                String[] data = new String[6];
                parameter[0] = "crushName"; data[0] = General.getEditText(nameEdit);
                parameter[1] = "country"; data[1] = ccButton.getText().toString().trim();
                parameter[2] = "crush"; data[2] = General.getEditText(cellEdit);
                parameter[3] = "matchContent"; data[3] = getString(R.string.matchContent);
                parameter[4] = "matchSubject"; data[4] = getString(R.string.matchSubject);
                parameter[5] = "order"; data[5] = ListActivity.addNumber+"";
                selectResult = General.handleHttp("http://www.crushlist.cc/match/addCrushAndroidFinal.php", parameter, data, LoginActivity.localContext); 
                
            	handler.sendEmptyMessage(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        private Handler handler = new Handler() {
            @Override
			public void handleMessage(Message msg) {
            	progressDialog.dismiss();

                //if (selectResult != null && selectResult.equals("success")) can't work cuz there is a white space in the string
            	if(selectResult != null && selectResult.contains("success"))
                {
            		if(settings.getBoolean("firstLogin", true))
            		{
            		    settings.edit().putBoolean("firstLogin", false).commit();

	            		Intent intent = new Intent();
	                    intent.setClass(SelectedActivity.this, ListActivity.class);
	                    startActivity(intent);
	
	                    overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
	                    finish();
            		}
            		else
            			General.goBack(SelectedActivity.this);
                } 
                else 
                {
                    Builder AlertDialog = new AlertDialog.Builder(SelectedActivity.this);
                    AlertDialog.setMessage(R.string.addFail);
                    
                    DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
						public void onClick(DialogInterface dialog, int which) {
                            dialog.dismiss();
                        }
                    };

                    AlertDialog.setNeutralButton("OK", OkClick);
                    AlertDialog.show();
                }

            }
        };
    }
    
    class ButtonListenerPhoneContacts implements OnClickListener {     
        @Override
        public void onClick(View v) {     	
            Intent intent = new Intent();
            intent.setClass(SelectedActivity.this, ContactActivity.class);
            intent.putExtra("activity","selected");
            intent.putExtra("contacts","phone");
            startActivity(intent);
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    class ButtonListenerEmailContacts implements OnClickListener {      
        @Override
        public void onClick(View v) {   	
            Intent intent = new Intent();
            intent.setClass(SelectedActivity.this, ContactActivity.class);
            intent.putExtra("activity","selected");
            intent.putExtra("contacts","email");
            startActivity(intent);
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}