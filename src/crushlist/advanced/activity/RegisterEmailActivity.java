package crushlist.advanced.activity;

import java.net.URLEncoder;
import java.util.Arrays;
import java.util.regex.Pattern;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.apache.http.HttpResponse;

import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.model.GraphUser;
import com.facebook.widget.LoginButton;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.text.Html;
import android.util.Patterns;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.accounts.Account;
import android.accounts.AccountManager;
import crushlist.advanced.R;
import crushlist.advanced.util.General;

public class RegisterEmailActivity extends Activity {
	
	private Button sendVerificationButton;
	private EditText emailEditText;
	private Button verifyButton;
	private EditText codeEditText;
	private ProgressDialog progressDialog;
	private LoginButton fbLoginButton;
	private View separator1;
	private View separator2; 
	private TextView setEmailNote;
	private TextView registerEmailTitle;
	
	private String mobile;
	private String email;
	private String code;
	private String sendCodeResult;
	private String checkCodeResult;
	private String updateFBEmailResult;
	
	private String fromWhichEmail;
	 
    private SharedPreferences settings = LoginActivity.settings;

    @Override
    @TargetApi(8)
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.registeremail);
        
        registerEmailTitle = (TextView) findViewById(R.id.registerEmailTitle);
        
	    fbLoginButton = (LoginButton) findViewById(R.id.authButton);
	    setEmailNote = (TextView) findViewById(R.id.setEmailNote);
	    separator1 = (View)findViewById(R.id.separator1);
	    separator2 = (View)findViewById(R.id.separator2);
        
        Bundle extras = getIntent().getExtras();
    	fromWhichEmail = extras.getString("fromWhichEmail");
        
        sendVerificationButton = (Button) findViewById(R.id.sendVerification);
        emailEditText = (EditText) findViewById(R.id.emailToRegister);
        verifyButton = (Button) findViewById(R.id.verify);
        codeEditText = (EditText) findViewById(R.id.codeToVerify);
        
        
        sendVerificationButton.setOnClickListener(new ButtonListenerSendCode());
        verifyButton.setOnClickListener(new ButtonListenerCheckCode());

        mobile = settings.getString("cc", null) + settings.getString("number", null);
        
        if(settings.getBoolean("waitingCode1", false))
        {
            verifyButton.setVisibility(View.VISIBLE);
    	    codeEditText.setVisibility(View.VISIBLE);
    	    emailEditText.setText(settings.getString("email", ""));
    	    emailEditText.setEnabled(false);
        }
        else if(settings.getBoolean("waitingCode2", false))
        {
        	verifyButton.setVisibility(View.VISIBLE);
    	    codeEditText.setVisibility(View.VISIBLE);
    	    emailEditText.setText(settings.getString("email2", ""));
    	    emailEditText.setEnabled(false);
        }
        else if(fromWhichEmail.equals("email"))
        {
        	verifyButton.setVisibility(View.INVISIBLE);
    	    codeEditText.setVisibility(View.INVISIBLE);
    	    emailEditText.setText(settings.getString("email", ""));
    	    emailEditText.setEnabled(true);
        }
        else if(fromWhichEmail.equals("email2"))
        {
        	verifyButton.setVisibility(View.INVISIBLE);
    	    codeEditText.setVisibility(View.INVISIBLE);
    	    emailEditText.setText(settings.getString("email2", ""));
    	    emailEditText.setEnabled(true);
        }
        
        if(fromWhichEmail.equals("login"))
        {
        	fbLoginButton.setVisibility(View.VISIBLE);
        	separator1.setVisibility(View.VISIBLE);
        	separator2.setVisibility(View.VISIBLE);
        	setEmailNote.setVisibility(View.VISIBLE);
        	registerEmailTitle.setText(getString(R.string.setEmail));
        	      	
        	Pattern emailPattern = null;
            	
            if (android.os.Build.VERSION.SDK_INT >= 8)
                emailPattern = Patterns.EMAIL_ADDRESS; // API level 8+
            		
    	    Account[] accounts = AccountManager.get(this).getAccounts();
    	    for (Account account : accounts) {
    	        if (emailPattern.matcher(account.name).matches()) {
    	            emailEditText.setText(account.name);
    	         	break;
    	        }
    	    }
    	    
            // set permission list, Don't foeget to add email
            //fbLoginButton.setReadPermissions(Arrays.asList("public_profile","email","user_friends")); 
            fbLoginButton.setReadPermissions(Arrays.asList("email")); 
            // session state call back event
            fbLoginButton.setSessionStatusCallback(new Session.StatusCallback() {  
    		    @Override
                public void call(Session session, SessionState state, Exception exception) {	
                    if (session.isOpened()) {
                        Request.newMeRequest(session,new Request.GraphUserCallback() {
                            @Override
                            public void onCompleted(GraphUser user,Response response) {
                        	    if (user != null) {
                        		    //Log.e("=======FB","User ID "+ user.getId()); useless, this id is different from profile id
                        		    //Log.e("=======FB","Username "+ user.getUsername()); don't work anymore
                        		    //Log.e("=======FB","Email "+ user.asMap().get("email"));  
                        		    if(user.asMap().get("email").toString() != null && user.asMap().get("email").toString().contains("@") )
                        		    {
                                        email = user.asMap().get("email").toString();
                                        UpdateFBEmail updateFBEmail = new UpdateFBEmail();
                                        updateFBEmail.execute();
                        		    } 
                        	    }
                            }
                        }).executeAsync();
                        /* Get friend list who also downloaded this app and can't get all friends anymore since April 30, 2014. 
                        Request.newMyFriendsRequest(session,new GraphUserListCallback() {
                            @Override
                            public void onCompleted(List<GraphUser> users,Response response) {
                                Log.e("Response JSON", response.toString());
                                String[] names = new String[users.size()];
                                String[] id = new String[users.size()];
                                for (int i=0; i<users.size();i++){
                                    names[i] = users.get(i).getName();
                                    id[i]= users.get(i).getId(); 
                                    Log.e("=======Friend", names[i] + " " + id[i]);
                                }                           
                            }
                        }).executeAsync();
                        */
                    }
                }
            }); 
        }
        else
        {
        	fbLoginButton.setVisibility(View.GONE);
        	separator1.setVisibility(View.GONE);
        	separator2.setVisibility(View.GONE);
        	setEmailNote.setVisibility(View.GONE);
        	
        	registerEmailTitle.setText(getString(R.string.updateEmail));
        }
    }
    
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }

    class ButtonListenerSendCode implements OnClickListener, Runnable
    {
        @Override
        public void onClick(View v)
        {
        	email = emailEditText.getText().toString().trim();
        	
        	if(fromWhichEmail.equals("email") && email.equals(ListActivity.email2FromServer))
        		handler.sendEmptyMessage(1);
        	else if(fromWhichEmail.equals("email2") && email.equals(ListActivity.emailFromServer))
        	    handler.sendEmptyMessage(1);
        	else
        	{
	        	progressDialog = new ProgressDialog(RegisterEmailActivity.this);
		    	progressDialog.setMessage(Html.fromHtml(getString(R.string.progressMessage)));
		    	progressDialog.setCancelable(false);
		    	progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
		    	    @Override
		    	    public void onClick(DialogInterface dialog, int which) {
		    	        dialog.dismiss();
		    	    }
		    	});	    	
		    	progressDialog.show();
		
		        Thread thread = new Thread(this);
			    thread.start();	
        	}
        }
        
        @Override
        public void run() {
            try 
            {
            	String link;
        	    if(fromWhichEmail.equals("email") || fromWhichEmail.equals("login"))
        		    link = "http://www.crushlist.cc/match/updateCode1.php";
        	    else
        		    link = "http://www.crushlist.cc/match/updateCode2.php";
            	
                HttpGet httpGet = new HttpGet(link + "?" + "email=" + URLEncoder.encode(email, "UTF-8") + "&" + "mobile=" + URLEncoder.encode(mobile, "UTF-8"));               
                HttpClient httpClient = new DefaultHttpClient();
                HttpResponse httpResponse = httpClient.execute(httpGet);
                sendCodeResult = EntityUtils.toString(httpResponse.getEntity());
            } catch (Exception e) {
                e.printStackTrace();
            }

            handler.sendEmptyMessage(0);
        }

        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {
            	
            	if(msg.what == 1)
            	{
            		Builder AlertDialog = new AlertDialog.Builder(RegisterEmailActivity.this);
	                
                	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                        	dialog.dismiss();
                        }
                	};
                    AlertDialog.setMessage(getString(R.string.sameEmail)); 
                    AlertDialog.setNeutralButton("OK", OkClick);
                    AlertDialog.show();
            	}
            	else
            	{
                	progressDialog.dismiss();
            		
	                Builder AlertDialog = new AlertDialog.Builder(RegisterEmailActivity.this);
	                if (sendCodeResult != null && sendCodeResult.contains("success")) {    
	                	
	                	LoginActivity.normalGoBack = 2;
	                	
	                	if(fromWhichEmail.equals("email") || fromWhichEmail.equals("login"))
	                	{
	                	    settings.edit().putBoolean("waitingCode1", true).commit();
	                	    settings.edit().putString("email", email).commit();  
	                	}
	                	else 
	                	{
	                	    settings.edit().putBoolean("waitingCode2", true).commit();
	                	    settings.edit().putString("email2", email).commit(); 
	                	}
	
	                	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
	                        @Override
	    					public void onClick(DialogInterface dialog, int which) {
	                        	dialog.dismiss();
	                        	
	                        	emailEditText.setEnabled(false);
	                        	verifyButton.setVisibility(View.VISIBLE);
	                        	codeEditText.setVisibility(View.VISIBLE);
	                        }
	                    };
	                    AlertDialog.setMessage(getString(R.string.sendVerificationDone)); 
	                    AlertDialog.setNeutralButton("OK", OkClick);
	                    AlertDialog.show();
	                }
            	}
            }
        };
    }
    
    class ButtonListenerCheckCode implements OnClickListener, Runnable
    {
        @Override
        public void onClick(View v)
        {    	
        	email = emailEditText.getText().toString().trim();
        	code = codeEditText.getText().toString().trim();
        	
        	progressDialog = new ProgressDialog(RegisterEmailActivity.this);
	    	progressDialog.setMessage(Html.fromHtml(getString(R.string.progressMessage)));
	    	progressDialog.setCancelable(false);
	    	progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
	    	    @Override
	    	    public void onClick(DialogInterface dialog, int which) {
	    	        dialog.dismiss();
	    	    }
	    	});	    	
	    	progressDialog.show();
	
	        Thread thread = new Thread(this);
		    thread.start();
        }
        
        @Override
        public void run() {
            try 
            {
        	    String link;
        	    if(fromWhichEmail.equals("email") || fromWhichEmail.equals("login"))
        		    link = "http://www.crushlist.cc/match/checkCode1.php";
        	    else
        		    link = "http://www.crushlist.cc/match/checkCode2.php";
        	
                HttpGet httpGet = new HttpGet(link + "?" + "email=" + URLEncoder.encode(email, "UTF-8") + "&" + "code=" + URLEncoder.encode(code, "UTF-8") + "&" + "mobile=" + URLEncoder.encode(mobile, "UTF-8"));               
                HttpClient httpClient = new DefaultHttpClient();
                HttpResponse httpResponse = httpClient.execute(httpGet);
                checkCodeResult = EntityUtils.toString(httpResponse.getEntity());
                
            }catch(Exception e) {
                e.printStackTrace();
            }

            handler.sendEmptyMessage(0);
        }

        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {
            	progressDialog.dismiss();
            	
                Builder AlertDialog = new AlertDialog.Builder(RegisterEmailActivity.this);
                if (checkCodeResult != null && checkCodeResult.contains("success")) {
 	
                	if(fromWhichEmail.equals("email") || fromWhichEmail.equals("login"))
                	{
                	    settings.edit().putBoolean("waitingCode1", false).commit();
                	    settings.edit().putString("email", email).commit();
                	}
                	else
                	{
                	    settings.edit().putBoolean("waitingCode2", false).commit();
                	    settings.edit().putString("email2", email).commit();
                	}
                	
                	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                        	dialog.dismiss();
                        	
                        	Intent intent = new Intent();
                            intent.setClass(RegisterEmailActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            
                            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
                            finish();
                        }
                    };
                    AlertDialog.setMessage(getString(R.string.registerEmailSuccessfully)); 
                    AlertDialog.setNeutralButton("OK", OkClick);
                    AlertDialog.show();
                }
                else
                {	     	
                	LoginActivity.normalGoBack = 2;
                	
                	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                        	dialog.dismiss();
                        	
                        	emailEditText.setEnabled(true);
                        	codeEditText.setText("");
                        	verifyButton.setVisibility(View.GONE);
                        	codeEditText.setVisibility(View.GONE);
                        }
                    };
                    AlertDialog.setMessage(getString(R.string.emailExisted)); 
                    AlertDialog.setNeutralButton("OK", OkClick);
                    AlertDialog.show();
                } 
            }
        };
    }
    
    class UpdateFBEmail implements Runnable
    {
    	public void execute()
    	{
    		progressDialog = new ProgressDialog(RegisterEmailActivity.this);
	    	progressDialog.setMessage(Html.fromHtml(getString(R.string.progressMessage)));
	    	progressDialog.setCancelable(false);
	    	progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, getString(R.string.cancel), new DialogInterface.OnClickListener() {
	    	    @Override
	    	    public void onClick(DialogInterface dialog, int which) {
	    	        dialog.dismiss();
	    	    }
	    	});	    	
    	    progressDialog.show();
    	
            Thread thread = new Thread(this);
	        thread.start();
    	}
    	
        @Override
        public void run() {
            try 
            {
                HttpGet httpGet = new HttpGet("http://www.crushlist.cc/match/updateEmail.php" + "?" + "email=" + URLEncoder.encode(email, "UTF-8") + "&" + "mobile=" + URLEncoder.encode(mobile, "UTF-8"));               
                HttpClient httpClient = new DefaultHttpClient();
                HttpResponse httpResponse = httpClient.execute(httpGet);
                updateFBEmailResult = EntityUtils.toString(httpResponse.getEntity()); 
            }catch(Exception e) {
                e.printStackTrace();
            }
            handler.sendEmptyMessage(0);
        }

        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {
            	progressDialog.dismiss();
            	
                Builder AlertDialog = new AlertDialog.Builder(RegisterEmailActivity.this);
                if (updateFBEmailResult != null && updateFBEmailResult.contains("success")) {
 	
                	if(fromWhichEmail.equals("email") || fromWhichEmail.equals("login"))
                	{
                	    settings.edit().putBoolean("waitingCode1", false).commit();
                	    settings.edit().putString("email", email).commit();
                	}
                	else
                	{
                	    settings.edit().putBoolean("waitingCode2", false).commit();
                	    settings.edit().putString("email2", email).commit();
                	}
                	
                	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                        	dialog.dismiss();
                        	
                        	Intent intent = new Intent();
                            intent.setClass(RegisterEmailActivity.this, LoginActivity.class);
                            intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                            startActivity(intent);
                            
                            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
                            finish();
                        }
                    };
                    AlertDialog.setMessage(getString(R.string.registerEmailSuccessfully)); 
                    AlertDialog.setNeutralButton("OK", OkClick);
                    AlertDialog.show();
                }
                else
                {	     	
                	LoginActivity.normalGoBack = 2;
                	
                	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                        	dialog.dismiss();
                        }
                    };
                    AlertDialog.setMessage(getString(R.string.emailExisted)); 
                    AlertDialog.setNeutralButton("OK", OkClick);
                    AlertDialog.show();
                } 
            }
        };
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
