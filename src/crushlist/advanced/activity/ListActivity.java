package crushlist.advanced.activity;

import java.lang.reflect.Method;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

public class ListActivity extends Activity implements Runnable{ 
    private Button styleButton[] = new Button[8];
    private Button addButton[] = new Button[8];
    private Button deleteButton[] = new Button[8];
    private Button informationButton[] = new Button[8];
    private TextView crushView[] = new TextView[8];
    private TextView statusView[] = new TextView[8];
    private ProgressBar progressBar;
    private WebView webView;
    private Button howManyView;
    private Button listSettingButton;

    private String crush[] = new String[8];
    private String crushName[] = new String[8];
    private String status[] = new String[8];
    private String style[] = new String[8];
    private String howMany;
    
    private String listResult;
    private JSONObject jsonResponse;

    private HttpContext localContext = LoginActivity.localContext;
    private String langMobile = LoginActivity.langMobile;
    private SharedPreferences settings = LoginActivity.settings;
    
    public static int addNumber;
    public static String emailFromServer;
    public static String email2FromServer;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.list);

        addButton[1] = (Button) findViewById(R.id.add1);
        addButton[2] = (Button) findViewById(R.id.add2);
        addButton[3] = (Button) findViewById(R.id.add3);
        addButton[4] = (Button) findViewById(R.id.add4);
        addButton[5] = (Button) findViewById(R.id.add5);
        addButton[6] = (Button) findViewById(R.id.add6);
        addButton[7] = (Button) findViewById(R.id.add7);
        styleButton[1] = (Button) findViewById(R.id.style1);
        styleButton[2] = (Button) findViewById(R.id.style2);
        styleButton[3] = (Button) findViewById(R.id.style3);
        styleButton[4] = (Button) findViewById(R.id.style4);
        styleButton[5] = (Button) findViewById(R.id.style5);
        styleButton[6] = (Button) findViewById(R.id.style6);
        styleButton[7] = (Button) findViewById(R.id.style7);  
        crushView[1] = (TextView) findViewById(R.id.crush1);
        crushView[2] = (TextView) findViewById(R.id.crush2);
        crushView[3] = (TextView) findViewById(R.id.crush3);
        crushView[4] = (TextView) findViewById(R.id.crush4);
        crushView[5] = (TextView) findViewById(R.id.crush5);
        crushView[6] = (TextView) findViewById(R.id.crush6);
        crushView[7] = (TextView) findViewById(R.id.crush7);
        statusView[1] = (TextView) findViewById(R.id.status1); 
        statusView[2] = (TextView) findViewById(R.id.status2);
        statusView[3] = (TextView) findViewById(R.id.status3); 
        statusView[4] = (TextView) findViewById(R.id.status4);
        statusView[5] = (TextView) findViewById(R.id.status5); 
        statusView[6] = (TextView) findViewById(R.id.status6);
        statusView[7] = (TextView) findViewById(R.id.status7); 
        deleteButton[1] = (Button) findViewById(R.id.crush1Delete);
        deleteButton[2] = (Button) findViewById(R.id.crush2Delete);
        deleteButton[3] = (Button) findViewById(R.id.crush3Delete);
        deleteButton[4] = (Button) findViewById(R.id.crush4Delete);
        deleteButton[5] = (Button) findViewById(R.id.crush5Delete);
        deleteButton[6] = (Button) findViewById(R.id.crush6Delete);
        deleteButton[7] = (Button) findViewById(R.id.crush7Delete);
        informationButton[1] = (Button) findViewById(R.id.information1);
        informationButton[2] = (Button) findViewById(R.id.information2);
        informationButton[3] = (Button) findViewById(R.id.information3);
        informationButton[4] = (Button) findViewById(R.id.information4);
        informationButton[5] = (Button) findViewById(R.id.information5);
        informationButton[6] = (Button) findViewById(R.id.information6);
        informationButton[7] = (Button) findViewById(R.id.information7);
        howManyView = (Button) findViewById(R.id.howMany);
        listSettingButton = (Button) findViewById(R.id.listSetting);
        progressBar = (ProgressBar) findViewById(R.id.listProgressBar);
        webView = (WebView) findViewById(R.id.listWebView);
        
        webView.setBackgroundColor(0x00000000);
        if (Build.VERSION.SDK_INT >= 11) // Android v3.0+
        {
            try {
        	  Method method = View.class.getMethod("setLayerType", int.class, Paint.class);
        	  method.invoke(webView, 1, new Paint()); // 1 = LAYER_TYPE_SOFTWARE (API11)
        	} catch (Exception e) {
        	}
        }

        addButton[1].setOnClickListener(new AddListener());
        addButton[2].setOnClickListener(new AddListener());
        addButton[3].setOnClickListener(new AddListener());
        addButton[4].setOnClickListener(new AddListener());
        addButton[5].setOnClickListener(new AddListener());
        addButton[6].setOnClickListener(new AddListener());
        addButton[7].setOnClickListener(new AddListener());
        informationButton[1].setOnClickListener(new ListDetailListener());
        informationButton[2].setOnClickListener(new ListDetailListener());
        informationButton[3].setOnClickListener(new ListDetailListener());
        informationButton[4].setOnClickListener(new ListDetailListener());
        informationButton[5].setOnClickListener(new ListDetailListener());
        informationButton[6].setOnClickListener(new ListDetailListener());
        informationButton[7].setOnClickListener(new ListDetailListener());
        deleteButton[1].setOnClickListener(new ButtonListenerDelete());
        deleteButton[2].setOnClickListener(new ButtonListenerDelete());
        deleteButton[3].setOnClickListener(new ButtonListenerDelete());
        deleteButton[4].setOnClickListener(new ButtonListenerDelete());
        deleteButton[5].setOnClickListener(new ButtonListenerDelete());
        deleteButton[6].setOnClickListener(new ButtonListenerDelete());
        deleteButton[7].setOnClickListener(new ButtonListenerDelete());
        styleButton[1].setOnClickListener(new ButtonListenerStyle());
        styleButton[2].setOnClickListener(new ButtonListenerStyle());
        styleButton[3].setOnClickListener(new ButtonListenerStyle());
        styleButton[4].setOnClickListener(new ButtonListenerStyle());
        styleButton[5].setOnClickListener(new ButtonListenerStyle());
        styleButton[5].setOnClickListener(new ButtonListenerStyle());
        styleButton[7].setOnClickListener(new ButtonListenerStyle());
        listSettingButton.setOnClickListener(new ListSettingListener()); 
        
        
        if(settings.getBoolean("firstLogin", true))
        {
            addNumber = 1;
            settings.edit().putBoolean("isLogin", false).commit();
        	
            Intent intent = new Intent();
            intent.setClass(ListActivity.this, SelectedActivity.class);
            startActivity(intent);
            
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
            finish();
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
            
        if(!General.haveNetworkConnection(this))
            General.showAlert(getString(R.string.noNetwork), this);
        else
        {
        	if(!settings.getBoolean("isLogin", false))
        	{
        	    progressBar.setVisibility(View.VISIBLE);
                new Thread(this).start();        
        	}
        	else
        	{
        		settings.edit().putBoolean("isLogin", false).commit();
        		
        		Bundle extras = getIntent().getExtras();
        	
        		if(extras.getString("email") != null && extras.getString("email").contains("@")) 
                	emailFromServer = extras.getString("email");
                else
                	emailFromServer = "";
        		
                if(extras.getString("email2") != null && extras.getString("email2").contains("@")) 
                	email2FromServer = extras.getString("email2");
                else
                	email2FromServer = "";

                howMany = extras.getString("howMany");
                
                for(int i=1; i<=7; i++)
                {
                	style[i] = extras.getString("style"+i);
                	crush[i] = extras.getString("crush"+i);
                	crushName[i] = extras.getString("crush"+i+"Name");
                	status[i] = extras.getString("status"+i);
                }
        		
        		handler.sendEmptyMessage(0);
        	}
        	webView.loadUrl("http://www.crushlist.cc/listShowAndroid.php?langM="+langMobile);
        }
    }
    
    @Override
    public void run() {

        try {
            HttpGet httpGet = new HttpGet("http://www.crushlist.cc/match/listJSONWithRFinal.php");
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(httpGet, localContext);
            listResult = EntityUtils.toString(httpResponse.getEntity());
            
            //Log.e("list json data",listResult);
            
            jsonResponse = new JSONObject(listResult);   
            howMany = jsonResponse.getString("howMany");
            
            if(jsonResponse.getString("email") != null && jsonResponse.getString("email").contains("@")) 
            	emailFromServer = jsonResponse.getString("email");
            
            if(jsonResponse.getString("email2") != null && jsonResponse.getString("email2").contains("@")) 
            	email2FromServer = jsonResponse.getString("email2");
            
            for(int i=1; i<=7; i++)
            {
            	style[i] = jsonResponse.getString("style"+i);
            	crush[i] = jsonResponse.getString("crush"+i);
            	crushName[i] = jsonResponse.getString("crush"+i+"Name") + "\n" + crush[i];

            	if(jsonResponse.getInt("crush"+i+"Result")==1) 
            		status[i]="No"; 
            	else 
            		status[i]="Yes";
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        handler.sendEmptyMessage(0);
    }
    
    private Handler handler = new Handler() {
        @Override
		public void handleMessage(Message msg) {
            progressBar.setVisibility(View.INVISIBLE);
            
            if(howMany != null)
            	howManyView.setText(howMany); 
            
            for(int i=1; i<=7; i++)
            {
            	if(crush[i] != null && !crush[i].equals("null"))
                {
                	crushView[i].setText(crushName[i]); 
                	informationButton[i].setVisibility(View.VISIBLE);
                	deleteButton[i].setVisibility(View.VISIBLE);
                	addButton[i].setVisibility(View.INVISIBLE);
                	crushView[i].setBackgroundColor(0x80ffffff);
                	statusView[i].setBackgroundColor(0x80ffffff);
                	
                	styleButton[i].setVisibility(View.VISIBLE);
                	if(style[i].equals("1")) styleButton[i].setBackgroundResource(R.drawable.tel128_128);
                    else if(style[i].equals("2")) styleButton[i].setBackgroundResource(R.drawable.email128_128);
                	else if(style[i].equals("3")) styleButton[i].setBackgroundResource(R.drawable.fb128_128);
                	
                	statusView[i].setText(status[i]);
                	if (status[i].equals("Yes")) {
                        statusView[i].setTextColor(Color.RED);
                        statusView[i].setTypeface(null, Typeface.BOLD);
                    }
                	else
                	{
                		statusView[i].setTextColor(Color.BLACK);
                        statusView[i].setTypeface(null, Typeface.BOLD);
                	}
                }
                else
                {
                	crushView[i].setText("");
                	informationButton[i].setVisibility(View.INVISIBLE);
                	deleteButton[i].setVisibility(View.INVISIBLE); 
                	addButton[i].setVisibility(View.VISIBLE);
                	styleButton[i].setBackgroundDrawable(null);
                	statusView[i].setText("");
                }
            }
        }
    };

    public void onBackPressed ()
    {
    	Builder AlertDialog = new AlertDialog.Builder(this);
    	
    	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {           	 
            	dialog.dismiss();
                General.goBack(ListActivity.this);
            }
        };
        
        DialogInterface.OnClickListener CancelClick = new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };
        
        String Logout = getString(R.string.label_logout);
        String YES = getString(R.string.YES);
        String NO = getString(R.string.NO);
        
        AlertDialog.setMessage(Logout).setPositiveButton(NO, CancelClick).setNegativeButton(YES, OkClick).show();
    }
    
    class ListSettingListener implements OnClickListener {
        
        @Override
        public void onClick(View v) {

            Intent intent = new Intent();
            intent.setClass(ListActivity.this, SetActivity.class);
            startActivity(intent);
            
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    class ButtonListenerStyle implements OnClickListener {
    	
    	int i;
    	
        @Override
        public void onClick(View v) {
        	i = Integer.parseInt(v.getTag().toString());
        	
        	String alert;
            String YES = getString(R.string.YES);
            String NO = getString(R.string.NO);
            if(crush[i].contains("@")) alert =  getString(R.string.emailFromYou);
        	else alert =  getString(R.string.callFromYou);
        		
        	Builder AlertDialog = new AlertDialog.Builder(ListActivity.this);
        	
        	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                @Override
    			public void onClick(DialogInterface dialog, int which) {           	 
                	dialog.dismiss();
                    
                    if(crush[i].contains("@"))
                	{
                    	Intent emailIntent = new Intent(android.content.Intent.ACTION_SEND);
                    	emailIntent.setType("plain/text");
                    	emailIntent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{crush[i]});
                    	ListActivity.this.startActivity(emailIntent);

                        overridePendingTransition(R.anim.enter_new, R.anim.enter_old);	
                	}
                	else
                	{
                		Intent intent = new Intent(Intent.ACTION_CALL, Uri.parse("tel:" + "+" +crush[i]));
                		startActivity(intent);

                        overridePendingTransition(R.anim.enter_new, R.anim.enter_old);	
                	}
                }
            };     
            DialogInterface.OnClickListener CancelClick = new DialogInterface.OnClickListener() {
                @Override
    			public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            };

            AlertDialog.setMessage(alert).setPositiveButton(NO, CancelClick).setNegativeButton(YES, OkClick).show();
        }
    }
    
    class AddListener implements OnClickListener {    
        @Override
        public void onClick(View v) {
        	addNumber = Integer.parseInt(v.getTag().toString());
        	
            Intent intent = new Intent();
            intent.setClass(ListActivity.this, SelectedActivity.class);
            startActivity(intent);
            
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }

    class ListDetailListener implements OnClickListener {
        
        @Override
        public void onClick(View v) {
        	
        	int i = Integer.parseInt(v.getTag().toString());
     	
            Intent intent = new Intent();
            intent.putExtra("nameAndNumber",crushName[i]);
            intent.putExtra("listCrush",crush[i]);
            intent.setClass(ListActivity.this, ListDetailActivity.class);
            startActivity(intent);         
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    } 
    
    class ButtonListenerDelete implements OnClickListener, Runnable {
    	
    	int i;
        @Override
        public void onClick(View v) {  
        	i = Integer.parseInt(v.getTag().toString());
        	crushView[i].setText("");
        	statusView[i].setText("");
        	informationButton[i].setVisibility(View.INVISIBLE);
        	deleteButton[i].setVisibility(View.INVISIBLE);
        	styleButton[i].setVisibility(View.INVISIBLE);
        	addButton[i].setVisibility(View.VISIBLE);
        	
            new Thread(this).start();
        }
        @Override
		public void run() {
            try 
            {
            	String[] parameter = new String[3];
                String[] data = new String[3];
                parameter[0] = "order"; data[0] = i+"";
                parameter[1] = "crush"; data[1] = crush[i];
                parameter[2] = "style"; data[2] = style[i];
            	General.handleHttp("http://www.crushlist.cc/match/deleteMatch.php", parameter, data, LoginActivity.localContext); 
            } catch (Exception e) 
            {
                e.printStackTrace();
            }
        }
    }
}
