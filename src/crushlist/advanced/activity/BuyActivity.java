package crushlist.advanced.activity;

import org.json.JSONObject;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import crushlist.advanced.R;
import crushlist.advanced.util.General;
//test
public class BuyActivity extends Activity implements Runnable{ 
	
	private ProgressBar progressBar;
	private TextView pointView;
    private Button buy50Button;
    private Button buy100Button;
    //private TextView crushNumberView;
    //private Button buy5Button;
    //private Button buy12Button;
    
    private String point;
    private String buyResult;
    //private String crushNumber;
    private String mobileNumber = LoginActivity.cc+LoginActivity.number;
    
    private JSONObject jsonResponse; 
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.buy);
        
        progressBar = (ProgressBar) findViewById(R.id.buyProgressBar);
        pointView = (TextView) findViewById(R.id.point);
        buy50Button = (Button) findViewById(R.id.buy50); 
        buy100Button = (Button) findViewById(R.id.buy100);
        //crushNumberView = (TextView) findViewById(R.id.crushNumber);
        //buy5Button = (Button) findViewById(R.id.buy5); 
        //buy12Button = (Button) findViewById(R.id.buy12);
        
        buy50Button.setOnClickListener(new ButtonListenerBuy50());
        buy100Button.setOnClickListener(new ButtonListenerBuy100()); 
        //buy5Button.setOnClickListener(new ButtonListenerBuy5());
        //buy12Button.setOnClickListener(new ButtonListenerBuy12());
    }
    
    @Override
    public void onResume() {
        super.onResume();
        
        progressBar.setVisibility(View.VISIBLE);
        Thread thread = new Thread(this);
        thread.start();
    }
    
    
    @Override
	public void run(){
        try
        {
            buyResult = General.handleHttp("http://www.crushlist.cc/match/getPointFinal.php", null, null, LoginActivity.localContext); 
            
            jsonResponse = new JSONObject(buyResult);
        	point = jsonResponse.getString("point");
            //crushNumber = jsonResponse.getString("crushNumber");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        handler.sendEmptyMessage(0);
    }
        
    private Handler handler = new Handler(){
        @Override
		public void handleMessage(Message msg){
        	progressBar.setVisibility(View.INVISIBLE);
        	
        	if(point != null)
        	  pointView.setText(point);
        	//if(crushNumber != null)
        	  //crushNumberView.setText(crushNumber);
        }
    };
    
    
    class ButtonListenerBuy50 implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
        	String uri = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=CKPSLF8ADE2K8&custom=" + mobileNumber;
        	        	
        	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        	startActivity(intent);
        }
    }
    
    class ButtonListenerBuy100 implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
        	String uri = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=U9MX3XENR63RC&custom=" + mobileNumber;

        	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        	startActivity(intent);
        }
    }
    
    class ButtonListenerBuy5 implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
        	String uri = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=2XUZGGUCAGT6U&custom=" + mobileNumber;

        	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        	startActivity(intent);
        }
    }
    
    class ButtonListenerBuy12 implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
        	String uri = "https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=NM9XYHPKBDBXU&custom=" + mobileNumber;

        	Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(uri));
        	startActivity(intent);
        }
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
