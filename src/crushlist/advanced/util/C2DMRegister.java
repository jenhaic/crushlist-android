package crushlist.advanced.util;

import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;

/**
 * Register and unregister the device via C2DM
 * @author root
 *
 */
public class C2DMRegister {

    public static void register(Context context) {
        Intent registrationIntent = new Intent("com.google.android.c2dm.intent.REGISTER");
        registrationIntent.putExtra("app", PendingIntent.getBroadcast(context, 0, new Intent(), 0));
        registrationIntent.putExtra("sender", "crushlist.android@gmail.com");
        context.startService(registrationIntent);       
    }
    
    public static void unregister(Context context) {
        Intent unregIntent = new Intent("com.google.android.c2dm.intent.UNREGISTER");
        unregIntent.putExtra("app", PendingIntent.getBroadcast(context, 0, new Intent(), 0));
        context.startService(unregIntent);
    }
}
