package crushlist.advanced.util;

import java.io.*;
import java.net.URLEncoder;

import org.apache.commons.io.IOUtils;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import crushlist.advanced.R;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.app.AlertDialog.Builder;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.text.Html;
import android.widget.EditText;
import android.app.Activity;

public class General {
	
	public static final int BUFFER_SIZE = 4*1024;
	public static ProgressDialog progressDialog;
	
	public static void goBack(Activity activity)
	{
		activity.finish();
		activity.overridePendingTransition(R.anim.back_new, R.anim.back_old);
	}
	
	public static boolean haveNetworkConnection(Context context) 
	{
        boolean haveConnectedWifi = false;
        boolean haveConnectedMobile = false;

        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
        for (NetworkInfo ni : netInfo) {
            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                if (ni.isConnected())
                    haveConnectedWifi = true;
            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                if (ni.isConnected())
                    haveConnectedMobile = true;
        }
        return haveConnectedWifi || haveConnectedMobile;
    }
	
	public static String handleHttp(String url, String[] parameter, String[] data, HttpContext localContext)
	{	
		String URL = null;
		
		if(parameter == null)
		{
			URL = url;
		}
		else
		{
			StringBuilder sb = new StringBuilder();
			int length = parameter.length;
			
			try {
			for(int i=0; i<length; i++)
			{
				if(i == 0)
					sb.append("?"+parameter[0]+"="+URLEncoder.encode(data[0], "UTF-8"));
				else
					sb.append("&"+parameter[i]+"="+URLEncoder.encode(data[i], "UTF-8"));
			}			
				URL = url + sb.toString();
				//Log.e("URL", URL);
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		
		try {
				HttpGet httpGet = new HttpGet(URL);
				DefaultHttpClient httpClient = new DefaultHttpClient();
		        HttpResponse httpResponse = null;
		        
		        if(localContext == null)
		        {
		            httpResponse = httpClient.execute(httpGet);
		        }
		        else
		        	httpResponse = httpClient.execute(httpGet, localContext);
		        
		        return EntityUtils.toString(httpResponse.getEntity());
	        
		} catch (Exception e) {
			e.printStackTrace();	
			return null;
		}
	}
	
	public static String getEditText(EditText editText)
	{
		String str = editText.getText().toString().trim();
		
		if(str.length()==0)
          return "";  
		else
	      return str;
	}
	
	public static void showAlert(String message, Context context)
	{
		Builder AlertDialog = new AlertDialog.Builder(context);
		
		AlertDialog.setMessage(message);

        DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };

        AlertDialog.setNeutralButton("OK", OkClick);
        AlertDialog.show();
	}
	
	public static void showProgressDialog(Context context)
	{	
		progressDialog = new ProgressDialog(context);
		
    	progressDialog.setMessage(Html.fromHtml(context.getString(R.string.progressMessage)));
    	progressDialog.setCancelable(true);
    	progressDialog.setButton(DialogInterface.BUTTON_NEGATIVE, context.getString(R.string.cancel), new DialogInterface.OnClickListener() {
    	    @Override
    	    public void onClick(DialogInterface dialog, int which) {
    	        dialog.dismiss();
    	    }
    	});	 
    	
    	progressDialog.show();
	}
	
	public static void dismissProgressDialog()
	{  	
    	progressDialog.dismiss();
	}
	
    public static byte[] downloadBitmap(String url) 
    {
        final DefaultHttpClient client = new DefaultHttpClient();
        final HttpGet getRequest = new HttpGet(url);

        try 
        {
            HttpResponse response = client.execute(getRequest);
            int statusCode = response.getStatusLine().getStatusCode();
            if (statusCode != HttpStatus.SC_OK) return null;
            
            HttpEntity entity = response.getEntity();
            if (entity != null) 
            {
                InputStream inputStream = null; 	 
                try 
                {
                    inputStream = entity.getContent(); 
                                 
                    byte[] imageByte =  IOUtils.toByteArray(inputStream);

                    return imageByte;                    
                } 
                finally 
                {	
                	if (inputStream != null) //Log.e("","even "return" first, these lines in "finally" will be executed");
                		inputStream.close();
             	
                    entity.consumeContent();
                }
            }
        } 
        catch (Exception e) 
        {
            getRequest.abort();
        }       
        return null;
    }   
    
    public static void writeObjectToFile(Context context, Object object, String filename) {

    	ObjectOutputStream objectOut = null;
        try {

            FileOutputStream fileOut = context.openFileOutput(filename, Context.MODE_PRIVATE);
            objectOut = new ObjectOutputStream(fileOut);
            objectOut.writeObject(object);
            fileOut.getFD().sync();

        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (objectOut != null) {
                try {
                    objectOut.close();
                } catch (IOException e) {
                    // do nowt
                }
            }
        }
    }

    public static Object readObjectFromFile(Context context, String filename) {

        ObjectInputStream objectIn = null;
        Object object = null;
        try {

            FileInputStream fileIn = context.getApplicationContext().openFileInput(filename);
            objectIn = new ObjectInputStream(fileIn);
            object = objectIn.readObject();

        } catch (Exception e) {
        	e.printStackTrace();
        } 
        
        finally {
            if (objectIn != null) {
                try {
                    objectIn.close();
                } catch (Exception e) {
                }
            }
        }

        return object;
    }
    
    public static byte[] convertIStoByte(InputStream input) throws IOException
    {
        byte[] buffer = new byte[8192];
        int bytesRead;
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        while ((bytesRead = input.read(buffer)) != -1)
        {
            output.write(buffer, 0, bytesRead);
        }
        return output.toByteArray();
    }

}