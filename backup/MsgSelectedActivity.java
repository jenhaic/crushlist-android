
package crushlist.advanced.activity;


import crushlist.advanced.R;
import crushlist.advanced.util.General;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
//import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import android.content.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MsgSelectedActivity extends Activity {

	private ProgressBar progressBar;
    private EditText cellView;
    private EditText hisNameView;
    private EditText yourNameView;
    private Button sendButton;
    private Button contactsButton;
    private Button ccButton;
    private EditText contentView;
    
    public static String cc;
    public static String number;
    public static String name;
    private String content;
    private String yourName;
    private String time;
    private String result;
    
    private SharedPreferences settings = LoginActivity.settings;
  
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.msgselected);

        progressBar = (ProgressBar) findViewById(R.id.selectedProgressBar);
        contactsButton = (Button) findViewById(R.id.contacts);
        ccButton = (Button) findViewById(R.id.ccButton);
        cellView = (EditText) findViewById(R.id.cellView);
        hisNameView = (EditText) findViewById(R.id.hisNameView);   
        yourNameView = (EditText) findViewById(R.id.yourNameView);   
        sendButton = (Button) findViewById(R.id.choose);          
        contentView = (EditText)findViewById(R.id.msgNew);
        
        ccButton.setText(LoginActivity.cc);
        if(settings.getString("myCrushNumberWOCC", null) != null)
            cellView.setText(settings.getString("myCrushNumberWOCC", null));
        
        contactsButton.setOnClickListener(new ButtonListenerContacts());
        ccButton.setOnClickListener(new ButtonListenerCC());
        sendButton.setOnClickListener(new ButtonListenerMessage());
    }
    
    public void onResume() {
        super.onResume();
        
        if(number != null) cellView.setText(number);
        if(name != null) hisNameView.setText(name);
        if(cc != null) ccButton.setText(cc); 
  
        contactsButton.setBackgroundResource(R.drawable.contacts40_40);
    }
    
    class ButtonListenerCC implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent();
            intent.setClass(MsgSelectedActivity.this, CCActivity.class);
            intent.putExtra("activity","msgSelected");
            startActivity(intent);
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }

    class ButtonListenerContacts implements OnClickListener {
        
        @Override
        public void onClick(View v) {
        	
        	contactsButton.setBackgroundResource(R.drawable.hourglass40_40);
        	
            Intent intent = new Intent();
            intent.setClass(MsgSelectedActivity.this, ContactActivity.class);
            intent.putExtra("activity","msgSelected");
            startActivity(intent);
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }

    }
    
    class ButtonListenerMessage implements OnClickListener, Runnable{

        @Override
    	public void onClick(View v) {

        	sendButton.setEnabled(false);
    		progressBar.setVisibility(View.VISIBLE);
    		
    		InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE); 
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
            
            number = General.getEditText(cellView).replaceAll("\\D", "");
            number = number.replaceFirst("^0*", "");
            cc = ccButton.getText().toString().trim().replaceAll("\\D", "");
            name = General.getEditText(hisNameView);
            content = General.getEditText(contentView);
            yourName = General.getEditText(yourNameView);
            
            if(cc != null && cc.length() <= 0)
            {            	
                General.showAlert(getString(R.string.sendFail), MsgSelectedActivity.this);
                progressBar.setVisibility(View.INVISIBLE);
                sendButton.setEnabled(true);
            }
            else if(number.length() <= 7)
            {
            	General.showAlert(getString(R.string.sendFail), MsgSelectedActivity.this);
                progressBar.setVisibility(View.INVISIBLE);
                sendButton.setEnabled(true);
            }
            else if(name.length() <= 0)
            {
            	General.showAlert(getString(R.string.sendFail), MsgSelectedActivity.this);
                progressBar.setVisibility(View.INVISIBLE);
                sendButton.setEnabled(true);
            }
            else if(content.length() <= 0)
            {
            	General.showAlert(getString(R.string.sendFail), MsgSelectedActivity.this);
                progressBar.setVisibility(View.INVISIBLE);
                sendButton.setEnabled(true);
            }
            else if(yourName.length() <= 0)
            {
            	General.showAlert(getString(R.string.sendFail), MsgSelectedActivity.this);
                progressBar.setVisibility(View.INVISIBLE);
                sendButton.setEnabled(true);
            }
            else
            {	
                if(content.length() > 100)
                    content = content.substring(0, 100);
                
                DateFormat dateFormat = new SimpleDateFormat("MM-dd HH:mm");
        		Date date = new Date();
        		time = dateFormat.format(date);
        		
            	new Thread(this).start();
            }       
        }

        @Override
        public void run() {

            String[] parameter = new String[5];
            String[] data = new String[5];
            parameter[0] = "time"; data[0] = time;
            parameter[1] = "id"; data[1] = cc+number;
            parameter[2] = "content"; data[2] = content;
            parameter[3] = "name"; data[3] = yourName;
            parameter[4] = "title"; data[4] = name;
            
        	result = General.handleHttp("http://www.crushlist.cc/chat/msgOld.php", parameter, data, LoginActivity.localContext); 
            handler.sendEmptyMessage(0);       
        }
        
        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {
            	
            progressBar.setVisibility(View.INVISIBLE);
            sendButton.setEnabled(true); 	
            
            if(result != null && result.contains("success"))
            {
            	Builder AlertDialog = new AlertDialog.Builder(MsgSelectedActivity.this);
        		
        		AlertDialog.setMessage(R.string.sendMessageSuccessForContacts);

                DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                    @Override
        			public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                };

                AlertDialog.setNeutralButton("OK", OkClick);
                AlertDialog.show();
            }
            else
            	General.showAlert(getString(R.string.sendFail), MsgSelectedActivity.this);
            }
        };       
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}