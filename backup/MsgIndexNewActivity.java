package crushlist.advanced.activity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.JSONArray;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ProgressBar;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

public class MsgIndexNewActivity extends Activity implements Runnable{ 
	
	private LinearLayout linearLayout;
	private ProgressBar progressBar;
	private Button[] button;
	private Button[] profileButton;
	private LinearLayout[] linearLayoutHorizontal;
     
    private JSONArray jsonArray;
    
    private int buttonNumber;
    private int r;
    private String id;
    private String title;
	
	private HttpContext localContext = LoginActivity.localContext;
	
	public static String tag;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.msgindexnew);
        
        progressBar = (ProgressBar) findViewById(R.id.msgIndexProgressBar);           
        progressBar.setVisibility(View.VISIBLE);
            
        linearLayout = (LinearLayout)findViewById(R.id.ll_msgIndex);  
        
        progressBar.setVisibility(View.VISIBLE);
        Thread thread = new Thread(MsgIndexNewActivity.this);
        thread.start();
    }
    
    /*
    @Override
    public void onResume() {
        super.onResume();  

        linearLayout.removeAllViews();
        progressBar.setVisibility(View.VISIBLE);
        Thread thread = new Thread(MsgIndexToActivity.this);
        thread.start();
    }
    */

    @Override
	public void run(){
        try{
        	HttpGet httpGet = new HttpGet("http://www.crushlist.cc/chat/msgNewIndex.php");
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(httpGet, localContext);
            String result = EntityUtils.toString(httpResponse.getEntity());
            
            jsonArray = new JSONArray(result);
       
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        handler.sendEmptyMessage(0);
    }
        
    private Handler handler = new Handler(){
        @Override
		public void handleMessage(Message msg){
        	progressBar.setVisibility(View.INVISIBLE);  
        	
        	if(jsonArray != null)
        	  buttonNumber = jsonArray.length();
        	
        	LinearLayout.LayoutParams p  = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);  p.setMargins(0, 30, 0, 0);
        	LinearLayout.LayoutParams p0 = new LinearLayout.LayoutParams(0,LayoutParams.WRAP_CONTENT,1); p0.setMargins(0, 0, 0, 0);
        	LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT,LayoutParams.WRAP_CONTENT,0); p1.setMargins(20, 0, 0, 0); p1.gravity=Gravity.CENTER_VERTICAL;

            button = new Button[buttonNumber];
            profileButton = new Button[buttonNumber];
            linearLayoutHorizontal = new LinearLayout[buttonNumber];
            for (int i = 0; i < buttonNumber; i++) {

            	linearLayoutHorizontal[i] = new LinearLayout(MsgIndexNewActivity.this);
                button[i] = new Button(MsgIndexNewActivity.this);
                button[i].setOnClickListener(new ButtonListenerContent());
                profileButton[i] = new Button(MsgIndexNewActivity.this);
                profileButton[i].setOnClickListener(new ButtonListenerProfile());
                profileButton[i].setBackgroundResource(R.drawable.privatedating40_40);
                
                try {
                	JSONObject jsonObject = jsonArray.getJSONObject(i);
                	button[i].setGravity(Gravity.LEFT);
					button[i].setText(jsonObject.getString("title"));
					button[i].setTypeface(null, Typeface.BOLD);
					button[i].setTag(i); 
					profileButton[i].setId(Integer.parseInt(jsonObject.getString("id")));

		            if(tag != null && tag.equals(jsonObject.getString("id")))
		            	button[i].setBackgroundResource(R.drawable.red_button);
		            else
		            	button[i].setBackgroundResource(R.drawable.yellow_button);
		            	//button[i].getBackground().setColorFilter(0xFFEAA3A5, PorterDuff.Mode.SCREEN);
				} catch (Exception e) {
					e.printStackTrace();
				}
                
                linearLayoutHorizontal[i].addView(button[i],p0);
                linearLayoutHorizontal[i].addView(profileButton[i],p1);
                
                linearLayout.addView(linearLayoutHorizontal[i], p);
            }                    
        }
    };
    
    class ButtonListenerProfile implements OnClickListener
    {	
    	@Override
        public void onClick(View v)
        {
    		ProfileViewActivity.profileViewMode = 3;
        	ProfileViewActivity.chatId = v.getId()+"";
        	
            Intent intent = new Intent();
            intent.setClass(MsgIndexNewActivity.this, ProfileViewActivity.class);
            MsgIndexNewActivity.this.startActivity(intent);
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    class ButtonListenerContent implements OnClickListener
    {	
    	@Override
        public void onClick(View v)
        {
        	
        	int index = 0;
        	for (int i = 0; i < buttonNumber; i++)
        	{
        	      if (button[i].getTag() == v.getTag()) 
        	      {
        	         index = i;
        	         break;
        	      }
            }
        	
        	try {
				JSONObject jsonObject = jsonArray.getJSONObject(index);
				id = jsonObject.getString("id");
				r = jsonObject.getInt("r");
				title = jsonObject.getString("title");
				
				if(tag != null && tag.equals(jsonObject.getString("id")))
				{
	            	tag = null;
	            	button[index].setBackgroundResource(R.drawable.yellow_button);
				}

			} catch (Exception e) {
				e.printStackTrace();
			}
        	
            Intent intent = new Intent();
            intent.putExtra("id", id); 
            intent.putExtra("r", r);
            intent.putExtra("title",title);
            intent.setClass(MsgIndexNewActivity.this, MsgContentActivity.class);
            MsgIndexNewActivity.this.startActivity(intent);
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
    
}
