package crushlist.advanced.activity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;
import org.json.JSONArray;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

public class ListDetailHeartActivity extends Activity implements Runnable{ 

	private TextView listDetailTitleView;
	private LinearLayout linearLayoutTop;
	private LinearLayout linearLayoutVertical;
	private LinearLayout[] linearLayoutHorizontal;
	private ProgressBar progressBar;
	private TextView[][] textView;
	private ScrollView scrollView;
	
	public static String title;
    public static String checkNumber;
		
    private JSONArray jsonArray;
    private int numberOfCell;
    
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listheart);
        
        listDetailTitleView = (TextView) findViewById(R.id.listDetailTitlePrefix);
        progressBar = (ProgressBar) findViewById(R.id.listDetailProgressBar);  
        linearLayoutTop        = (LinearLayout)findViewById(R.id.ll_listDetail_top);
        linearLayoutVertical   = (LinearLayout)findViewById(R.id.ll_listDetail_vertical);
        scrollView = (ScrollView) findViewById(R.id.listDetailScroll);
        
        listDetailTitleView.setText(title);
    }

    @Override
    
    public void onResume() {
        super.onResume();
        
        progressBar.setVisibility(View.VISIBLE);

        Thread thread = new Thread(ListDetailHeartActivity.this);
        thread.start();
      
    }
    
    @Override
	public void onPause() {
        super.onPause();
        
        linearLayoutTop.removeAllViews();
        linearLayoutVertical.removeAllViews();
    }
    
    @Override
	public void run(){
        try{
        	HttpGet httpGet = new HttpGet("http://www.crushlist.cc/listDetailCellEmail.php");
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(httpGet, LoginActivity.localContext);
            String result = EntityUtils.toString(httpResponse.getEntity());
            
            jsonArray = new JSONArray(result);
       
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        handler.sendEmptyMessage(0);
    }
        
    private Handler handler = new Handler(){
        @Override
		public void handleMessage(Message msg){
        	progressBar.setVisibility(View.INVISIBLE);  
        	        	
        	if(jsonArray != null)
          	  numberOfCell = jsonArray.length();
        	
        	int pixelsValue = 3; // margin in pixels
        	float d = ListDetailHeartActivity.this.getResources().getDisplayMetrics().density;
        	int margindp = (int)(pixelsValue * d);
        	        	
        	LinearLayout.LayoutParams p  = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT);  p.setMargins(0, 7, 0, 0);
        	LinearLayout.LayoutParams p0 = new LinearLayout.LayoutParams(0, 43, 0.7f); p0.setMargins(0, 0, 0, 0);  	
        	LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(0, 43, 1f); p1.setMargins(margindp, 0, 0, 0);
        	LinearLayout.LayoutParams p2 = new LinearLayout.LayoutParams(0, 43, 1f); p2.setMargins(margindp, 0, 0, 0);
        	LinearLayout.LayoutParams p3 = new LinearLayout.LayoutParams(0, 43, 1f); p3.setMargins(margindp, 0, 0, 0);
        	LinearLayout.LayoutParams p4 = new LinearLayout.LayoutParams(0, 43, 0.61f); p4.setMargins(margindp, 0, margindp, 0);

        	linearLayoutHorizontal = new LinearLayout[numberOfCell+1];
            textView = new TextView[numberOfCell+1][5];
            
            
            textView[numberOfCell][0] = new TextView(ListDetailHeartActivity.this);
            textView[numberOfCell][1] = new TextView(ListDetailHeartActivity.this);
            textView[numberOfCell][2] = new TextView(ListDetailHeartActivity.this);
            textView[numberOfCell][3] = new TextView(ListDetailHeartActivity.this);
            textView[numberOfCell][4] = new TextView(ListDetailHeartActivity.this);
            linearLayoutHorizontal[numberOfCell] = new LinearLayout(ListDetailHeartActivity.this);
            
            textView[numberOfCell][0].setGravity(Gravity.CENTER);
        	textView[numberOfCell][1].setGravity(Gravity.CENTER);
        	textView[numberOfCell][2].setGravity(Gravity.CENTER);
        	textView[numberOfCell][3].setGravity(Gravity.CENTER);
        	textView[numberOfCell][4].setGravity(Gravity.CENTER);
        	
        	textView[numberOfCell][0].setText(getString(R.string.listDetailNumber));
        	textView[numberOfCell][1].setText(getString(R.string.listDetailCrushList));
        	textView[numberOfCell][2].setText(getString(R.string.listDetailMessageNumber));
        	textView[numberOfCell][3].setText(getString(R.string.listDetailTime));
        	textView[numberOfCell][4].setText(getString(R.string.listDetailMsg));
        	
        	textView[numberOfCell][0].setTextColor(Color.parseColor("#555555"));
        	textView[numberOfCell][1].setTextColor(Color.parseColor("#555555"));
        	textView[numberOfCell][2].setTextColor(Color.parseColor("#555555"));
        	textView[numberOfCell][3].setTextColor(Color.parseColor("#555555"));
        	textView[numberOfCell][4].setTextColor(Color.parseColor("#555555"));
        	
        	textView[numberOfCell][1].setBackgroundColor(0x80eed287);
        	textView[numberOfCell][2].setBackgroundColor(0x80eaa3a5);
        	textView[numberOfCell][3].setBackgroundColor(0x806fb7e9);
        	textView[numberOfCell][4].setBackgroundColor(0x809ccab2);
        	
        	linearLayoutHorizontal[numberOfCell].addView(textView[numberOfCell][0], p0);
            linearLayoutHorizontal[numberOfCell].addView(textView[numberOfCell][1], p1);
            linearLayoutHorizontal[numberOfCell].addView(textView[numberOfCell][2], p2);
            linearLayoutHorizontal[numberOfCell].addView(textView[numberOfCell][3], p3);
            linearLayoutHorizontal[numberOfCell].addView(textView[numberOfCell][4], p4);
            
            linearLayoutTop.addView(linearLayoutHorizontal[numberOfCell], p);
            
            
            for (int i = 0; i < numberOfCell; i++) {

                textView[i][0] = new TextView(ListDetailHeartActivity.this);
                textView[i][1] = new TextView(ListDetailHeartActivity.this);
                textView[i][2] = new TextView(ListDetailHeartActivity.this);
                textView[i][3] = new TextView(ListDetailHeartActivity.this);
                textView[i][4] = new TextView(ListDetailHeartActivity.this);
                linearLayoutHorizontal[i] = new LinearLayout(ListDetailHeartActivity.this);
                
                
                
                try {
                	JSONObject jsonObject = jsonArray.getJSONObject(i);
                	
                	textView[i][0].setGravity(Gravity.CENTER);
                	textView[i][1].setGravity(Gravity.CENTER);
                	textView[i][2].setGravity(Gravity.CENTER);
                	textView[i][3].setGravity(Gravity.CENTER);
                	textView[i][4].setGravity(Gravity.CENTER);
                	
                	//textView[i][0].setBackgroundColor(0x80ffffff);
                	textView[i][1].setBackgroundColor(0x80ffffff);
                	textView[i][2].setBackgroundColor(0x80ffffff);
                	textView[i][3].setBackgroundColor(0x80ffffff);
                	textView[i][4].setBackgroundColor(0x80ffffff);
                	
                	textView[i][0].setTextColor(0xFF000000);
                	textView[i][1].setTextColor(0xFF000000);
                	textView[i][2].setTextColor(0xFF000000);
                	textView[i][3].setTextColor(0xFF000000);
                	textView[i][4].setTextColor(0xFF000000);
                	
                	textView[i][0].setTypeface(null, Typeface.BOLD);
                	textView[i][1].setTypeface(null, Typeface.BOLD);
                	textView[i][2].setTypeface(null, Typeface.BOLD);
                	textView[i][3].setTypeface(null, Typeface.BOLD);
                	textView[i][4].setTypeface(null, Typeface.BOLD);
                	
                	textView[i][0].setText(i+1+"");
                	if(jsonObject.getInt("crushlist") > 0)
                	{
                	  textView[i][1].setTextColor(Color.RED);
					  textView[i][1].setText("Yes");
                	}
                	else
                	  textView[i][1].setText("No");
                	
					textView[i][2].setText(jsonObject.getInt("message")+"");
					textView[i][3].setText(jsonObject.getString("time").substring(2,10));
					textView[i][4].setBackgroundResource(R.drawable.greenrightarrow32_32);
					textView[i][4].setId(jsonObject.getInt("_id"));
					textView[i][4].setTag(i+1);
					textView[i][4].setOnClickListener(new ListenerChat());
					//Log.e("_id", textView[i][4].getId()+"");
					
				} catch (Exception e) {
					e.printStackTrace();
				}
                
                linearLayoutHorizontal[i].addView(textView[i][0], p0);
                linearLayoutHorizontal[i].addView(textView[i][1], p1);
                linearLayoutHorizontal[i].addView(textView[i][2], p2);
                linearLayoutHorizontal[i].addView(textView[i][3], p3);
                linearLayoutHorizontal[i].addView(textView[i][4], p4);
                
                linearLayoutVertical.addView(linearLayoutHorizontal[i], p);
            }    
            
            scrollView.post(new Runnable(){public void run() {scrollView.fullScroll(View.FOCUS_DOWN);}});

        }
    };
    
    class ListenerChat implements OnClickListener
    {	
    	@Override
        public void onClick(View v)
        {    	    		
        	MsgOldFromHeartActivity.personID = v.getId()+"";
        	MsgOldFromHeartActivity.tag = v.getTag()+"";
        	
            Intent intent = new Intent();
            intent.setClass(ListDetailHeartActivity.this, MsgOldFromHeartActivity.class);
            ListDetailHeartActivity.this.startActivity(intent);   
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
