package crushlist.advanced.activity;

import java.net.URLEncoder;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;

import crushlist.advanced.R;
import crushlist.advanced.util.General;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.TextView;

public class UpdateEmailActivity extends Activity {

	private ProgressBar progressBar;
	private TextView emailOldTextView;
	private EditText emailNewEditText;
	private Button updateEmailButton;
	
	private String email = null;
	private String emailResult = null;
	
	private HttpContext localContext = LoginActivity.localContext;
	private SharedPreferences settings = LoginActivity.settings;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.updateemail);

        progressBar = (ProgressBar) findViewById(R.id.updateEmailProgressBar);
        emailOldTextView = (TextView) findViewById(R.id.emailOld);
        emailNewEditText = (EditText) findViewById(R.id.emailNew);
        updateEmailButton = (Button) findViewById(R.id.updateEmail);

        emailOldTextView.setText(settings.getString("email", null));
        updateEmailButton.setOnClickListener(new ButtonListenerUpdateEmail());       
    }
    
    class ButtonListenerUpdateEmail implements OnClickListener, Runnable
    {
        @Override
        public void onClick(View v)
        {
        	progressBar.setVisibility(View.VISIBLE);
        	
            Thread thread = new Thread(this);
		    thread.start();
        }
        
        @Override
        public void run() {
            try {
                  email = emailNewEditText.getText().toString().trim(); 
                  
                  HttpGet httpGet = new HttpGet("http://www.crushlist.cc/updateEmail.php" + "?" + "email2=" + URLEncoder.encode(email, "UTF-8"));               
                  HttpClient httpClient = new DefaultHttpClient();
                  HttpResponse httpResponse = httpClient.execute(httpGet, localContext);
                  emailResult = EntityUtils.toString(httpResponse.getEntity()).substring(8, 15);
                  
                  //HttpGet httpGet = new HttpGet(URLEncoder.encode("http://www.crushlist.cc/updateEmail.php" + "?" + "email2=" + email, "UTF-8")); can't work

            } catch (Exception e) {
                e.printStackTrace();
            }

            handler.sendEmptyMessage(0);
        }

        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {
                progressBar.setVisibility(View.INVISIBLE);

                //Log.e("",emailResult);
                
                Builder AlertDialog = new AlertDialog.Builder(UpdateEmailActivity.this);
                if (emailResult != null && emailResult.equals("success")) {

                	settings.edit().putString("email", email).commit();
                	emailOldTextView.setText(settings.getString("email", null));
                	
                	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                        	dialog.dismiss();
                        }
                    };
                    AlertDialog.setMessage(getString(R.string.sendSuccess)); 
                    AlertDialog.setNeutralButton("OK", OkClick);
                    AlertDialog.show();

                } 
                else if (emailResult != null && emailResult.equals("existed")) {
                	
                	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                        	dialog.dismiss();
                        }
                    };
                    AlertDialog.setMessage(getString(R.string.emailExisted)); 
                    AlertDialog.setNeutralButton("OK", OkClick);
                    AlertDialog.show();

                } 
                else {
                    DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                        @Override
    					public void onClick(DialogInterface dialog, int which) {
                        	dialog.dismiss();
                        }
                    };
                    AlertDialog.setMessage(getString(R.string.sendFail)); 
                    AlertDialog.setNeutralButton("OK", OkClick);
                    AlertDialog.show();
                }
            }
        };
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}

