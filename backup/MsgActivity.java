package crushlist.advanced.activity;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.protocol.HttpContext;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

public class MsgActivity extends Activity implements Runnable{ 
	
	private ProgressBar progressBar;
    private Button fromNumberView;
    private Button toNumberView;
    
    private int howManyFrom;
    private int howManyTo;
    private JSONObject jsonResponse; 
    
    private HttpContext localContext = LoginActivity.localContext;
    private SharedPreferences settings = LoginActivity.settings;

    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.msg);
        
    
        progressBar = (ProgressBar) findViewById(R.id.msgNumberProgressBar);
        
        fromNumberView = (Button) findViewById(R.id.fromNumber); 
        fromNumberView.setOnClickListener(new ButtonListenerFrom());
        
        toNumberView = (Button) findViewById(R.id.toNumber);
        toNumberView.setOnClickListener(new ButtonListenerTo());   
        
	    
    }
    
    @Override
    public void onResume() {
        super.onResume();
        
        settings.edit().putString("myCrushNumberWOCC", null).commit();
        
        if(!General.haveNetworkConnection(this))
            General.showAlert(getString(R.string.noNetwork), this);
        else
        {
        	progressBar.setVisibility(View.VISIBLE);
            new Thread(this).start();
        }
    }
    
    @Override
	public void run(){
        try{
        	HttpGet httpGet = new HttpGet("http://www.crushlist.cc/chat/msgNumber.php");
            HttpClient httpClient = new DefaultHttpClient();
            HttpResponse httpResponse = httpClient.execute(httpGet, localContext);
            String msgResult = EntityUtils.toString(httpResponse.getEntity());
            
            jsonResponse = new JSONObject(msgResult);   
            howManyFrom = jsonResponse.getInt("old");
            howManyTo   = jsonResponse.getInt("new");
        }
        catch(Exception e){
            e.printStackTrace();
        }
        
        handler.sendEmptyMessage(0);
    }
        
    private Handler handler = new Handler(){
        @Override
		public void handleMessage(Message msg){
        	progressBar.setVisibility(View.INVISIBLE);
        	
        	fromNumberView.setText(getResources().getString(R.string.howManyFrom) + ": " +Integer.toString(howManyFrom) + getResources().getString(R.string.people));
        	toNumberView.setText(getResources().getString(R.string.howManyTo) + ": " + Integer.toString(howManyTo) + getResources().getString(R.string.people));               
        }
    };
    
    class ButtonListenerFrom implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent();
            intent.setClass(MsgActivity.this, MsgIndexOldActivity.class);
            MsgActivity.this.startActivity(intent);
        }
    }
    
    class ButtonListenerTo implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent();
            intent.setClass(MsgActivity.this, MsgIndexNewActivity.class);
            MsgActivity.this.startActivity(intent);
        }
    }
    
    public void onBackPressed ()
    {
    	Builder AlertDialog = new AlertDialog.Builder(this);
    	
    	DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent();
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                intent.setClass(MsgActivity.this, LoginActivity.class);
                startActivity(intent);
            }
        };
        
        DialogInterface.OnClickListener CancelClick = new DialogInterface.OnClickListener() {
            @Override
			public void onClick(DialogInterface dialog, int which) {
                dialog.dismiss();
            }
        };
        
        String Logout = getString(R.string.label_logout);
        String YES = getString(R.string.YES);
        String NO = getString(R.string.NO);
        
        AlertDialog.setMessage(Logout).setPositiveButton(NO, CancelClick).setNegativeButton(YES, OkClick).show();
    }
}
