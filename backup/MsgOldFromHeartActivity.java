
package crushlist.advanced.activity;


import crushlist.advanced.R;
import crushlist.advanced.util.General;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
//import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import android.content.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MsgOldFromHeartActivity extends Activity {

	public static String personID;
	public static String tag;

    private EditText yourNameView;
    private Button sendButton;
    private EditText contentView;
    private TextView msgOldHeartTextView;
    
    private String content;
    private String yourName;
    private String time;
    private String title;
    private String result;
    
  
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.msgoldfromheart);
 
        yourNameView = (EditText) findViewById(R.id.yourNameView);   
        sendButton = (Button) findViewById(R.id.choose);          
        contentView = (EditText)findViewById(R.id.msgNew);
        msgOldHeartTextView = (TextView)findViewById(R.id.msgOldHeartTitleTextView);
        
        msgOldHeartTextView.setText("#"+tag);

        sendButton.setOnClickListener(new ButtonListenerMessage());
    }
    
    class ButtonListenerMessage implements OnClickListener, Runnable{

        @Override
    	public void onClick(View v) {
    		
    		InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE); 
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
            
            content = General.getEditText(contentView);
            yourName = General.getEditText(yourNameView);
            title = msgOldHeartTextView.getText().toString().trim();
            
            if(content.length() <= 0)
            {
            	General.showAlert(getString(R.string.sendFail), MsgOldFromHeartActivity.this);
            }
            else if(yourName.length() <= 0)
            { 
            	General.showAlert(getString(R.string.sendFail), MsgOldFromHeartActivity.this);
            }
            else
            {	
                if(content.length() > 100)
                    content = content.substring(0, 100);
                
                DateFormat dateFormat = new SimpleDateFormat("MM-dd HH:mm");
        		Date date = new Date();
        		time = dateFormat.format(date);
        		
        		General.showProgressDialog(MsgOldFromHeartActivity.this);
            	new Thread(this).start();
            }       
        }

        @Override
        public void run() {

            String[] parameter = new String[5];
            String[] data = new String[5];
            parameter[0] = "time"; data[0] = time;
            parameter[1] = "id"; data[1] = personID;
            parameter[2] = "content"; data[2] = content;
            parameter[3] = "toTag"; data[3] = yourName;
            parameter[4] = "fromTag"; data[4] = title;
            
        	result = General.handleHttp("http://www.crushlist.cc/chat/msgOldFromHeart.php", parameter, data, LoginActivity.localContext); 
            handler.sendEmptyMessage(0);       
        }
        
        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {
            	
            General.dismissProgressDialog(); 	
            
            if(result != null && result.contains("success"))
            {
            	Builder AlertDialog = new AlertDialog.Builder(MsgOldFromHeartActivity.this);
        		
        		AlertDialog.setMessage(R.string.sendMessageSuccessForContacts);

                DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                    @Override
        			public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                        finish();
                    }
                };

                AlertDialog.setNeutralButton("OK", OkClick);
                AlertDialog.show();
            }
            else
            	General.showAlert(getString(R.string.sendFail), MsgOldFromHeartActivity.this);
            }
        };       
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}