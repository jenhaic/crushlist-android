package crushlist.advanced.activity;

import android.app.TabActivity;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TabHost;
import android.widget.TextView;
import android.view.View;
import crushlist.advanced.R;
import android.widget.TabHost.OnTabChangeListener;
import crushlist.advanced.activity.Tab;


public class Tab extends TabActivity {

	public static TabHost host;
	
    private String label_list = null;
    private String label_set = null;
    //private String label_msg = null;
    //private String label_share = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        host = getTabHost();
        
        label_list = getString(R.string.label_list);
        label_set = getString(R.string.label_set);
        //label_msg = getString(R.string.label_msg);
        //label_share = getString(R.string.party);

        host.addTab(host.newTabSpec("0").setIndicator(label_list)
                .setContent(new Intent(this, ListActivity.class)));       
        //host.addTab(host.newTabSpec("1").setIndicator(label_msg)
                //.setContent(new Intent(this, MsgActivity.class)));
        //host.addTab(host.newTabSpec("2").setIndicator(label_share)
                //.setContent(new Intent(this, ShareActivity.class)));       
        host.addTab(host.newTabSpec("1").setIndicator(label_set)
                .setContent(new Intent(this, SetActivity.class)));    
        
        
        host.getTabWidget().getChildAt(0).getLayoutParams().height = 80;
        host.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.w_list);
        //host.getTabWidget().getChildAt(1).getLayoutParams().height = 80;
        //host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.b_msg);
        //host.getTabWidget().getChildAt(2).getLayoutParams().height = 80;
        //host.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.b_share);
        host.getTabWidget().getChildAt(1).getLayoutParams().height = 80;
        host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.b_setting);
        
        // too good
        View tabView;
        for (int i = 0; i < host.getTabWidget().getChildCount(); i++) 
        {
            tabView = host.getTabWidget().getChildAt(i);
            LinearLayout.LayoutParams tabLayout = (LinearLayout.LayoutParams) tabView.getLayoutParams();
            tabLayout.setMargins(1, 0, 1, 1);
            
            // finally find how to set text color and size without creating a lot of XML
            TextView tv = (TextView) host.getTabWidget().getChildAt(i).findViewById(android.R.id.title);
            tv.setTextColor(Color.parseColor("#666666"));
            tv.setTextSize(12);             	
        }
        host.getTabWidget().requestLayout(); 
        
        host.setOnTabChangedListener(new TabListenerChange());
    }
    
    class TabListenerChange implements OnTabChangeListener {
      public void onTabChanged(String tabId) {
    	
        int selectedTab = Integer.parseInt(tabId);
   
          switch(selectedTab)
          {
          case 0:
        	  host.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.w_list); 
        	  //host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.b_msg);   	  
        	  //host.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.b_share);
        	  host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.b_setting);
        	  break;
          /*
          case 1:
        	  host.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.b_list);
        	  host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.w_msg); 	  
        	  host.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.b_share);
        	  host.getTabWidget().getChildAt(3).setBackgroundResource(R.drawable.b_setting);
        	break;
          case 2:
        	  host.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.b_list);
        	  host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.b_msg); 	  
        	  host.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.w_share);
        	  host.getTabWidget().getChildAt(3).setBackgroundResource(R.drawable.b_setting);
        	  break;
          */
          case 1:
        	  host.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.b_list);
        	  //host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.b_msg);	  
        	  //host.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.b_share);
        	  host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.w_setting);
        	  break;
          default:
        	  host.getTabWidget().getChildAt(0).setBackgroundResource(R.drawable.w_list); 
        	  //host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.b_msg);   	  
        	  //host.getTabWidget().getChildAt(2).setBackgroundResource(R.drawable.b_share);
        	  host.getTabWidget().getChildAt(1).setBackgroundResource(R.drawable.b_setting);
          }
       }  
    }
}