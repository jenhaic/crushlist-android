package crushlist.advanced.activity;

import org.json.JSONObject;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.ProgressBar;


public class ShareActivity extends Activity implements Runnable{

	public static int shareMode; //1 create 2 update
	public static String A;
	public static String B;
	public static String name;
	public static int sex;
	public static String age;
	public static String city;
	public static String school;
	public static String job;
	public static String height;
	public static String weight;
	public static String interest;
	public static String intro;
	public static int hidden;
	
	private ProgressBar progressBar;
	private Button publicProfileButton;
	private Button personalProfileButton;
    
	private String result;
	private JSONObject jsonResponse;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.share);        
        
        progressBar = (ProgressBar) findViewById(R.id.progressBar);
        publicProfileButton = (Button) findViewById(R.id.publicProfileButton);
        personalProfileButton = (Button) findViewById(R.id.personalProfileButton);
        
        publicProfileButton.setOnClickListener(new ButtonListenerPublicProfile());
        personalProfileButton.setOnClickListener(new ButtonListenerPersonalProfile()); 
        
        publicProfileButton.setEnabled(false);
        personalProfileButton.setEnabled(false);
    }
    
    @Override
    public void onResume() {
        super.onResume();
        
        if(!General.haveNetworkConnection(this))
            General.showAlert(getString(R.string.noNetwork), this);
        else
        {
        	progressBar.setVisibility(View.VISIBLE);
            new Thread(this).start();
        }
    }
    
    @Override
	public void run(){          
        result = General.handleHttp("http://www.crushlist.cc/new/getShareMode.php", null, null, LoginActivity.localContext); 
        handler.sendEmptyMessage(0);
    }
        
    private Handler handler = new Handler(){
        @Override
		public void handleMessage(Message msg){
        	progressBar.setVisibility(View.INVISIBLE);
        	        	
	        try 
	        {
				jsonResponse = new JSONObject(result);
				
				if(jsonResponse != null && jsonResponse.getInt("shareMode") == 1)
				{			
					shareMode = jsonResponse.getInt("shareMode");
					A = jsonResponse.getString("A");
					B = jsonResponse.getString("B");
					
					personalProfileButton.setText(getString(R.string.createProfile));
					personalProfileButton.setEnabled(true);
				}
				else if(jsonResponse != null && jsonResponse.getInt("shareMode") == 2)
				{				
					shareMode = jsonResponse.getInt("shareMode");
					A = jsonResponse.getString("A");
					B = jsonResponse.getString("B");
					name = jsonResponse.getString("name");
					sex = Integer.parseInt(jsonResponse.getString("sex"));
					age = jsonResponse.getString("age");
					city = jsonResponse.getString("city");
					school = jsonResponse.getString("school");
					job = jsonResponse.getString("job");
					height = jsonResponse.getString("height");
					weight = jsonResponse.getString("weight");
					interest = jsonResponse.getString("interest");
					intro = jsonResponse.getString("intro");
					hidden = Integer.parseInt(jsonResponse.getString("hidden"));
					
					personalProfileButton.setText(getString(R.string.updateProfile));
					publicProfileButton.setEnabled(true);
			        personalProfileButton.setEnabled(true);			
				}
			      
			} catch (Exception e) 
			{
				e.printStackTrace();
			}
        }
    };
    
    
    class ButtonListenerPublicProfile implements OnClickListener{
        @Override
        public void onClick(View v) {

        	Intent intent = new Intent();
            intent.setClass(ShareActivity.this, ProfileSearchActivity.class);
            ShareActivity.this.startActivity(intent);
        }
    }
    
    class ButtonListenerPersonalProfile implements OnClickListener{
        @Override
        public void onClick(View v) {

        	Intent intent = new Intent();
            intent.setClass(ShareActivity.this, ProfileMakeActivity.class);
            ShareActivity.this.startActivity(intent);
        }
    }
    
    /*
    class ButtonListenerShareFriend implements OnClickListener{
        @Override
        public void onClick(View v) {

        	Intent sharingIntent = new Intent(android.content.Intent.ACTION_SEND);
            sharingIntent.setType("text/plain");
            startActivity(Intent.createChooser(sharingIntent, getString(R.string.loginSpinnerTitle)));  
        }
    }
    */
    
    public void onBackPressed ()
    {
    	  Builder AlertDialog = new AlertDialog.Builder(this);
    	
    	  DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
              @Override
			  public void onClick(DialogInterface dialog, int which) {
                  Intent intent = new Intent();
                  intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                  intent.setClass(ShareActivity.this, LoginActivity.class);
                  startActivity(intent);
              }
          };
        
          DialogInterface.OnClickListener CancelClick = new DialogInterface.OnClickListener() {
              @Override
			  public void onClick(DialogInterface dialog, int which) {
                  dialog.dismiss();
              }
          };
        
          String Logout = getString(R.string.label_logout);
          String YES = getString(R.string.YES);
          String NO = getString(R.string.NO);
        
          AlertDialog.setMessage(Logout).setPositiveButton(NO, CancelClick).setNegativeButton(YES, OkClick).show();
    }
    
}
