package crushlist.advanced.activity;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.util.Date;
import java.util.concurrent.Executors;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Typeface;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.widget.ImageView;
import android.widget.LinearLayout;

import android.widget.TextView;
import crushlist.advanced.util.General;
import crushlist.advanced.R;

public class ProfileSearchResultActivity extends Activity implements Runnable{ 
	
	public static int profileSearchResultMode; // 1 create time and city 2 login time and city 3 create time
	
	private ImageView[] imageView;
	private TextView[][] textView;
	private LinearLayout profileLL;
	private LinearLayout[] linearLayoutHorizontal;
	
	private String result;
	private int sex;
	private String city;
	private JSONArray jsonArray;
    private int numberOfCell;
	
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profilesearchresult); 
        
        city = ShareActivity.city;
        if(ShareActivity.sex == 1)
        	sex = 2;
        else
        	sex = 1;
        
        profileLL   = (LinearLayout)findViewById(R.id.profileLL);
    	
        
        if(!General.haveNetworkConnection(this))
            General.showAlert(getString(R.string.noNetwork), this);
        else
        {
        	General.showProgressDialog(this);
        	new Thread(this).start();
        }
        
    }
        
    @Override
	public void run()
    {          
    	if(profileSearchResultMode == 1)
    	{
    	    String[] parameter = new String[2];
            String[] data = new String[2];
            parameter[0] = "sex"; data[0] = sex+"";
            parameter[1] = "city"; data[1] = city;
    	
    	    result = General.handleHttp("http://www.crushlist.cc/new/searchByCreateTime.php", parameter, data, LoginActivity.localContext);  
    	}
    	else if(profileSearchResultMode == 2)
    	{
    	    String[] parameter = new String[2];
            String[] data = new String[2];
            parameter[0] = "sex"; data[0] = sex+"";
            parameter[1] = "city"; data[1] = city;
    	
    	    result = General.handleHttp("http://www.crushlist.cc/new/searchByUpdateTime.php", parameter, data, LoginActivity.localContext);  
    	}
    	else if(profileSearchResultMode == 3)
    	{
    	    String[] parameter = new String[1];
            String[] data = new String[1];
            parameter[0] = "sex"; data[0] = sex+"";
    	
    	    result = General.handleHttp("http://www.crushlist.cc/new/searchAllByCreateTime.php", parameter, data, LoginActivity.localContext);  
    	}
    	handler.sendEmptyMessage(0);
    }
          
    private Handler handler = new Handler()
    {
        @Override
		public void handleMessage(Message msg)
        {	
        	General.dismissProgressDialog();
        	
        	try {
				jsonArray = new JSONArray(result);
			} catch (JSONException e) {
				e.printStackTrace();
			}
        	
        	if(jsonArray != null)
          	  numberOfCell = jsonArray.length();
        	
        	imageView = new ImageView[numberOfCell];
        	textView = new TextView[numberOfCell][2];
        	linearLayoutHorizontal = new LinearLayout[numberOfCell];
        	
        	int pixelsValue = 2; // margin in pixels
        	float d = ProfileSearchResultActivity.this.getResources().getDisplayMetrics().density;
        	int margindp = (int)(pixelsValue * d);
        	
        	//LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.WRAP_CONTENT); 
        	LinearLayout.LayoutParams p0 = new LinearLayout.LayoutParams((int)(70*d), (int)(70*d), 0); p0.setMargins(margindp, 3*margindp, 0, 3*margindp);
        	LinearLayout.LayoutParams p1 = new LinearLayout.LayoutParams(0, (int)(50*d), 1); p1.setMargins(margindp, 0, 0, 0); p1.gravity = Gravity.CENTER;
        	LinearLayout.LayoutParams p2 = new LinearLayout.LayoutParams(0, (int)(50*d), 2); p2.setMargins(margindp, 0, 0, 0); p2.gravity = Gravity.CENTER_VERTICAL;
        	LinearLayout.LayoutParams p3 = new LinearLayout.LayoutParams(LayoutParams.FILL_PARENT, 2); //p3.setMargins(0, 0, 0, 0);
        	
        	for (int i = 0; i < numberOfCell; i++) 
        	{  	
        		imageView[i] = new ImageView(ProfileSearchResultActivity.this);
        		textView[i][0] = new TextView(ProfileSearchResultActivity.this);
                textView[i][1] = new TextView(ProfileSearchResultActivity.this);
                linearLayoutHorizontal[i] = new LinearLayout(ProfileSearchResultActivity.this);
                View bottomLine = new View(ProfileSearchResultActivity.this); 
                bottomLine.setBackgroundColor(0xFFEDEAE1);
        		
        		try 
        		{
                	JSONObject jsonObject = jsonArray.getJSONObject(i);
                	
                	//Log.e("",jsonObject.getString("name"));
                	textView[i][0].setText(jsonObject.getString("name"));
                	textView[i][1].setText(jsonObject.getString("intro"));
                	textView[i][0].setTypeface(null,Typeface.BOLD);
                	textView[i][0].setTextSize((float)16.0);
                	textView[i][0].setTextColor(0xFF000000);
                	textView[i][1].setTextColor(0xFF000000);
                	textView[i][0].setGravity(Gravity.CENTER);
                	textView[i][1].setGravity(Gravity.CENTER_VERTICAL);
                	
                	imageView[i].setId(i);
                	textView[i][0].setId(i);
                	textView[i][1].setId(i);
                	imageView[i].setOnClickListener(new ListenerChoose());
                	textView[i][0].setOnClickListener(new ListenerChoose());
                	textView[i][1].setOnClickListener(new ListenerChoose());
        		}
        		catch (Exception e) 
        		{
					e.printStackTrace();
				}
        		
        		linearLayoutHorizontal[i].addView(imageView[i], p0);
        		linearLayoutHorizontal[i].addView(textView[i][0], p1);
        		linearLayoutHorizontal[i].addView(textView[i][1], p2);
        		
        		profileLL.addView(linearLayoutHorizontal[i]);
        	    profileLL.addView(bottomLine,p3);
        	}
        	
        	getImage();
        }
    };
    
    class ListenerChoose implements OnClickListener
    {	
    	@Override
        public void onClick(View v)
        {    	    
    		Intent intent = new Intent();
    		
        	ProfileViewActivity.profileViewMode = 2;
        	try
			{
				ProfileViewActivity.jsonObject = jsonArray.getJSONObject(v.getId());
			} catch (Exception e)
			{
				e.printStackTrace();
			}
        	//imageView[v.getId()].buildDrawingCache();
        	//Bitmap bmp = imageView[v.getId()].getDrawingCache();
        	//ByteArrayOutputStream bs = new ByteArrayOutputStream();
        	//bmp.compress(Bitmap.CompressFormat.PNG, 100, bs);
        	//intent.putExtra("byteArray", bs.toByteArray());
        	
            intent.setClass(ProfileSearchResultActivity.this, ProfileViewActivity.class);
            ProfileSearchResultActivity.this.startActivity(intent);   
            overridePendingTransition(R.anim.enter_new, R.anim.enter_old);
        }
    }
    
    @TargetApi(11)
    public void getImage()
    {
    	AmazonS3Client s3Client = new AmazonS3Client(new BasicAWSCredentials("AKIAIR6LAC3JUL7LQKKQ", "fyR0J8TugJA9wphHpNqsJAwNdrEPIs3tINx4GPL5"));
    	
    	ResponseHeaderOverrides override = new ResponseHeaderOverrides();
    	override.setContentType( "image/jpeg" );
    	
    	for (int i = 0; i < numberOfCell; i++) 
    	{ 	
    		GeneratePresignedUrlRequest urlRequest;
			try
			{
				JSONObject jsonObject = jsonArray.getJSONObject(i);
				urlRequest = new GeneratePresignedUrlRequest("makingfriends",jsonObject.getString("number") + "-1.jpg");
				
				urlRequest.setExpiration(new Date(System.currentTimeMillis() + 3600000));  // Added an hour's worth of milliseconds to the current time.
	    	    urlRequest.setResponseHeaders(override);
	    	
	    	    URL url = s3Client.generatePresignedUrl(urlRequest);
	    	    
	    	    PicAsyncTask picAsyncTask = new PicAsyncTask(i,url.toString());
		        //picAsyncTask.execute();
	    	    //picAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    	    
	    	    
	    	    if (Build.VERSION.SDK_INT >= 11) // Android v3.0+
                    picAsyncTask.executeOnExecutor(Executors.newFixedThreadPool(7));
	    	    else
	    	    	picAsyncTask.execute();
	    	    
			} catch (Exception e)
			{
				e.printStackTrace();
			}	    
    	}
    }
    
    
    class PicAsyncTask extends AsyncTask<Void, Void, byte[]>{
    	
    	int i;
    	String url;
    	
    	public PicAsyncTask(int i, String url) {
    		this.i = i;
    		this.url = url;
            //progressBar.setVisibility(View.VISIBLE);
        }
    	
    	@Override
    	protected byte[] doInBackground(Void... params) {

    		byte[] imageByte = General.downloadBitmap(url);
    		return imageByte;
    	}
    	
    	@Override
    	@TargetApi(11)
    	protected void onPostExecute(byte[] imageByte) {
    		//progressBar.setVisibility(View.INVISIBLE);		
			try
			{	
				Metadata metadata = ImageMetadataReader.readMetadata(new BufferedInputStream(new ByteArrayInputStream(imageByte)), false); // true for streaming
	            ExifIFD0Directory exifIFD0Directory = metadata.getDirectory(ExifIFD0Directory.class);
	            
	            if(exifIFD0Directory != null && exifIFD0Directory.containsTag(ExifIFD0Directory.TAG_ORIENTATION))
	            {
		            int orientation = exifIFD0Directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
		            
		            
		            if (Build.VERSION.SDK_INT >= 11) // Android v3.0+
		            {
		                try 
		                {
				            if (orientation ==ExifInterface.ORIENTATION_NORMAL) 
				            {
				            } 
				            else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) 
				            {
				            	imageView[i].setRotation((float)90.0);
				            } 
				            else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) 
				            {
				            	imageView[i].setRotation((float)180.0);
				            } 
				            else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) 
				            {
				            	imageView[i].setRotation((float)270.0);
				            }	
				            
		                } catch (Exception e) {
		            	}
		            }
	            }
				
				BitmapFactory.Options options=new BitmapFactory.Options();
	            options.inJustDecodeBounds = false;
	            options.inSampleSize = 10; //width，hight設為原來的十分一
	            Bitmap bmp =BitmapFactory.decodeByteArray(imageByte,0,imageByte.length,options); 
	              		
			    imageView[i].setImageBitmap(bmp);
			    			    	    		    
			} catch (Exception e)
			{
				e.printStackTrace();
			}		   
    	}
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
