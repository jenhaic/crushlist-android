package crushlist.advanced.activity;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import crushlist.advanced.R;
import crushlist.advanced.util.General;

public class ProfileSearchActivity extends Activity implements Runnable{

    private Button ruleButton;
    private Button userSelfButton;
    private Button createTimeButton;
    private Button updateTimeButton;
    private Button allCreateTimeButton;
    private TextView searchCityTextView;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profilesearch);

        ruleButton = (Button) findViewById(R.id.ruleButton);
        userSelfButton = (Button) findViewById(R.id.userSelfButton);
        createTimeButton = (Button) findViewById(R.id.createTimeButton);
        updateTimeButton = (Button) findViewById(R.id.updateTimeButton);
        allCreateTimeButton = (Button) findViewById(R.id.allCreateTimeButton);
        searchCityTextView = (TextView) findViewById(R.id.searchCityTextView);
        
        searchCityTextView.setText(getString(R.string.searchCity)+" "+ShareActivity.city);
        
        ruleButton.setOnClickListener(new ButtonListenerRule());
        userSelfButton.setOnClickListener(new ButtonListenerUserSelf());
        createTimeButton.setOnClickListener(new ButtonListenerCreateTime());
        updateTimeButton.setOnClickListener(new ButtonListenerUpdateTime());
        allCreateTimeButton.setOnClickListener(new ButtonListenerAllCreateTime());
        
        Thread thread = new Thread(this);
        thread.start();      
    }
    
    public void run()
    {          
    	HttpGet httpGet = new HttpGet("http://www.crushlist.cc/new/updateTime.php");
        HttpClient httpClient = new DefaultHttpClient();
        try
		{
			httpClient.execute(httpGet, LoginActivity.localContext);
		} catch (Exception e)
		{
			e.printStackTrace();
		} 
    }
    
    class ButtonListenerRule implements OnClickListener{
        @Override
        public void onClick(View v) {
 	 
            General.showAlert(getString(R.string.rule), ProfileSearchActivity.this);                
        }
    }
    
    class ButtonListenerUserSelf implements OnClickListener{
        @Override
        public void onClick(View v) {
 	 
        	ProfileViewActivity.profileViewMode = 1;
        	
        	Intent intent = new Intent();
            intent.setClass(ProfileSearchActivity.this, ProfileViewActivity.class);
            startActivity(intent);                  
        }
    }
    
    class ButtonListenerCreateTime implements OnClickListener{
        @Override
        public void onClick(View v) {
 	 
        	if(ShareActivity.hidden == 2)
        		General.showAlert(getString(R.string.pleaseOpenFile), ProfileSearchActivity.this);
        	else
        	{
        	    ProfileSearchResultActivity.profileSearchResultMode = 1;
        	
        	    Intent intent = new Intent();
                intent.setClass(ProfileSearchActivity.this, ProfileSearchResultActivity.class);
                startActivity(intent);
        	}
        }
    }
    
    class ButtonListenerUpdateTime implements OnClickListener{
        @Override
        public void onClick(View v) {
 	 
        	if(ShareActivity.hidden == 2)
        		General.showAlert(getString(R.string.pleaseOpenFile), ProfileSearchActivity.this);
        	else
        	{
	        	ProfileSearchResultActivity.profileSearchResultMode = 2;
	        	
	        	Intent intent = new Intent();
	            intent.setClass(ProfileSearchActivity.this, ProfileSearchResultActivity.class);
	            startActivity(intent);
        	}
        }
    }
    
    class ButtonListenerAllCreateTime implements OnClickListener{
        @Override
        public void onClick(View v) {
 	 
        	if(ShareActivity.hidden == 2)
        		General.showAlert(getString(R.string.pleaseOpenFile), ProfileSearchActivity.this);
        	else
        	{
	        	ProfileSearchResultActivity.profileSearchResultMode = 3;
	        	
	        	Intent intent = new Intent();
	            intent.setClass(ProfileSearchActivity.this, ProfileSearchResultActivity.class);
	            startActivity(intent);   
        	}
        }
    }
    
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
