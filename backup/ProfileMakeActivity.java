package crushlist.advanced.activity;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.util.Date;

import org.json.JSONObject;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.ProgressEvent;
import com.amazonaws.services.s3.model.ProgressListener;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

import android.annotation.TargetApi;
import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.provider.MediaStore;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ScrollView;


//LoginActivity.settings.getString("email", null);

public class ProfileMakeActivity extends Activity{
	
	private int profileMode = ShareActivity.shareMode; //1 create 2 update
	
	private ScrollView profileScrollView;
	private ImageView profileImageView1;
	private Button uploadImageButton;
	private Button uploadDataButton;
	private Button sexButton;
	private EditText nameEditText;
	private EditText ageEditText;
	private EditText cityEditText;
	private EditText schoolEditText;
	private EditText jobEditText;
	private EditText heightEditText;
	private EditText weightEditText;
	private EditText interestEditText;
	private EditText introEditText;
	
	private String picPath1;
	private String name;
	private int sexNumber = 2;
	private String age;
	private String city;
	private String school;
	private String job;
	private String height;
	private String weight;
	private String interest;
	private String intro;
	private String result;
	private int RESULT_LOAD_IMAGE = 1;
	private JSONObject jsonResponse;
	
	private AmazonS3Client s3Client;
	private PutObjectRequest request1;
	private Bitmap selectedBmp;
	private Bitmap downloadBmp;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profilemake);        
   
        profileScrollView = (ScrollView)findViewById(R.id.profileScrollView);
        profileImageView1 = (ImageView)findViewById(R.id.profileImageView1);
        uploadImageButton = (Button)findViewById(R.id.uploadImageButton);
        uploadDataButton = (Button)findViewById(R.id.uploadDataButton);
        sexButton = (Button)findViewById(R.id.sexButton);
        nameEditText = (EditText)findViewById(R.id.nameEditText);
        ageEditText = (EditText)findViewById(R.id.ageEditText);
        cityEditText = (EditText)findViewById(R.id.cityEditText);
        schoolEditText = (EditText)findViewById(R.id.schoolEditText);
        jobEditText = (EditText)findViewById(R.id.jobEditText);
        heightEditText = (EditText)findViewById(R.id.heightEditText);
        weightEditText = (EditText)findViewById(R.id.weightEditText);
        interestEditText = (EditText)findViewById(R.id.interestEditText);
        introEditText = (EditText)findViewById(R.id.introEditText);
        
        profileImageView1.setOnTouchListener(new ListenerProfileImageView1());
        uploadImageButton.setOnClickListener(new ListenerUploadImageButton());
        uploadDataButton.setOnClickListener(new ListenerUploadDataButton());
        sexButton.setOnClickListener(new ButtonListenerSex());
        
        Drawable img = getResources().getDrawable(R.drawable.female24_24);
    	sexButton.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null );
    	
    	uploadImageButton.setText(getString(R.string.peopleUploadPhoto1));
    	uploadDataButton.setText(getString(R.string.peopleUploadData1)); 
     
        if(profileMode == 2)
        {
        	uploadImageButton.setText(getString(R.string.peopleUploadPhoto2));
        	uploadDataButton.setText(getString(R.string.peopleUploadData2)); 
        	
        	nameEditText.setText(ShareActivity.name);
        	ageEditText.setText(ShareActivity.age);
        	cityEditText.setText(ShareActivity.city);
        	schoolEditText.setText(ShareActivity.school);
        	jobEditText.setText(ShareActivity.job);
        	heightEditText.setText(ShareActivity.height);
        	weightEditText.setText(ShareActivity.weight);
        	interestEditText.setText(ShareActivity.interest);
        	introEditText.setText(ShareActivity.intro);
        	
        	sexButton.setBackgroundColor(Color.TRANSPARENT);
        	sexButton.setEnabled(false);
        	if(ShareActivity.sex == 1)
        	{
        		img = getResources().getDrawable(R.drawable.male24_24);
            	sexButton.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        	}
        	else
        	{
        		img = getResources().getDrawable(R.drawable.female24_24);
            	sexButton.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        	}	
        	    	
        	if(!General.haveNetworkConnection(this))
                General.showAlert(getString(R.string.noNetwork), this);
            else
            {
            	General.showProgressDialog(this);
            	downloadImage();
            } 
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        
        profileScrollView.post(new Runnable(){public void run() {profileScrollView.fullScroll(View.FOCUS_UP);}});
    }
    
    public void downloadImage()
    {
    	s3Client = new AmazonS3Client(new BasicAWSCredentials(ShareActivity.A, ShareActivity.B));
    	
    	ResponseHeaderOverrides override = new ResponseHeaderOverrides();
    	override.setContentType( "image/jpeg" );
    	
	
		GeneratePresignedUrlRequest urlRequest;

	    urlRequest = new GeneratePresignedUrlRequest("makingfriends",LoginActivity.cc+LoginActivity.number+"-1.jpg");
			
		urlRequest.setExpiration(new Date(System.currentTimeMillis() + 3600000));  // Added an hour's worth of milliseconds to the current time.
    	urlRequest.setResponseHeaders(override);
    	
    	URL url = s3Client.generatePresignedUrl(urlRequest);
    	    
    	PicAsyncTask picAsyncTask = new PicAsyncTask(url.toString());
	    picAsyncTask.execute();
        //picAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    	//picAsyncTask.executeOnExecutor(Executors.newFixedThreadPool(3));	    
    	
    	
    }
    
    class PicAsyncTask extends AsyncTask<Void, Void, byte[]>{    	
    	String url;
    	
    	public PicAsyncTask(String url) {
    		this.url = url;
        }
    	
    	@Override
    	protected byte[] doInBackground(Void... params) {

    		byte[] imageByte = General.downloadBitmap(url);
    		return imageByte;
    	}
    	
    	@Override
    	@TargetApi(11)
    	protected void onPostExecute(byte[] imageByte) {
		
    		
    		if(imageByte == null || (imageByte != null && imageByte.length > 1000000))
    			General.dismissProgressDialog();
    		else
    		{
				try
				{	   
					Metadata metadata = ImageMetadataReader.readMetadata(new BufferedInputStream(new ByteArrayInputStream(imageByte)), false); // true for streaming
		            ExifIFD0Directory exifIFD0Directory = metadata.getDirectory(ExifIFD0Directory.class);
		            
		            if(exifIFD0Directory != null && exifIFD0Directory.containsTag(ExifIFD0Directory.TAG_ORIENTATION))
		            {
			            int orientation = exifIFD0Directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
			            
			            
			            if (Build.VERSION.SDK_INT >= 11) // Android v3.0+
			            {
			                try 
			                {
			                	if (orientation == ExifInterface.ORIENTATION_NORMAL) 
			 		            {
			 		            } 
			 		            else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) 
			 		            {
			 		            	profileImageView1.setRotation((float)90.0);
			 		            } 
			 		            else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) 
			 		            {
			 		            	profileImageView1.setRotation((float)180.0);
			 		            } 
			 		            else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) 
			 		            {
			 		            	profileImageView1.setRotation((float)90.0);
			 		            }	
				            	  
			            	} catch (Exception e) {
			            	}
			            }
			           
		            }
		            
					
					BitmapFactory.Options options=new BitmapFactory.Options();
		            options.inJustDecodeBounds = false;
		            options.inSampleSize = 2; 
		            downloadBmp =BitmapFactory.decodeByteArray(imageByte,0,imageByte.length,options); 
		              	
		            profileImageView1.setImageBitmap(downloadBmp);
		            General.dismissProgressDialog();
				    			    	    		    
				} catch (Exception e)
				{
					e.printStackTrace();
				}	
    	    }
    	}
    }
    
    class ButtonListenerSex implements OnClickListener
    {
        @Override
        public void onClick(View v)
        {
            sexNumber = sexNumber + 1;
            
            if(sexNumber%2 == 1)
            {
            	Drawable img = getResources().getDrawable(R.drawable.male24_24);
            	sexButton.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            }
            else
            {
            	Drawable img = getResources().getDrawable(R.drawable.female24_24);
            	sexButton.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
            }      	
        }
    }
    
    class ListenerProfileImageView1 implements OnTouchListener{

		@Override
		public boolean onTouch(View arg0, MotionEvent arg1)
		{			
			Intent i = new Intent(Intent.ACTION_PICK,android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);          
            startActivityForResult(i, RESULT_LOAD_IMAGE);
            
			return false;
		}
    }
    
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
         
        if (requestCode == RESULT_LOAD_IMAGE && resultCode == RESULT_OK && null != data) {
            Uri selectedImage = data.getData();
            String[] filePathColumn = { MediaStore.Images.Media.DATA };
 
            Cursor cursor = getContentResolver().query(selectedImage,filePathColumn, null, null, null);
            cursor.moveToFirst();
 
            int columnIndex = cursor.getColumnIndex(filePathColumn[0]);
            picPath1 = cursor.getString(columnIndex);
            selectedBmp = BitmapFactory.decodeFile(picPath1);
            //b.compress(Bitmap.CompressFormat.JPEG, 50, new FileOutputStream(new java.io.File(picPath1)));

            profileImageView1.setImageBitmap(selectedBmp);            
            
            cursor.close();
        }
    }
    
    class ListenerUploadImageButton implements OnClickListener, Runnable{
        @Override
        public void onClick(View v) {

        	if(picPath1 == null) 
        		General.showAlert(getString(R.string.chooseOneImage), ProfileMakeActivity.this);
        	else
        	{
        		General.showProgressDialog(ProfileMakeActivity.this);
        		
        		s3Client = new AmazonS3Client(new BasicAWSCredentials(ShareActivity.A, ShareActivity.B));

        		new Thread(this).start();
        	}
        }
        
        public void run() {

        	//request1 = new PutObjectRequest("makingfriends", LoginActivity.cc+LoginActivity.number+"-1.jpg", new java.io.File(picPath1));
        	InputStream inputStream = null;
        	ByteArrayOutputStream outputStream = null;
			try
			{
				outputStream = new ByteArrayOutputStream();
				selectedBmp.compress(Bitmap.CompressFormat.JPEG, 50, outputStream);
				
				inputStream = new ByteArrayInputStream(outputStream.toByteArray());
			} catch (Exception e)
			{
				e.printStackTrace();
			}
        	ObjectMetadata metadata = new ObjectMetadata();
        	request1 = new PutObjectRequest("makingfriends", LoginActivity.cc+LoginActivity.number+"-1.jpg",inputStream,metadata);
        	
    		s3Client.putObject(request1);
    		
        	
    		handler.sendEmptyMessage(0);       
        }
        
        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {	
            	
            	picPath1 = null;

            	General.dismissProgressDialog();
            	General.showAlert(ProfileMakeActivity.this.getString(R.string.sendSuccess), ProfileMakeActivity.this);
            }
        };       
    }
    
    class ListenerUploadDataButton implements OnClickListener, Runnable{
        @Override
        public void onClick(View v) {

        	InputMethodManager inputManager = (InputMethodManager)
                    getSystemService(Context.INPUT_METHOD_SERVICE); 
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                       InputMethodManager.HIDE_NOT_ALWAYS);
            
            name = General.getEditText(nameEditText);
            age = General.getEditText(ageEditText);
            city = General.getEditText(cityEditText);
            school = General.getEditText(schoolEditText);
            job = General.getEditText(jobEditText);
            height = General.getEditText(heightEditText);
            weight = General.getEditText(weightEditText);
            interest = General.getEditText(interestEditText);
            intro = General.getEditText(introEditText);
            
            if(name.length() < 1)           	
                General.showAlert(getString(R.string.sendFail), ProfileMakeActivity.this);
            else if(age.length() < 1 || age.length() > 2)           	
                General.showAlert(getString(R.string.sendFail), ProfileMakeActivity.this);
            else if(city.length() < 2)           	
                General.showAlert(getString(R.string.sendFail), ProfileMakeActivity.this);
            else if(job.length() < 2)           	
                General.showAlert(getString(R.string.sendFail), ProfileMakeActivity.this);
            else if(height.length() != 3)           	
                General.showAlert(getString(R.string.sendFail), ProfileMakeActivity.this);
            else if(weight.length() != 2 && weight.length() != 3)           	
                General.showAlert(getString(R.string.sendFail), ProfileMakeActivity.this);
            else if(interest.length() < 2)           	
                General.showAlert(getString(R.string.sendFail), ProfileMakeActivity.this);
            else if(intro.length() < 20)           	
                General.showAlert(getString(R.string.sendFail), ProfileMakeActivity.this);
            else
            {
            	if(sexNumber%2 == 1) sexNumber = 1; else sexNumber = 2;
            	if(intro.length() > 300)
                    intro = intro.substring(0, 300);
                
            	General.showProgressDialog(ProfileMakeActivity.this);
            	new Thread(this).start();
            }   
        }
        
        @Override
    	public void run(){          

    		String[] parameter = new String[11];
            String[] data = new String[11];
            parameter[0] = "profileMode"; data[0] = profileMode+"";
            parameter[1] = "name"; data[1] = name;
            parameter[2] = "sex"; data[2] = sexNumber+"";
            parameter[3] = "age"; data[3] = age;
            parameter[4] = "city"; data[4] = city;
            parameter[5] = "school"; data[5] = school;
            parameter[6] = "job"; data[6] = job;
            parameter[7] = "height"; data[7] = height;
            parameter[8] = "weight"; data[8] = weight;
            parameter[9] = "interest"; data[9] = interest;
            parameter[10] = "intro"; data[10] = intro;
            
        	result = General.handleHttp("http://www.crushlist.cc/new/createUpdateProfile.php", parameter, data, LoginActivity.localContext); 
            handler.sendEmptyMessage(0);  	
        }
            
        private Handler handler = new Handler(){
            @Override
    		public void handleMessage(Message msg){
            	General.dismissProgressDialog();
        		try
				{
					jsonResponse = new JSONObject(result); 
					if(jsonResponse.getString("result") != null && jsonResponse.getString("result").equals("success"))
					{
						profileMode = 2;
						General.showAlert(ProfileMakeActivity.this.getString(R.string.sendSuccess), ProfileMakeActivity.this);
					}
					else
						General.showAlert(ProfileMakeActivity.this.getString(R.string.sendFail), ProfileMakeActivity.this);
				} catch (Exception e)
				{
					e.printStackTrace();
				}
            	
            }
        };
    }
    
    class ListenerUploadImage implements ProgressListener
    {       
		@Override
		public void progressChanged(ProgressEvent arg0)
		{   
            if(arg0.getEventCode() == ProgressEvent.COMPLETED_EVENT_CODE) 
            {
 
            }
		}
    }
    
    @Override
    public void onBackPressed() 
    {
    	if(selectedBmp != null) selectedBmp.recycle();
    	if(downloadBmp != null) downloadBmp.recycle();
        General.goBack(this);
    }
}
