package crushlist.advanced.activity;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.json.JSONObject;
import org.json.JSONArray;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.AlertDialog.Builder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.Gravity;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.ScrollView;
import android.widget.TextView;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

public class MsgContentActivity extends Activity implements Runnable{ 
	
	private ProgressBar    progressBar;
	private TextView[]     textView;
	private EditText       editTextView;
	private TextView[]     timeTextView;
	private Button         button;
	private ScrollView     scrollView;
	private TextView       titleView;
	private LinearLayout   linearLayout;
    
    private JSONArray jsonArray;
    
    private int messageNumber;
    private String id;
    private String title;
    private int r;
    private String content;
    private String time;
    private String result;

    public static String talkId;
    
    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.msgcontent);
        
        titleView = (TextView) findViewById(R.id.msgContentToTitle);
        scrollView = (ScrollView) findViewById(R.id.sv_msgContentTo);     		
        progressBar = (ProgressBar) findViewById(R.id.msgContentToProgressBar);           
        editTextView = (EditText)findViewById(R.id.msgInput);
        button   = (Button)findViewById(R.id.msgSend);
        linearLayout = (LinearLayout)findViewById(R.id.ll_msgContentTo); 
        
        button.setOnClickListener(new ButtonListenerSend());  
        
        Intent intent = getIntent();
        id = intent.getStringExtra("id");
        title = intent.getStringExtra("title");
        r = intent.getExtras().getInt("r");
        titleView.setText(title);
        talkId = id;          
    }
    
    @Override
    public void onResume() {
        super.onResume();  
        
        IntentFilter filter = new IntentFilter();
        filter.addAction("crushlist_message_new");
        registerReceiver(receiver, filter);  
        
        if(!General.haveNetworkConnection(this))
            General.showAlert(getString(R.string.noNetwork), this);
        else
        {
            linearLayout.removeAllViews();
            progressBar.setVisibility(View.VISIBLE);
            Thread thread = new Thread(this);
            thread.start();
        }
    }
    
    public void run(){  
          result = General.handleHttp("http://www.crushlist.cc/chat/msgContent.php?id="+id, null, null, LoginActivity.localContext);  
          handler.sendEmptyMessage(0);
    }
        
    private Handler handler = new Handler(){
        @Override
		public void handleMessage(Message msg){     	      	
        	progressBar.setVisibility(View.INVISIBLE); 

        	try {
				jsonArray = new JSONArray(result);
			} catch (Exception e) {
				e.printStackTrace();
			}
        	
        	if(jsonArray != null)
        		messageNumber = jsonArray.length();
        	
    	    textView = new TextView[messageNumber];
    	    timeTextView = new TextView[messageNumber];
    	    
            for (int i = 0; i < messageNumber; i++) 
            {        
                textView[i] = new TextView(MsgContentActivity.this);
                timeTextView[i] = new TextView(MsgContentActivity.this);
                textView[i].setTextSize(18);
                timeTextView[i].setTextSize(10);
                textView[i].setTextColor(Color.parseColor("#000000"));
                timeTextView[i].setTextColor(Color.parseColor("#555555"));
                textView[i].setTypeface(null, Typeface.BOLD);
                try 
                {
            	    JSONObject jsonObject = jsonArray.getJSONObject(i);
            	    textView[i].setText(jsonObject.getString("content"));
            	    timeTextView[i].setText(jsonObject.getString("time").substring(5,16));
            	    
            	    LinearLayout.LayoutParams p =new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        	    	LinearLayout.LayoutParams p_time = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
            	    if(jsonObject.getInt("r") == 2)
                	{
            	    	p.setMargins(20, 0, 20, 0); 
            	    	p.gravity = Gravity.RIGHT; //the way to set layout_gravity in program
            	    	p_time.setMargins(20, 0, 20, 0);
            	    	p_time.gravity = Gravity.RIGHT;
            	    	textView[i].setBackgroundResource(R.drawable.red_msg); //setBackgroundColor(Color.parseColor("#eaa3a5")); not good enough
            	    }    
            	    else
            	    {
            	    	p.setMargins(20, 0, 20, 0); 
            	    	p.gravity = Gravity.LEFT; 
            	    	p_time.setMargins(20, 0, 20, 0);
            	    	p_time.gravity = Gravity.LEFT;
                	    textView[i].setBackgroundResource(R.drawable.green_msg);	
            	    }
            	    linearLayout.addView(textView[i], p);
        	    	linearLayout.addView(timeTextView[i], p_time);
                } 
                catch (Exception e) 
                {
				    e.printStackTrace();
			    }	               
            }   
            
        	scrollView.post(new Runnable(){public void run() {scrollView.fullScroll(View.FOCUS_DOWN);}});      
        }
    };
    
    
    class ButtonListenerSend implements OnClickListener, Runnable
    {	
    	@Override
        public void onClick(View v)
        {
    		button.setEnabled(false);
    		
    		InputMethodManager inputManager = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE); 
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),InputMethodManager.HIDE_NOT_ALWAYS);
                       
            content = General.getEditText(editTextView);           
            if(content.length() <= 0)
            {            	
                General.showAlert(getString(R.string.sendFail), MsgContentActivity.this);
                progressBar.setVisibility(View.INVISIBLE);
                button.setEnabled(true);
            }
            else if(!General.haveNetworkConnection(MsgContentActivity.this))
            {
                General.showAlert(getString(R.string.noNetwork), MsgContentActivity.this);
                progressBar.setVisibility(View.INVISIBLE);
                button.setEnabled(true);
            }
            else
            {	
                if(content.length() > 100)
                    content = content.substring(0, 100);
                
                DateFormat dateFormat = new SimpleDateFormat("MM-dd HH:mm");
        		Date date = new Date();
        		time = dateFormat.format(date);
                
        		TextView textView = new TextView(MsgContentActivity.this);
	        	TextView timeTextView = new TextView(MsgContentActivity.this);
	            textView.setTextSize(18);
	            timeTextView.setTextSize(10);
	            textView.setTextColor(Color.parseColor("#000000"));
	            timeTextView.setTextColor(Color.parseColor("#555555"));
	            textView.setTypeface(null, Typeface.BOLD);
	            
	    	    textView.setText(content);
	    	    timeTextView.setText(time);
	    	    
	    	    LinearLayout.LayoutParams p =new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
		    	LinearLayout.LayoutParams p_time = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    	    if(r == 2)
	        	{
	    	    	p.setMargins(20, 0, 20, 0); 
	    	    	p.gravity = Gravity.RIGHT;
	    	    	p_time.setMargins(20, 0, 20, 0);
	    	    	p_time.gravity = Gravity.RIGHT;
	    	    	textView.setBackgroundResource(R.drawable.red_msg);
	    	    }    
	    	    else
	    	    {
	    	    	p.setMargins(20, 0, 20, 0); 
	    	    	p.gravity = Gravity.LEFT; 
	    	    	p_time.setMargins(20, 0, 20, 0);
	    	    	p_time.gravity = Gravity.LEFT;
	        	    textView.setBackgroundResource(R.drawable.green_msg);	
	    	    }
	    	    linearLayout.addView(textView, p);
		    	linearLayout.addView(timeTextView, p_time);
		    	
		    	scrollView.post(new Runnable(){public void run(){scrollView.fullScroll(View.FOCUS_DOWN);}}); 
		    	
		    	editTextView.setText("");
        		
                Thread thread = new Thread(this);
    		    thread.start();
            }          		
        }
    	
    	@Override
		public void run()
    	{	
    		String[] parameter = new String[3];
            String[] data = new String[3];
            parameter[0] = "content"; data[0] = content;
            parameter[1] = "time"; data[1] = time;
            parameter[2] = "id"; data[2] = id;
                       
        	result = General.handleHttp("http://www.crushlist.cc/chat/msgAdd.php", parameter, data, LoginActivity.localContext); 
            handler.sendEmptyMessage(0);      
    	 }  
    	 
    	 private Handler handler = new Handler()
    	 {
		   public void handleMessage(Message msg)
    	   {         			       		   
                if(result != null && !result.contains("success"))
                {
            	  Builder AlertDialog = new AlertDialog.Builder(MsgContentActivity.this);
          		
          		  AlertDialog.setMessage(R.string.sendFail);

                  DialogInterface.OnClickListener OkClick = new DialogInterface.OnClickListener() {
                      @Override
          			public void onClick(DialogInterface dialog, int which) {
                          dialog.dismiss();
                          MsgContentActivity.this.onResume();
                      }
                  };

                  AlertDialog.setNeutralButton("OK", OkClick);
                  AlertDialog.show();
                  
                  editTextView.setText(content);

            	  button.setEnabled(true);
                }
                else
        	      button.setEnabled(true);

    	    }
    	 };
    }
    
    private BroadcastReceiver receiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			
	        String message = intent.getStringExtra("message");
	        String time = intent.getStringExtra("time");
			
			TextView textView = new TextView(MsgContentActivity.this);
        	TextView timeTextView = new TextView(MsgContentActivity.this);
            textView.setTextSize(18);
            timeTextView.setTextSize(10);
            textView.setTextColor(Color.parseColor("#000000"));
            timeTextView.setTextColor(Color.parseColor("#555555"));
            textView.setTypeface(null, Typeface.BOLD);
            
    	    textView.setText(message);
    	    timeTextView.setText(time);
    	    
    	    LinearLayout.LayoutParams p =new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	    	LinearLayout.LayoutParams p_time = new LinearLayout.LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
    	    if(r == 1)
        	{
    	    	p.setMargins(20, 0, 20, 0); 
    	    	p.gravity = Gravity.RIGHT;
    	    	p_time.setMargins(20, 0, 20, 0);
    	    	p_time.gravity = Gravity.RIGHT;
    	    	textView.setBackgroundResource(R.drawable.red_msg);
    	    }    
    	    else
    	    {
    	    	p.setMargins(20, 0, 20, 0); 
    	    	p.gravity = Gravity.LEFT; 
    	    	p_time.setMargins(20, 0, 20, 0);
    	    	p_time.gravity = Gravity.LEFT;
        	    textView.setBackgroundResource(R.drawable.green_msg);	
    	    }
    	    linearLayout.addView(textView, p);
	    	linearLayout.addView(timeTextView, p_time);
	    	
	    	scrollView.post(new Runnable(){public void run(){scrollView.fullScroll(View.FOCUS_DOWN);}});      
		}
    };
    
    @Override
    protected void onPause() {
        super.onPause();     

        unregisterReceiver(receiver);
    }
    
    @Override
    public void onBackPressed() 
    {
        General.goBack(this);
    }
}
