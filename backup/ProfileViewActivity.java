package crushlist.advanced.activity;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.net.URL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import com.amazonaws.auth.BasicAWSCredentials;
import com.amazonaws.services.s3.AmazonS3Client;
import com.amazonaws.services.s3.model.GeneratePresignedUrlRequest;
import com.amazonaws.services.s3.model.ResponseHeaderOverrides;
import com.drew.imaging.ImageMetadataReader;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.ExifIFD0Directory;

import crushlist.advanced.R;
import crushlist.advanced.util.General;

import android.annotation.TargetApi;
import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.media.ExifInterface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.ImageView;
import android.widget.ScrollView;


//LoginActivity.settings.getString("email", null);

public class ProfileViewActivity extends Activity implements Runnable{
	
	public static int profileViewMode; // 1 userSelf 2 selectedOne 3 message
	public static JSONObject jsonObject;
	public static String chatId;
	
	private ScrollView profileScrollView;
	private ImageView profileImageView1;
	private TextView sexTextView;
	private TextView nameTextView;
	private TextView ageTextView;
	private TextView cityTextView;
	private TextView schoolTextView;
	private TextView jobTextView;
	private TextView heightTextView;
	private TextView weightTextView;
	private TextView interestTextView;
	private TextView introTextView;
	private EditText replyEditText;
	private Button hideButton;
	private Button replyButton;
	
	private String reply;
	private int hidden;
	private String result;
	private JSONObject jsonResponse;
	private String time;
	private Bitmap bmp;
	
	private AmazonS3Client s3Client;
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profileview);        
   
        profileScrollView = (ScrollView)findViewById(R.id.profileScrollView);
        profileImageView1 = (ImageView)findViewById(R.id.profileImageView1);
        sexTextView = (TextView)findViewById(R.id.sexTextView);
        nameTextView = (TextView)findViewById(R.id.nameTextView);
        ageTextView = (TextView)findViewById(R.id.ageTextView);
        cityTextView = (TextView)findViewById(R.id.cityTextView);
        schoolTextView = (TextView)findViewById(R.id.schoolTextView);
        jobTextView = (TextView)findViewById(R.id.jobTextView);
        heightTextView = (TextView)findViewById(R.id.heightTextView);
        weightTextView = (TextView)findViewById(R.id.weightTextView);
        interestTextView = (TextView)findViewById(R.id.interestTextView);
        introTextView = (TextView)findViewById(R.id.introTextView);
        replyEditText = (EditText)findViewById(R.id.replyEditText);
        hideButton = (Button)findViewById(R.id.hideButton);
        replyButton = (Button)findViewById(R.id.replyButton);
        
        Drawable img = getResources().getDrawable(R.drawable.female24_24);
    	sexTextView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null );
     
        if(profileViewMode == 1)
        {
        	nameTextView.setText(ShareActivity.name);
        	ageTextView.setText(ShareActivity.age);
        	cityTextView.setText(ShareActivity.city);
        	schoolTextView.setText(ShareActivity.school);
        	jobTextView.setText(ShareActivity.job);
        	heightTextView.setText(ShareActivity.height);
        	weightTextView.setText(ShareActivity.weight);
        	interestTextView.setText(ShareActivity.interest);
        	introTextView.setText(ShareActivity.intro);
        	
        	if(ShareActivity.sex == 1)
        	{
        		img = getResources().getDrawable(R.drawable.male24_24);
            	sexTextView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        	}
        	else
        	{
        		img = getResources().getDrawable(R.drawable.female24_24);
            	sexTextView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
        	}	
        	
        	hideButton.setVisibility(View.VISIBLE);
        	hideButton.setOnClickListener(new ButtonListenerHide());
        	if(ShareActivity.hidden == 1)
        		hideButton.setText(getString(R.string.hideFile));
        	else
        		hideButton.setText(getString(R.string.openFile));
        	
        	if(!General.haveNetworkConnection(this))
                General.showAlert(getString(R.string.noNetwork), this);
            else
            {
            	General.showProgressDialog(ProfileViewActivity.this);
            	downloadImage();
            } 
        }
        else if(profileViewMode == 2)
        {
        	try
			{
				nameTextView.setText(jsonObject.getString("name"));
				ageTextView.setText(jsonObject.getString("age"));
				cityTextView.setText(jsonObject.getString("city"));
				schoolTextView.setText(jsonObject.getString("school"));
				jobTextView.setText(jsonObject.getString("job"));
				heightTextView.setText(jsonObject.getString("height"));
				weightTextView.setText(jsonObject.getString("weight"));
				interestTextView.setText(jsonObject.getString("interest"));
				introTextView.setText(jsonObject.getString("intro"));
				
				if(Integer.parseInt(jsonObject.getString("sex")) == 1)
				{
					img = getResources().getDrawable(R.drawable.male24_24);
					sexTextView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
				}
				else
				{
					img = getResources().getDrawable(R.drawable.female24_24);
					sexTextView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
				}				
				
			} catch (Exception e)
			{
				e.printStackTrace();
			} 
        	
        	replyEditText.setVisibility(View.VISIBLE);
        	replyButton.setVisibility(View.VISIBLE);
        	replyButton.setOnClickListener(new ButtonListenerReply());
        	
        	
        	if(!General.haveNetworkConnection(this))
                General.showAlert(getString(R.string.noNetwork), this);
            else
            {
            	General.showProgressDialog(ProfileViewActivity.this);
            	downloadImage();
            }
        	
        	
        	/*
        	if(getIntent().hasExtra("byteArray")) 
        	{
        		Bitmap bmp = BitmapFactory.decodeByteArray(getIntent().getByteArrayExtra("byteArray"),0,getIntent().getByteArrayExtra("byteArray").length);        
        		profileImageView1.setImageBitmap(bmp);        		
        	}
        	*/
        }
        else if(profileViewMode == 3)
        {
        	if(!General.haveNetworkConnection(this))
                General.showAlert(getString(R.string.noNetwork), this);
            else
            {
            	jsonObject = null;
            	General.showProgressDialog(ProfileViewActivity.this);
            	new Thread(this).start();
            }
        }
    }
    
    @Override
    public void onResume() {
        super.onResume();
        
        profileScrollView.post(new Runnable(){public void run() {profileScrollView.fullScroll(View.FOCUS_UP);}});
    }
    
    @Override
	public void run()
	{
    	try
		{
			HttpGet httpGet = new HttpGet("http://www.crushlist.cc/chat/checkProfile.php?ID="+chatId);
			HttpClient httpClient = new DefaultHttpClient();
			HttpResponse httpResponse = httpClient.execute(httpGet, LoginActivity.localContext);
			result = EntityUtils.toString(httpResponse.getEntity());
		} catch (Exception e)
		{
			e.printStackTrace();
		} 
    	handler.sendEmptyMessage(0); 
	}
    
    private Handler handler = new Handler() {
        @Override
		public void handleMessage(Message msg) {	

        	//General.dismissProgressDialog();
        	
        	try
			{
        		jsonObject = new JSONObject(result);   
        		
        		ShareActivity.A = jsonObject.getString("A"); 
        		ShareActivity.B = jsonObject.getString("B");
				nameTextView.setText(jsonObject.getString("name"));
				ageTextView.setText(jsonObject.getString("age"));
				cityTextView.setText(jsonObject.getString("city"));
				schoolTextView.setText(jsonObject.getString("school"));
				jobTextView.setText(jsonObject.getString("job"));
				heightTextView.setText(jsonObject.getString("height"));
				weightTextView.setText(jsonObject.getString("weight"));
				interestTextView.setText(jsonObject.getString("interest"));
				introTextView.setText(jsonObject.getString("intro"));
						
				Drawable img = getResources().getDrawable(R.drawable.female24_24);
		    	sexTextView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null );
				if(Integer.parseInt(jsonObject.getString("sex")) == 1)
				{
					img = getResources().getDrawable(R.drawable.male24_24);
					sexTextView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
				}
				else
				{
					img = getResources().getDrawable(R.drawable.female24_24);
					sexTextView.setCompoundDrawablesWithIntrinsicBounds(img, null, null, null);
				}				
				
			} catch (Exception e)
			{
				e.printStackTrace();
			} 
        	
        	//General.showProgressDialog(ProfileViewActivity.this);
        	downloadImage();
        }
    };       
    
    class ButtonListenerHide implements OnClickListener, Runnable{
        @Override
        public void onClick(View v) {

        	if(ShareActivity.hidden == 1)
        		hidden = 2;
        	else
        		hidden = 1;

        	General.showProgressDialog(ProfileViewActivity.this);
            new Thread(this).start();
        }
        
        public void run() {
        	
        	try
			{
				HttpGet httpGet = new HttpGet("http://www.crushlist.cc/new/hideProfile.php?hidden="+hidden);
				HttpClient httpClient = new DefaultHttpClient();
				HttpResponse httpResponse = httpClient.execute(httpGet, LoginActivity.localContext);
				result = EntityUtils.toString(httpResponse.getEntity());
			} catch (Exception e)
			{
				e.printStackTrace();
			} 
       	
    		handler.sendEmptyMessage(0);       
        }
        
        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {	

            	General.dismissProgressDialog();
            	
            	try
				{
					jsonResponse = new JSONObject(result);   
					if(jsonResponse != null && jsonResponse.getString("result") != null && jsonResponse.getString("result").equals("success"))
					{
						if(ShareActivity.hidden == 1)
						{
							ShareActivity.hidden = 2;
							hideButton.setText(getString(R.string.openFile));
						}
						else
						{
							ShareActivity.hidden = 1;
							hideButton.setText(getString(R.string.hideFile));
						}
						
						General.showAlert(getString(R.string.sendSuccess), ProfileViewActivity.this);
					}
				} catch (Exception e)
				{
					e.printStackTrace();
				}
            }
        };       
    }
    
    class ButtonListenerReply implements OnClickListener, Runnable{
        @Override
        public void onClick(View v) {

        	reply = General.getEditText(replyEditText);
        	if(reply.length() < 1)
        		General.showAlert(getString(R.string.sendFail), ProfileViewActivity.this);
        	else
        	{
        	    if(reply.length() > 100)
                    reply = reply.substring(0, 100);

        	    DateFormat dateFormat = new SimpleDateFormat("MM-dd HH:mm");
        		Date date = new Date();
        		time = dateFormat.format(date);
        	    
        	    General.showProgressDialog(ProfileViewActivity.this);
                new Thread(this).start();
        	}
        }
        
        public void run() {
        	
        	String[] parameter = null;
			String[] data = null;
			try
			{
				parameter = new String[5];
				data = new String[5];
				parameter[0] = "time"; data[0] = time;
				parameter[1] = "toId"; data[1] = jsonObject.getString("number");
				parameter[2] = "content"; data[2] = reply;
				parameter[3] = "fromTag"; data[3] = ShareActivity.name;
				parameter[4] = "toTag"; data[4] = jsonObject.getString("name");
			} catch (Exception e)
			{
				e.printStackTrace();
			}
            
        	result = General.handleHttp("http://www.crushlist.cc/chat/msgNewFromProfile.php", parameter, data, LoginActivity.localContext); 
            handler.sendEmptyMessage(0);        
        }
        
        private Handler handler = new Handler() {
            @Override
    		public void handleMessage(Message msg) {	

            	General.dismissProgressDialog();
            	
            	if(result != null && result.contains("success"))
            	{
            		replyEditText.setText("");
            		General.showAlert(getString(R.string.sendSuccess), ProfileViewActivity.this);
            	}
                else
                	General.showAlert(getString(R.string.sendFail), ProfileViewActivity.this);
                
            }
        };       
    }
    
    public void downloadImage()
    {
    	s3Client = new AmazonS3Client(new BasicAWSCredentials(ShareActivity.A, ShareActivity.B));
    	
    	ResponseHeaderOverrides override = new ResponseHeaderOverrides();
    	override.setContentType( "image/jpeg" );
    	
	
		GeneratePresignedUrlRequest urlRequest = null;

		if(profileViewMode == 1)
	        urlRequest = new GeneratePresignedUrlRequest("makingfriends",LoginActivity.cc+LoginActivity.number+"-1.jpg");
		else if(profileViewMode == 2 || profileViewMode == 3)
			try
			{
				urlRequest = new GeneratePresignedUrlRequest("makingfriends",jsonObject.getString("number")+"-1.jpg");
			} catch (Exception e)
			{
				e.printStackTrace();
			}
			
		urlRequest.setExpiration(new Date(System.currentTimeMillis() + 3600000));  // Added an hour's worth of milliseconds to the current time.
    	urlRequest.setResponseHeaders(override);
    	
    	URL url = s3Client.generatePresignedUrl(urlRequest);
    	    
    	PicAsyncTask picAsyncTask = new PicAsyncTask(url.toString());
	    picAsyncTask.execute();
        //picAsyncTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
    	//picAsyncTask.executeOnExecutor(Executors.newFixedThreadPool(3));	    
    	
    	
    }
    
    class PicAsyncTask extends AsyncTask<Void, Void, byte[]>{    	
    	String url;
    	
    	public PicAsyncTask(String url) {
    		this.url = url;
        }
    	
    	@Override
    	protected byte[] doInBackground(Void... params) {

    		byte[] imageByte = General.downloadBitmap(url);
    		return imageByte;
    	}
    	
    	@Override
    	@TargetApi(11)
    	protected void onPostExecute(byte[] imageByte) {
		
    		//if(imageByte != null) Log.e("size", imageByte.length+"");
    		  		
    		if(imageByte == null || (imageByte != null && imageByte.length > 1000000))
    			General.dismissProgressDialog();
    		else
    		{
    		
				try
				{	   
					Metadata metadata = ImageMetadataReader.readMetadata(new BufferedInputStream(new ByteArrayInputStream(imageByte)), false); // true for streaming
		            ExifIFD0Directory exifIFD0Directory = metadata.getDirectory(ExifIFD0Directory.class);
		            
		            if(exifIFD0Directory != null && exifIFD0Directory.containsTag(ExifIFD0Directory.TAG_ORIENTATION))
		            {
			            int orientation = exifIFD0Directory.getInt(ExifIFD0Directory.TAG_ORIENTATION);
			            
			            
			            if (Build.VERSION.SDK_INT >= 11) // Android v3.0+
			            {
			                try 
			                {
			                	if (orientation == ExifInterface.ORIENTATION_NORMAL) 
			 		            {
			 		            } 
			 		            else if (orientation == ExifInterface.ORIENTATION_ROTATE_90) 
			 		            {
			 		            	profileImageView1.setRotation((float)90.0);
			 		            } 
			 		            else if (orientation == ExifInterface.ORIENTATION_ROTATE_180) 
			 		            {
			 		            	profileImageView1.setRotation((float)180.0);
			 		            } 
			 		            else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) 
			 		            {
			 		            	profileImageView1.setRotation((float)90.0);
			 		            }	
				            	  
			            	} catch (Exception e) {
			            	}
			            }
		            }
		            
					
		            
					BitmapFactory.Options options=new BitmapFactory.Options();
		            options.inJustDecodeBounds = false;
		            options.inSampleSize = 2; 
		            
		            //BitmapFactory.Options opts = new BitmapFactory.Options();
		            //opts.inJustDecodeBounds = true;
		            //can't show photo
		            
		            bmp =BitmapFactory.decodeByteArray(imageByte,0,imageByte.length,options); 
		            
		            profileImageView1.setImageBitmap(bmp);
	
		            General.dismissProgressDialog();
				    			    	    		    
				} catch (Exception e)
				{
					e.printStackTrace();
				}		  
    		}
    	}
    }
    
    @Override
    public void onBackPressed() 
    {
    	if(bmp != null) bmp.recycle();
        General.goBack(this);
    }

	
}
